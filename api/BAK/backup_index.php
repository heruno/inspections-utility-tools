<?php
date_default_timezone_set('Asia/Jakarta');
require 'Database/Database.php';
require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();

$app->post('/postEquipment', 'postByEquipment');
$app->post('/postKode', 'postKode');
$app->post('/postLogin', 'postByLogin');
$app->post('/listEquipment/:id', 'getListEquipment');
$app->get('/listEquipment/:id', 'getListEquipment');
$app->post('/inspectlot', 'getInspctLot');
$app->post('/inspectlot/:id', 'getInspctLotById');
$app->post('/inspectbyid/:id', 'getinspectionByLot');
$app->get('/inspectbyid/:id', 'getinspectionByLot');
$app->post('/upload', 'uploadFile');

//$app->get('/getEquipment/:id', 'getByEquipment');
$app->get('/getEquipment', 'getEquipment');
$app->post('/getEquipment', 'getEquipment');
$app->get('/syncDownload/:dinas', 'getEquipmentByDinas');
$app->get('/syncTemplate/:dinas', 'getTemplateByDinas');
$app->get('/test', 'test');
$app->get('/generate', 'generate');
$app->get('/getEquipment/:id', 'cekTemplate');
$app->get('/getImage', 'getImage');
$app->run();

function getImage()
{
    $img = array();
    if ($handle = opendir('./img')) {
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                $img[] = "http://192.168.1.102/equipment/api/getImage/" . $entry;
            }
        }
        closedir($handle);
    }
    echo json($img);
}

function getListEquipment($id)
{
    $db = new Database();
    $db->where('uid', $id);
    $result = $db->get('tbequipment', null, ['idequipment', 'name']);
    echo json($result);
}

function getInspctLot()
{
    $db = new Database();
    $result = $db->get('tbequipment', null, ['idequipment', 'name']);
    echo json($result);
}

function getInspctLotById($id)
{
    $db = new Database();
    $db->where('idequipment', $id);
    $result = $db->get('tbinspectionlot', null, ['inspectionid', 'inputdt',
        'inputtm']);
    $data = array();
    foreach ($result as $rows) {
        array_push($data, array(
            'inspectionid' => $rows['inspectionid'],
            'date_time' => $rows['inputdt'] . ' ' . $rows['inputtm'],
        ));
    }
    echo json($data);
}

function getinspectionByLot($id)
{
    $db = new Database();
    $db->where('idinspection', $id);
    $result = $db->get('inspection_vd', null, ['label', 'value', 'uom', 'state']);
    echo json($result);
}

function cekTemplate($id)
{
    _log('CEK_ID', $id);
    try {
        $db = new Database();
        $db->where('kode', $id);
        $result = $db->get('template');
        echo json(array('status' => 'sukses', 'msg' => array(
            'header' => $result[0]['header'],
            'transform' => json_decode($result[0]['transform']),
            'data' => json_decode($result[0]['data']))));
    } catch (exception $e) {
        echo json(array('status' => 'error', 'msg' => $e->getMessage()));
    }
}

function cek_min_max($id, $val)
{
    $count = 1;
    $db = new Database();
    $db->where('id', $id);
    $row = $db->get('dd_attribute');
    switch ($row[0]['flag_msg']) {
        case 0:
            $count = 2;
            break;
        case 1:
            if ($val == $row[0]['value_min'])
                $count = 0;
            break;
        case 2:
            if ($val > $row[0]['value_max'])
                $count = 0;
            if ($val < $row[0]['value_min'])
                $count = 0;
            break;
    }
    return $count;
}

function postByLogin()
{
    $request = \Slim\Slim::getInstance()->request();
    $field = json_decode($request->getBody(), true);
    try {
        $hasil = login_proses($field['username']);
        if (sizeof($hasil) > 0) {
            echo json_encode(array(
                'status' => 'sukses',
                'msg' => 'Login Sukses',
                'token' => base64_encode(
                    json_encode(
                        array(
                            'sts' => true,
                            'inspector' => $hasil[0]['nik'],
                            'dinas' => $hasil[0]['dinas']
                        )
                    )
                )
            ));
        } else {
            echo json_encode(array('msg' => 'Username/Password Salah'));
        }
    } catch (exception $e) {
        echo json_encode(array('status' => 'error', 'msg' => $e->getMessage()));
    }
}

function login_proses($username)
{
    $db = new Database();
    $db->where('nik', $username);
    return $db->get('tbinspector');
}

function postByEquipment()
{
    $request = \Slim\Slim::getInstance()->request();
    $field = json_decode($request->getBody(), true);
    $hasil = array();
    $param = null;
    foreach ($field[0] as $row) {
        if ($row['name'] == 'idequipment') {
            $param = $row['value'];
            $lot = array(
                'inspectionid' => null,
                'inputdt' => date('Y-m-d'),
                'inputtm' => date('H:i:s'),
                'idequipment' => $param,
                'idinspector' => $field[1],
                'shift' => getShift(),
                'conditions' => '0');
            _log('CEK_LOT', json_encode($lot));
            $db = new Database();
            $db->insert('tbinspectionlot', $lot);
            $idinsp = $db->getInsertId();
        } else {
            $data = array(
                'idinspection' => $idinsp,
                'idequipment' => $param,
                'idparam' => $row['name'],
                'inputdt' => date('Y-m-d'),
                'inputtm' => date('H:i:s'),
                'value' => $row['value'],
                'state' => cek_min_max($row['name'], $row['value']));
            array_push($hasil, $data);
        }
    }
    $cnt = 0;
    try {
        $i = 0;
        foreach ($hasil as $insert) {
            $cek = cek_min_max($insert['idparam'], $insert['value']);
            if ($cek == 0)
                $cnt = $cnt + 1;
            $db->insert('tbinspection', $insert);
            $result[$i++] = $db->getInsertId();
        }
        if ($cnt > 0) {
            $emm = '';
            foreach (getEmail() as $email) {
                $emm .= $email['email'] . ';';
            }
            send_mail($emm, $field);
            foreach (getPhone() as $phone) {
                send($phone['phone'], 'KODE : ' . $param . ' MEMILIKI ' . $cnt .
                    ' NOTIFIKASI MOHON MAAF JIKA TERGANGGU :)');
            }
        }
        echo json_encode(array('msg' => 'NEW INSPECTION SUCCESS'));
    } catch (exception $e) {
        echo json_encode(array('status' => 'error', 'msg' => $e->getMessage()));
    }
}

function postKode()
{
    $request = \Slim\Slim::getInstance()->request();
    $field = json_decode($request->getBody(), true);
    try {
        $db = new Database();
        $db->insert('nfc', array(
                'kode' => trim($field['kode']),
                'nomor' => trim($field['nomor']))
        );
        $hasil = $db->getInsertId();
        if ($hasil > 0) {
            echo json_encode(array(
                'status' => 'sukses',
                'msg' => 'Berhasil  (' . $hasil . ')',
            ));
        } else {
            echo json_encode(array(
                'status' => 'gagal',
                'msg' => 'Gagal  (' . $hasil . ')',
            ));
        }
    } catch (exception $e) {
        return json(array('status' => 'error', 'msg' => $e->getMessage()));
    }
}

function test()
{
    try {
        $db = new Database();
        $data = $db->get('tbequipment', null, array('idequipment', 'name', 'dinas'));
        foreach ($data as $rows) {
            $d = testData($rows['idequipment']);
            $db->insert('template', array(
                'kode' => $rows['idequipment'],
                'dinas' => $rows['dinas'],
                'header' => $d['header'],
                'transform' => json_encode($d['transform']),
                'data' => json_encode($d['data'])));
        }
        echo "Berhasil";
    } catch (exception $e) {
        return json(array('status' => 'error', 'msg' => $e->getMessage()));
    }
}

function countTemplate($kode)
{
    $db = new Database();
    $db->where('kode', $kode);
    $row = $db->getOne('template', 'COUNT(*) as total');
    return $row['total'];
}

function generate()
{
    try {
        $db = new Database();
        $data = $db->get('tbequipment', null, array('idequipment','uid', 'name', 'dinas'));
        $totalUp = 0;
        $totalIn = 0;
        foreach ($data as $rows) {
            $d     = generateOnline($rows['idequipment']);
            $count = countTemplate($rows['idequipment']);
            if ($count > 0) {
                $db->where('kode', $rows['idequipment']);
                $db->update('template', array(
                    'kode' => $rows['idequipment'],
                    'uid' => $rows['uid'],
                    'dinas' => $rows['dinas'],
                    'header' => $d['header'],
                    'transform' => json_encode($d['transform']),
                    'data' => json_encode($d['data']),
                    'version'=> date("Y-m-d H:i:s")
                ));
                $totalUp++;
            } else {
                $db->insert('template', array(
                    'kode' => $rows['idequipment'],
                    'uid' => $rows['uid'],
                    'dinas' => $rows['dinas'],
                    'header' => $d['header'],
                    'transform' => json_encode($d['transform']),
                    'data' => json_encode($d['data'])
                ));
                $totalIn++;
            }
        }
        echo "Berhasil Insert: $totalIn Update : $totalUp";
    } catch (exception $e) {
        return json(array('status' => 'error', 'msg' => $e->getMessage()));
    }
}

function testData($id)
{
    $ip = $_SERVER['SERVER_ADDR'];
    $idequipment = $id;
    try {
        $header = getHeader($idequipment);
        if (sizeof($header > 0)) {
            $template = getTemplate('form-inspect-3');
            $data = array();
            $hasil1 = array(
                "idequipment" => $header[0]['idequipment'],
                "id" => 'idequipment',
                "type" => "text",
                "label" => "<b>Equipment Code</b>",
                "class" => "easyui-textbox",
                "img" => str_replace('localhost', $ip, $header[0]['image']),
                "name" => "idequipment",
                "value" => $header[0]['idequipment'],
                "data-options" => "readonly:true",
                "style" => "width:100%");
            array_push($data, $hasil1);
            foreach (getByKatalog($idequipment) as $row) {
                array_push($data, $row);
            }
            $result['header'] = $header[0]['name'];
            $result['transform'] = $template[0]['text'];
            $result['data'] = $data;
            return $result;
        }
    } catch (exception $e) {
        return $e->getMessage();
    }
}

function generateOnline($id)
{
    $ip = $_SERVER['SERVER_ADDR'];
    $idequipment = $id;
    try {
        $header = getHeader($idequipment);
        if (sizeof($header > 0)) {
            $template = getTemplate('form-inspect-3');
            $data = array();
            $hasil1 = array(
                "idequipment" => $header[0]['idequipment'],
                "id" => 'idequipment',
                "type" => "text",
                "label" => "<b>Equipment Code</b>",
                "class" => "easyui-textbox",
                "img" => str_replace('localhost', $ip, $header[0]['image']),
                "name" => "idequipment",
                "value" => $header[0]['idequipment'],
                "data-options" => "readonly:true",
                "style" => "width:100%");
            array_push($data, $hasil1);
            foreach (getByKatalog($idequipment) as $row) {
                array_push($data, $row);
            }
            $result['header'] = $header[0]['name'];
            $result['transform'] = $template[0]['text'];
            $result['data'] = $data;
            return $result;
        }
    } catch (exception $e) {
        return $e->getMessage();
    }
}

function getShift()
{
    $date = date('H');
    if ($date >= 22 && $date <= 06)
        return '1';
    if ($date >= 06 && $date <= 14)
        return '2';
    if ($date >= 14 && $date <= 22)
        return '3';
}

function getEmail()
{
    try {
        $db = new Database();
        $data = $db->get('tbemail', null, array('email'));
        return $data;
    } catch (exception $e) {
        return array("email" => "wolu.88@gmail.com");
    }
}

function getPhone()
{
    try {
        $db = new Database();
        $data = $db->get('tbphone', null, array('phone'));
        return $data;
    } catch (exception $e) {
        return array("phone" => "085697469122");
    }
}

function getEquipmentByDinas($dinas)
{
    $response = \Slim\Slim::getInstance()->response();
    try {
        $db = new Database();
        $data = $db->rawQuery('SELECT * FROM dd_attribute_vd WHERE dinas = ?', [$dinas], true);
        //$db->where('dinas', $dinas);
        //$data = $db->get('dd_attribute_vd', null, ['idequipment', 'uid', 'name', 'line', 'dinas', 'spec', 'image']);
        $response->write(json($data));
        return $response;
    } catch (exception $e) {
        return json(array('status' => 'error', 'msg' => $e->getMessage()));
    }
}
function getTemplateByDinas($dinas)
{
    $response = \Slim\Slim::getInstance()->response();
    try {
        $db = new Database();
        $data = $db->rawQuery('SELECT * FROM template WHERE dinas = ?', [$dinas], true);
        //$db->where('dinas', $dinas);
        //$data = $db->get('dd_attribute_vd', null, ['idequipment', 'uid', 'name', 'line', 'dinas', 'spec', 'image']);
        $response->write(json($data));
        return $response;
    } catch (exception $e) {
        return json(array('status' => 'error', 'msg' => $e->getMessage()));
    }
}

function getEquipment()
{
    $response = \Slim\Slim::getInstance()->response();
    try {
        $db = new Database();
        $data = $db->get('tbequipment', null, array('idequipment', 'uid', 'name'));
        $response->write(json($data));
        return $response;
    } catch (exception $e) {
        return json(array('status' => 'error', 'msg' => $e->getMessage()));
    }
}

function getHeader($id)
{
    $db = new Database();
    $db->where('idequipment', $id);
    return $db->get('tbequipment');
}

function getByEquipment($id)
{
    $idequipment = $id;
    $ip = $_SERVER['SERVER_ADDR'];
    /**
     * if (strlen($id) > 1) {
     * $idequipment = substr(trim($id), 1, 7);
     * } else {
     * $idequipment = 'xxx';
     * }*/
    _log('CEK_ID', $idequipment);
    try {
        $header = getHeader($idequipment);

        if (sizeof($header > 0)) {
            $template = getTemplate('form-inspect-3');
            $data = array();
            $hasil1 = array(
                "idequipment" => $header[0]['idequipment'],
                "id" => 'idequipment',
                "type" => "text",
                "label" => "<b>Equipment Code</b>",
                "class" => "easyui-textbox",
                "img" => str_replace('localhost', $ip, $header[0]['image']),
                "name" => "idequipment",
                "value" => $header[0]['idequipment'],
                "data-options" => "readonly:true",
                "style" => "width:100%");
            array_push($data, $hasil1);
            foreach (getByKatalog($idequipment) as $row) {
                array_push($data, $row);
            }
            $result['header'] = $header[0]['name'];
            $result['transform'] = $template[0]['text'];
            $result['data'] = $data;
            echo json(array('status' => 'sukses', 'msg' => $result));
        }
    } catch (exception $e) {

        echo json(array('status' => 'error', 'msg' => $e->getMessage()));
    }
}

function getTemplate($id)
{
    $db = new Database();
    $db->where('id', $id);
    return $db->get('dd_template');
}

function getByKatalog($id)
{
    $db = new Database();
    $db->where('idequipment', $id);
    $result = $db->get('dd_attribute');
    $arrayx = array();
    foreach ($result as $row) {
        $array['label'] = $row['label'];
        if ((($row['value_min'] == 0) && ($row['value_max'] == 1)) || (($row['value_min'] ==
                    1) && ($row['value_max'] == 0)) || (($row['value_min'] == 0) && ($row['value_max'] ==
                    0))) {
            $array['param'] = '';
        } else {
            $array['param'] = '(' . $row['value_min'] . ' - ' . $row['value_max'] . ')';
        }
        $ip = $_SERVER['SERVER_ADDR'];
        $array['img'] = str_replace('localhost', $ip, $row['img']);
        $array['id'] = $row['id'];
        $array['type'] = $row['type'];
        $array['class'] = $row['class'];
        $array['id'] = $row['id'];
        $array['data-options'] = $row['data-options'];
        $array['style'] = $row['style'];
        array_push($arrayx, $array);
    }
    return $arrayx;
}

function json($data)
{
    header('application/json');
    return json_encode($data, JSON_PRETTY_PRINT);
}

function _log($type = 'DEBUG', $text = "KOSONG")
{
    $log = array(
        'id' => null,
        'datetime' => date('Y-m-d H:i:s'),
        'type' => $type,
        'text' => $text);
    $db = new Database();
    $db->insert('log', $log);
}

function send($number, $text)
{
    $cmd = 'C:\Gammu\bin\gammu-smsd-inject.exe -c C:\Gammu\bin\smsdrc EMS "' . $number . '" -text "' . $text . '"';
    if (substr(php_uname(), 0, 7) == "Windows") {
        pclose(popen("start /B " . $cmd, "r"));
    } else {
        exec($cmd . " > /dev/null &");
    }
}

function send_mail($to, $field)
{
    $message = '<html><body>';
    //$message .= "<tr style='background: #eee;'><td><strong>Equipment ID:</strong> </td><td>{Equipment Equipmwnt}</td></tr>";
    $idinsp = 0;
    $hasil = array();
    $param = null;
    foreach ($field[0] as $row) {
        $msgg = cek_min_max($row['name'], $row['value']);
        if ($msgg != 0) {
            if ($row['name'] == 'idequipment') {
                $param = $row['value'];
                $message .= '<table rules="all" style="border:1px solid #666;" cellpadding="10">';
                $message .= "<tr style='background: #eee;'><td><strong>Equipment ID:</strong> </td><td>" .
                    $row['value'] . "</td></tr>";
                $message .= "<tr style='background: #eee;'><td><strong>Inspector:</strong> </td><td>" .
                    $field[1] . "</td></tr>";
                $message .= "<tr style='background: #eee;'><td><strong>Date:</strong> </td><td>" .
                    date('Y-m-d') . "</td></tr>";
                $message .= "<tr style='background: #eee;'><td><strong>Shift:</strong> </td><td>" .
                    getShift() . "</td></tr>";
                $message .= "</table> <br />";
                $message .= '<table rules="all" style="border:1px solid #666;" cellpadding="10">';
                $message .= "<tr style='background: #eee;'><td>ID Parameter</td><td>Value</td><td>State</td></tr>";
            } else {
                switch ($msgg) {
                    case 0:
                        $msgg = 'GOOD CONDITIONS';
                        break;
                    case 1:
                        $msgg = 'NO GOOD CONDITIONS';
                        break;
                    case 2:
                        $msgg = '';
                        break;
                }
                $message .= "<tr><td>" . $row['name'] . "<td>" . $row['value'] . "</td><td>" . $msgg .
                    "</td></tr>";
            }
        }
    }
    $message .= "</table>";
    $message .= "</body></html> <br />";
    $message .= "MOHON MAAF, JIKA TERGANGGU DENGAN EMAIL INI SEDANG MELAKUKAN TEST APP EQUIPMENT INFORMATION SYSTEM";
    $to = $to;
    $subject = 'NOTIFICATION EQUIPMENT INFORMATION SYSTEM';
    $sender = 'wolu.88@gmail.com';
    $password = base64_decode('ZGVzc3lfaGVydW5vXzAyMDY=');
    if (email_localhost($to, $subject, $message, $sender, $password))
        _log('EMAIL', 'EMAIL SENT');
    else
        _log('EMAIL', 'SENDING FAILED');
}

function email_localhost($to, $subject, $message, $sender, $password)
{
    $currentDir = getcwd();
    $send_email = shell_exec('sendEmail.exe -f ' . $sender . ' -t ' . $to . ' -u ' .
        escapeshellarg($subject) . ' -m ' . escapeshellarg($message) .
        ' -s smtp.gmail.com:587 -xu ' . $sender . ' -xp ' . escapeshellarg($password) .
        ' -o message-content-type=html message-charset=utf-8 tls=yes');
    chdir($currentDir);
    if ($send_email) {
        return true;
    } else {
        return false;
    }
}

function uploadFile()
{
    if (!isset($_FILES['uploads'])) {
        echo "No files uploaded!!";
        return;
    }
    $imgs = array();

    $files = $_FILES['uploads'];
    $cnt = count($files['name']);

    for ($i = 0; $i < $cnt; $i++) {
        if ($files['error'][$i] === 0) {
            $name = uniqid('img-' . date('Ymd') . '-');
            if (move_uploaded_file($files['tmp_name'][$i], 'uploads/' . $name) === true) {
                $imgs[] = array('url' => '/uploads/' . $name, 'name' => $files['name'][$i]);
            }

        }
    }

    $imageCount = count($imgs);

    if ($imageCount == 0) {
        echo 'No files uploaded!!  <p><a href="/">Try again</a>';
        return;
    }

    $plural = ($imageCount == 1) ? '' : 's';

    foreach ($imgs as $img) {
        printf('%s <img src="%s" width="50" height="50" /><br/>', $img['name'], $img['url']);
    }
}
