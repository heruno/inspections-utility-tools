<?php


foreach ($field[0] as $row) {
    if ($row['name'] == 'idequipment') {
        $param = $row['value'];
        $lot = array(
            'inspectionid' => null,
            'inputdt' => date('Y-m-d'),
            'inputtm' => date('H:i:s'),
            'idequipment' => $param,
            'idinspector' => $field[1],
            'shift' => getShift(),
            'conditions' => '0');
        $db = new Database();
        $db->insert('tbinspectionlot', $lot);
        $idinsp = $db->getInsertId();
    } else {
        $data = array(
            'idinspection' => $idinsp,
            'idequipment' => $param,
            'idparam' => $row['name'],
            'inputdt' => date('Y-m-d'),
            'inputtm' => date('H:i:s'),
            'value' => $row['value'],
            'state' => cek_min_max($row['name'], $row['value']));
        array_push($hasil, $data);
    }
    $cnt = 0;
    try {
        $i = 0;
        foreach ($hasil as $insert) {
            $cek = cek_min_max($insert['idparam'], $insert['value']);
            if ($cek == 0)
                $cnt = $cnt + 1;
            $db->insert('tbinspection', $insert);
            $result[$i++] = $db->getInsertId();
        }
        if ($cnt > 0) {
            $emm = '';
            foreach (getEmail() as $email) {
                $emm .= $email['email'] . ';';
            }
            send_mail($emm, $field);
            foreach (getPhone() as $phone) {
                send($phone['phone'], 'KODE : ' . $param . ' MEMILIKI ' . $cnt .
                    ' NOTIFIKASI MOHON MAAF JIKA TERGANGGU :)');
            }
        }
        echo json_encode(array('msg' => 'NEW INSPECTION SUCCESS'));
    } catch (exception $e) {
        echo json_encode(array('status' => 'error', 'msg' => $e->getMessage()));
    }
}
     