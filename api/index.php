<?php
ini_set('max_execution_time', 300); //300 seconds = 5 minutes
ini_set('session.gc_maxlifetime', 28800);
date_default_timezone_set('Asia/Jakarta');
define('ENVIRONMENT', 'development');

if (defined('ENVIRONMENT')) {
    switch (ENVIRONMENT) {
        case 'development':
            error_reporting(E_ALL ^ E_NOTICE);
            break;
        case 'testing':
        case 'production':
            error_reporting(0);
            break;
        default:
            exit('The application environment is not set correctly.');
    }
}
require 'Phpmailer/class.phpmailer.php';
require 'Database/Database.php';
require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();

//$app->post('/postEquipment', 'tester');
$app->post('/postEquipment', 'postByEquipment');
$app->post('/postKode', 'postKode');
$app->post('/postLogin', 'postByLogin');
$app->post('/listEquipment/:id', 'getListEquipment');
$app->get('/listEquipment/:id', 'getListEquipment');
$app->post('/inspectlot', 'getInspctLot');
$app->post('/inspectlot/:id', 'getInspctLotById');
$app->post('/inspectbyid/:id', 'getinspectionByLot');
$app->get('/inspectbyid/:id', 'getinspectionByLot');
$app->post('/upload', 'uploadFile');

//$app->get('/getEquipment/:id', 'getByEquipment');
$app->get('/getEquipment', 'getEquipment');
$app->post('/getEquipment', 'getEquipment');
$app->get('/syncDownload/:dinas', 'getEquipmentByDinas');
$app->get('/syncTemplate/:dinas', 'getTemplateByDinas');
$app->get('/syncNfcImage', 'getNfcImage');
$app->get('/test', 'test_email');
$app->get('/generate', 'generate');
$app->get('/job', 'job');
$app->get('/getEquipment/:id', 'cekTemplate');
$app->get('/getImage', 'getImage');
$app->get('/ping', 'ping');
$app->get('/version', 'checkVersion');
$app->post('/ping', 'ping');
$app->run();
function checkVersion(){
    $db = new Database();
    $results = $db->getOne('update_version');
    echo json($results);
}
function job()
{
    $db = new Database();
    $db->where('status', 1);
    $results = $db->get('tbnotif');
    $x = 0;
    foreach ($results as $result) {
        $data[$x]['id'] = $result['id'];
        $data[$x]['email'] = $result['email'];
        $data[$x]['sms'] = $result['sms'];
        $data[$x]['data'] = json_decode($result['ilot'], true);
        $data[$x]['data']['data'] = json_decode($result['insp'], true);
        $i = 0;
        foreach ($data[$x]['data']['data'] as $row) {
            $data[$x]['data']['data'][$i++]['label'] = $this->getLabel($row['data']['data']['idparam']);
        }
        $x++;
    }
    //$result[0]['ilot_status'] = json_decode($result[0]['ilot_status'],true);
    // echo json_encode($data[1],JSON_PRETTY_PRINT);
    for ($i = 0; $i < sizeof($data); $i++) {
        kirim_email($data[$i]);
    }

}

function _sms()
{

}

function _mail($_data)
{
    foreach ($_data as $_row) {
        kirim_email($_row);
    }
}

function test_email()
{
    $data = '{
    "data": [
    {
      "id": "j9xqxqqh-909013",
      "idequipment": "KC-125-E-01A",
      "nama": "TEST DENGAN GAMBAR",
      "inspector": "909013",
      "dinas": "2",
      "date": "11\/13\/2017 12:26:26",
      "note": "TEST CATATAN ",
      "data": [
        {
          "name": "nama",
          "value": "TEST DENGAN GAMBAR"
        },
        {
          "name": "1575",
          "value": "1"
        },
        {
          "name": "1576",
          "value": "1"
        }
      ]
    }
  ]
}';
    kirim_email(json_decode($data, true));
    //postByEquipmentTest(json_decode($data, true));
}

function tester()
{
    $request = \Slim\Slim::getInstance()->request();
    $field = json_decode($request->getBody(), true);
    _log('SYNC UP', json_encode($field));
    echo json_encode(array('msg' => $field));
}

function ping()
{
    $db = new Database();
    echo json_encode(array('msg' => $db->ping()));
}

function getImage()
{
    $img = array();
    if ($handle = opendir('./img')) {
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                $img[] = "http://192.168.1.102/equipment/api/getImage/" . $entry;
            }
        }
        closedir($handle);
    }
    echo json($img);
}

function getListEquipment($id)
{
    $db = new Database();
    $db->where('uid', $id);
    $result = $db->get('tbequipment', null, ['idequipment', 'name']);
    echo json($result);
}

function getInspctLot()
{
    $db = new Database();
    $result = $db->get('tbequipment', null, ['idequipment', 'name']);
    echo json($result);
}

function getInspctLotById($id)
{
    $db = new Database();
    $db->where('idequipment', $id);
    $result = $db->get('tbinspectionlot', null, ['inspectionid', 'inputdt',
        'inputtm']);
    $data = array();
    foreach ($result as $rows) {
        array_push($data, array(
            'inspectionid' => $rows['inspectionid'],
            'date_time' => $rows['inputdt'] . ' ' . $rows['inputtm'],
        ));
    }
    echo json($data);
}

function getinspectionByLot($id)
{
    $db = new Database();
    $db->where('idinspection', $id);
    $result = $db->get('inspection_vd', null, ['label', 'value', 'uom', 'state']);
    echo json($result);
}

function cekTemplate($id)
{
    _log('CEK_ID', $id);
    try {
        $db = new Database();
        $db->where('kode', $id);
        $result = $db->get('template');
        echo json(array('status' => 'sukses', 'msg' => array(
            'header' => $result[0]['header'],
            'transform' => json_decode($result[0]['transform']),
            'data' => json_decode($result[0]['data']))));
    } catch (exception $e) {
        echo json(array('status' => 'error', 'msg' => $e->getMessage()));
    }
}

function cek_min_max($id, $val)
{
    $count = 1;
    $db = new Database();
    $db->where('id', $id);
    $row = $db->get('dd_attribute');
    switch ($row[0]['flag_msg']) {
        case 0:
            $count = 2;
            break;
        case 1:
            if ($val == $row[0]['value_min'])
                $count = 0;
            break;
        case 2:
            if ($val > $row[0]['value_max'])
                $count = 0;
            if ($val < $row[0]['value_min'])
                $count = 0;
            break;
    }
    return $count;
}

function postByLogin()
{
    $request = \Slim\Slim::getInstance()->request();
    $field = json_decode($request->getBody(), true);
    try {
        $hasil = login_proses($field['username']);
        if (sizeof($hasil) > 0) {
            echo json_encode(array(
                'status' => 'sukses',
                'msg' => 'Login Sukses',
                'token' => base64_encode(
                    json_encode(
                        array(
                            'sts' => true,
                            'nik' => $hasil[0]['nik'],
                            'nama' => $hasil[0]['nama'],
                            'dinas' => $hasil[0]['dinas'],
                            'line' => $hasil[0]['line'],
                            'nama_dinas' => getNamaDinas($hasil[0]['dinas'])
                        )
                    )
                )
            ));
        } else {
            echo json_encode(array('msg' => 'NIK TIDAK TERDAFTAR'));
        }
    } catch (exception $e) {
        echo json_encode(array('status' => 'error', 'msg' => $e->getMessage()));
    }
}

function getNamaDinas($code)
{
    $db = new Database();
    $db->where('code', $code);
    $row = $db->getOne('mdinas', 'name');
    return $row['name'];
}

function login_proses($username)
{
    $db = new Database();
    $db->where('nik', $username);
    return $db->get('tbinspector');
}

function postByEquipmentTest($data)
{
    $db = new Database();
    $inspecLot = array();
    $statusLot = array();
    $field = $data;

    try {
        for ($i = 0; $i < sizeof($field['data']); $i++) {

            $inspecLot['localid'] = $field['id'];
            $inspecLot['localdt'] = date_format(date_create($field['date']), "Y-m-d H:i:s");
            $inspecLot['inputdt'] = date('Y-m-d');
            $inspecLot['inputtm'] = date('H:i:s');
            $inspecLot['inspectionid'] = null;
            $inspecLot['dinas'] = $field['dinas'];
            $inspecLot['idequipment'] = $field['idequipment'];
            $inspecLot['idinspector'] = $field['inspector'];
            $inspecLot['shift'] = getShift();
            $inspecLot['status'] = $field['status'];;
            $inspecLot['conditions'] = 0;
            $inspecLot['note'] = $field['note'];
            $db->insert('tbinspectionlot', $inspecLot);
            /***xxxxxxxxxxxxxxxxxxxxxxx**/
            $statusLot[$i]['insertId'] = $db->getInsertId();
            $statusLot[$i]['localid'] = $field['id'];
            $statusLot[$i]['total_data'] = sizeof($field['data']);
            $in = 0;
            $hasil = array();
            foreach ($field['data'] as $insert) {
                $state = cek_min_max($insert['name'], $insert['value']);
                $data = array(
                    'localid' => $field['id'],
                    'idinspection' => $statusLot[$i]['insertId'],
                    'idequipment' => $field['idequipment'],
                    'idparam' => $insert['name'],
                    'inputdt' => date('Y-m-d'),
                    'inputtm' => date('H:i:s'),
                    'value' => $insert['value'],
                    'state' => $state
                );
                $db->insert('tbinspection', $data);
                if ($state == 0) array_push($hasil, $data);
                $in++;
            }

            $statusLot[$i]['total'] = $in;
            $statusLot[$i]['status'] = ($in == sizeof($field['data'])) ? 'VALID' : 'INVALID';
            if (sizeof($hasil) > 0) {
                $notif = array(
                    'id' => null,
                    'ilot' => json_encode($inspecLot),
                    'ilot_status' => json_encode($statusLot[$i]),
                    'insp' => json_encode($hasil),
                    'status' => 1,
                );
                $db->insert('tbnotif', $notif);
            }
        }
        echo json_encode(array('msg' => $statusLot));
    } catch (exception $e) {
        echo json_encode(array('msg' => $e->getMessage()));
    }

}

function postByEquipment()
{
    $db = new Database();
    $inspecLot = array();
    $statusLot = array();
    $request = \Slim\Slim::getInstance()->request();
    $field = json_decode($request->getBody(), true);

    try {
        for ($i = 0; $i < sizeof($field['data']); $i++) {

            $inspecLot['localid'] = $field['id'];
            $inspecLot['localdt'] = date_format(date_create($field['date']), "Y-m-d H:i:s");
            $inspecLot['inputdt'] = date('Y-m-d');
            $inspecLot['inputtm'] = date('H:i:s');
            $inspecLot['inspectionid'] = null;
            $inspecLot['dinas'] = $field['dinas'];
            $inspecLot['idequipment'] = $field['idequipment'];
            $inspecLot['idinspector'] = $field['inspector'];
            $inspecLot['shift'] = getShift();
            $inspecLot['status'] = $field['status'];;
            $inspecLot['conditions'] = 0;
            $inspecLot['note'] = $field['note'];
            $db->insert('tbinspectionlot', $inspecLot);
            /***xxxxxxxxxxxxxxxxxxxxxxx**/
            $statusLot[$i]['insertId'] = $db->getInsertId();
            $statusLot[$i]['localid'] = $field['id'];
            $statusLot[$i]['total_data'] = sizeof($field['data']);
            $in = 0;
            $hasil = array();
            foreach ($field['data'] as $insert) {
                $state = cek_min_max($insert['name'], $insert['value']);
                $data = array(
                    'localid' => $field['id'],
                    'idinspection' => $statusLot[$i]['insertId'],
                    'idequipment' => $field['idequipment'],
                    'idparam' => $insert['name'],
                    'inputdt' => date('Y-m-d'),
                    'inputtm' => date('H:i:s'),
                    'value' => $insert['value'],
                    'state' => $state
                );
                $db->insert('tbinspection', $data);
                if ($state == 0) array_push($hasil, $data);
                $in++;
            }

            $statusLot[$i]['total'] = $in;
            $statusLot[$i]['status'] = ($in == sizeof($field['data'])) ? 'VALID' : 'INVALID';
            if (sizeof($hasil) > 0) {
                $notif = array(
                    'id' => null,
                    'ilot' => json_encode($inspecLot),
                    'ilot_status' => json_encode($statusLot[$i]),
                    'insp' => json_encode($hasil),
                    'status' => 1,
                );
                $db->insert('tbnotif', $notif);
            }
        }
        /*UPLOAD FOTO*/
        for ($j = 0; $j < sizeof($field['photo']); $j++) {
            $poto = upload($field['photo'][$j]['localid'], $field['photo'][$j]['img'], $j);
            $arrPhoto = array(
                'id' => null,
                'inspector' => $field['photo'][$j]['inspector'],
                'dinas' => $field['photo'][$j]['dinas'],
                'localid' => $field['photo'][$j]['localid'],
                'src' => $poto
            );
            if ($poto) {
                $db->insert('photo', $arrPhoto);
                $statusLot[$i]['insertId'] = $db->getInsertId();
            }
        }

        echo json_encode(array('msg' => $statusLot));
    } catch (exception $e) {
        echo json_encode(array('msg' => $e->getMessage()));
    }

}


function postKode()
{
    $request = \Slim\Slim::getInstance()->request();
    $field = json_decode($request->getBody(), true);
    try {
        $db = new Database();
        $db->insert('nfc', array(
                'kode' => trim($field['kode']),
                'nomor' => trim($field['nomor']))
        );
        $hasil = $db->getInsertId();
        if ($hasil > 0) {
            echo json_encode(array(
                'status' => 'sukses',
                'msg' => 'Berhasil  (' . $hasil . ')',
            ));
        } else {
            echo json_encode(array(
                'status' => 'gagal',
                'msg' => 'Gagal  (' . $hasil . ')',
            ));
        }
    } catch (exception $e) {
        return json(array('status' => 'error', 'msg' => $e->getMessage()));
    }
}

function test()
{
    try {
        $db = new Database();
        $data = $db->get('tbequipment', null, array('idequipment', 'name', 'dinas'));
        foreach ($data as $rows) {
            $d = testData($rows['idequipment']);
            $db->insert('template', array(
                'kode' => $rows['idequipment'],
                'dinas' => $rows['dinas'],
                'header' => $d['header'],
                'transform' => json_encode($d['transform']),
                'data' => json_encode($d['data'])));
        }
        echo "Berhasil";
    } catch (exception $e) {
        return json(array('status' => 'error', 'msg' => $e->getMessage()));
    }
}

function countTemplate($kode)
{
    $db = new Database();
    $db->where('kode', $kode);
    $row = $db->getOne('template', 'COUNT(*) as total');
    return $row['total'];
}

function generate()
{
    try {
        $db = new Database();
        $data = $db->get('tbequipment', null, array('idequipment', 'uid', 'name', 'dinas'));
        $totalUp = 0;
        $totalIn = 0;
        $no = 1;
        foreach ($data as $rows) {
            $d = generateOnline($rows['idequipment']);
            $count = countTemplate($rows['idequipment']);
            if ($count > 0) {
                $db->where('kode', $rows['idequipment']);
                $db->update('template', array(
                    'kode' => $rows['idequipment'],
                    'uid' => $rows['uid'],
                    'dinas' => $rows['dinas'],
                    'header' => $d['header'],
                    'transform' => json_encode($d['transform']),
                    'data' => json_encode($d['data']),
                    'version' => date("Y-m-d H:i:s")
                ));
                echo $no.'. UPDATE EQUIPMENT CODE :'.$rows['idequipment'].'<br/>';
                $totalUp++;
            } else {
                $db->insert('template', array(
                    'kode' => $rows['idequipment'],
                    'uid' => $rows['uid'],
                    'dinas' => $rows['dinas'],
                    'header' => $d['header'],
                    'transform' => json_encode($d['transform']),
                    'data' => json_encode($d['data']),
                    'version' => date("Y-m-d H:i:s")
                ));
                echo $no.' INSERT EQUIPMENT CODE :'.$rows['idequipment'].'<br/>';
                $totalIn++;
            }
            $no++;
        }
        echo "SUKSES TOTAL INSERT: $totalIn TOTAL UPDATE : $totalUp";
    } catch (exception $e) {
        return json(array('status' => 'error', 'msg' => $e->getMessage()));
    }
}

function testData($id)
{
    $ip = $_SERVER['SERVER_ADDR'];
    $idequipment = $id;
    try {
        $header = getHeader($idequipment);
        if (sizeof($header > 0)) {
            $template = getTemplate('form-inspect-3');
            $data = array();
            $hasil1 = array(
                "idequipment" => $header[0]['idequipment'],
                "id" => 'idequipment',
                "type" => "text",
                "label" => "<b>Equipment Code</b>",
                "class" => "easyui-textbox",
                "name" => "idequipment",
                "value" => $header[0]['idequipment'],
                "data-options" => "readonly:true",
                "style" => "width:100%");
            array_push($data, $hasil1);
            //"img" => str_replace('localhost', $ip, $header[0]['image']),
            foreach (getByKatalog($idequipment) as $row) {
                array_push($data, $row);
            }
            $result['header'] = $header[0]['name'];
            $result['transform'] = $template[0]['text'];
            $result['data'] = $data;
            return $result;
        }
    } catch (exception $e) {
        return $e->getMessage();
    }
}

function generateOnline($id)
{
    $ip = $_SERVER['SERVER_ADDR'];
    $idequipment = $id;
    try {
        $header = getHeader($idequipment);
        if (sizeof($header > 0)) {
            $template = getTemplate('form-inspect-3');
            $data = array();
            $hasil1 = array(
                "idequipment" => $header[0]['idequipment'],
                "id" => 'idequipment',
                "type" => "text",
                "label" => "<b>Equipment Code</b>",
                "class" => "easyui-textbox",
                "name" => "idequipment",
                "value" => $header[0]['idequipment'],
                "data-options" => "readonly:true",
                "style" => "width:100%");
            array_push($data, $hasil1);
            //"img" => str_replace('localhost', $ip, $header[0]['image']),
            foreach (getByKatalog($idequipment) as $row) {
                array_push($data, $row);
            }
            $result['header'] = $header[0]['name'];
            $result['transform'] = $template[0]['text'];
            $result['data'] = $data;
            return $result;
        }
    } catch (exception $e) {
        return $e->getMessage();
    }
}

function getShift()
{
    $date = date('H');
    if ($date >= 22 && $date <= 06)
        return '1';
    if ($date >= 06 && $date <= 14)
        return '2';
    if ($date >= 14 && $date <= 22)
        return '3';
}

function getLabel($id)
{
    $db = new Database();
    $db->where('id', $id);
    $row = $db->getOne('dd_attribute', 'label');
    return $row['label'];
}

function getEmail()
{
    try {
        $db = new Database();
        $data = $db->get('mkaryawan', null, array('email'));
        return $data;
    } catch (exception $e) {
        return array("email" => "wolu.88@gmail.com");
    }
}

function getPhone()
{
    try {
        $db = new Database();
        $data = $db->get('mkaryawan', null, array('no_telp'));
        return $data;
    } catch (exception $e) {
        return array("phone" => "085697469122");
    }
}
function getNfcImage(){
    $response = \Slim\Slim::getInstance()->response();
    try {
        $db = new Database();
        $data = $db->rawQuery('SELECT kode,image,image64   FROM nfc');
        //$db->where('dinas', $dinas);
        //$data = $db->get('dd_attribute_vd', null, ['idequipment', 'uid', 'name', 'line', 'dinas', 'spec', 'image']);
        $response->write(json($data));
        return $response;
    } catch (exception $e) {
        return json(array('status' => 'error', 'msg' => $e->getMessage()));
    }
}
function getEquipmentByDinas($dinas)
{
    $response = \Slim\Slim::getInstance()->response();
    try {
        $db = new Database();
        $data = $db->rawQuery('SELECT * FROM dd_attribute_vd WHERE dinas = ?', [$dinas], true);
        //$db->where('dinas', $dinas);
        //$data = $db->get('dd_attribute_vd', null, ['idequipment', 'uid', 'name', 'line', 'dinas', 'spec', 'image']);
        $response->write(json($data));
        return $response;
    } catch (exception $e) {
        return json(array('status' => 'error', 'msg' => $e->getMessage()));
    }
}

function getTemplateByDinas($dinas)
{
    $response = \Slim\Slim::getInstance()->response();
    try {
        $db = new Database();
        $data = $db->rawQuery('SELECT * FROM template WHERE dinas = ?', [$dinas], true);
        //$db->where('dinas', $dinas);
        //$data = $db->get('dd_attribute_vd', null, ['idequipment', 'uid', 'name', 'line', 'dinas', 'spec', 'image']);
        $response->write(json($data));
        return $response;
    } catch (exception $e) {
        return json(array('status' => 'error', 'msg' => $e->getMessage()));
    }
}

function getEquipment()
{
    $response = \Slim\Slim::getInstance()->response();
    try {
        $db = new Database();
        $data = $db->get('tbequipment', null, array('idequipment', 'uid', 'name'));
        $response->write(json($data));
        return $response;
    } catch (exception $e) {
        return json(array('status' => 'error', 'msg' => $e->getMessage()));
    }
}

function getHeader($id)
{
    $db = new Database();
    $db->where('idequipment', $id);
    return $db->get('tbequipment');
}

function getByEquipment($id)
{
    $idequipment = $id;
    $ip = $_SERVER['SERVER_ADDR'];
    _log('CEK_ID', $idequipment);
    try {
        $header = getHeader($idequipment);

        if (sizeof($header > 0)) {
            $template = getTemplate('form-inspect-3');
            $data = array();
            $hasil1 = array(
                "idequipment" => $header[0]['idequipment'],
                "id" => 'idequipment',
                "type" => "text",
                "label" => "<b>EQUIPMENT CODE</b>",
                "class" => "easyui-textbox",
                "name" => "idequipment",
                "value" => $header[0]['idequipment'],
                "data-options" => "readonly:true",
                "style" => "width:100%");
            array_push($data, $hasil1);
            foreach (getByKatalog($idequipment) as $row) {
                array_push($data, $row);
            }
            $result['header'] = $header[0]['name'];
            $result['transform'] = $template[0]['text'];
            $result['data'] = $data;
            echo json(array('status' => 'sukses', 'msg' => $result));
        }
    } catch (exception $e) {

        echo json(array('status' => 'error', 'msg' => $e->getMessage()));
    }
}

function getTemplate($id)
{
    $db = new Database();
    $db->where('id', $id);
    return $db->get('dd_template');
}

function getByKatalog($id)
{
    $db = new Database();
    $db->where('idequipment', $id);
    $result = $db->get('dd_attribute');
    $arrayx = array();
    foreach ($result as $row) {
        $array['label'] = $row['label'];
        if ((($row['value_min'] == 0) && ($row['value_max'] == 1)) || (($row['value_min'] == 1) && ($row['value_max'] == 0)) || (($row['value_min'] == 0) && ($row['value_max'] == 0))) {
            $array['param'] = '';
        } else {
            $array['param'] = '(' . $row['value_min'] . ' - ' . $row['value_max'] . ')';
        }

        $array['id'] = $row['id'];
        $array['type'] = $row['type'];
        $array['class'] = $row['class'];
        $array['id'] = $row['id'];
        $array['data-options'] = $row['data-options'];
        $array['style'] = $row['style'];
        array_push($arrayx, $array);
    }
    return $arrayx;
}

function upload($username, $img, $no = 0)
{
    $file = '';
    if ($img != '') {
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $file = $username . '-' . $no . '-' . date('YmdHis') . '.png';
        $des = 'uploads/capture/' . $file;
        file_put_contents($des, $data);
    }
    return $file;
}

function json($data)
{
    header('application/json');
    return json_encode($data);
}

function _log($type = 'DEBUG', $text = "KOSONG")
{
    $log = array(
        'id' => null,
        'datetime' => date('Y-m-d H:i:s'),
        'type' => $type,
        'text' => $text);
    $db = new Database();
    $db->insert('log', $log);
}

function send($number, $text)
{
    $cmd = 'C:\Gammu\bin\gammu-smsd-inject.exe -c C:\Gammu\bin\smsdrc EMS "' . $number . '" -text "' . $text . '"';
    if (substr(php_uname(), 0, 7) == "Windows") {
        pclose(popen("start /B " . $cmd, "r"));
    } else {
        exec($cmd . " > /dev/null &");
    }
}

function kirim_email($row)
{
    foreach ($row as $field) {
        $message = '<html><body>';
        $message .= '<table rules="all" style="border:1px solid #666;" cellpadding="10">';
        $message .= "<tr style='background: #eee;'><td><strong>INSPECTION ID :</strong> </td><td>" . $field['localid'] . "</td></tr>";
        $message .= "<tr style='background: #eee;'><td><strong>EQUIPMENT ID :</strong> </td><td>" . $field['idequipment'] . "</td></tr>";
        $message .= "<tr style='background: #eee;'><td><strong>EQUIPMENT NAME :</strong> </td><td>" . $field['nama'] . "</td></tr>";
        $message .= "<tr style='background: #eee;'><td><strong>INSPECTOR :</strong> </td><td>" . $field['idinspector'] . "</td></tr>";
        $message .= "<tr style='background: #eee;'><td><strong>DINAS :</strong> </td><td>" . $field['dinas'] . "</td></tr>";
        $message .= "<tr style='background: #eee;'><td><strong>SHIFT :</strong> </td><td>" . $field['shift'] . "</td></tr>";
        $message .= "<tr style='background: #eee;'><td><strong>DATE INSPECT :</strong> </td><td>" . $field['localdt'] . "</td></tr>";
        $message .= "<tr style='background: #eee;'><td><strong>NOTE :</strong> </td><td>" . $field['note'] . "</td></tr>";
        $message .= "</table> <br />";
        $message .= '<table rules="all" style="border:1px solid #666;" cellpadding="10">';
        $message .= "<tr style='background: #eee;'><td>ID</td><td>NAME</td><td>VALUE</td><td>STATE</td></tr>";
        //$message .= json_encode($field['data']);
        $total = 0;
        for ($i = 0; $i < sizeof($field['data']); $i++) {
            $msgg = cek_min_max($field['data'][$i]['idparam'], $field['data'][$i]['value']);
            $txt = '';
            switch ($msgg) {
                case 0:
                    $txt = 'GOOD CONDITIONS';
                    break;
                case 1:
                    $txt = 'NO GOOD CONDITIONS';
                    break;
                case 2:
                    $txt = '';
                    break;
            }
            $total = $i;
            $message .= "<tr><td>" . $field['data'][$i]['idparam'] . "</td><td>" . $field['data'][$i]['label'] . "</td><td>" . $field['data'][$i]['value'] . "</td><td>" . $txt . "</td></tr>";
        }
        $message .= "</table>";
        $message .= "</body></html><br />";
    }

    $subject = 'NOTIFICATION EQUIPMENT INFORMATION SYSTEM';
    foreach (getPhone() as $phone) {
        try {
            send($phone['phone'], 'KODE : ' . $field['idequipment'] . ' MEMILIKI ' . $total . ' KONDISI DILUAR STANDAR');
            _log('SMS', 'SMS SENT ' . $phone['phone']);
        } catch (Exception $e) {
            _log('SMS', 'SMS ERROR ' . $e->getMessage());
        }

    }
    foreach (getEmail() as $email) {

        if (sendEmail($email['email'], $subject, $message, 'mesflat@krakatausteel.com')) {
            _log('EMAIL', 'EMAIL SENT ' . $email['email']);
            updateNotifStatus($row['id'], 2);
        } else {
            _log('EMAIL', 'SENDING FAILED ' . $email['email']);
            updateNotifStatus($row['id'], 3);
        }
    }
}

function updateNotifStatus($id, $status)
{
    $db = new Database();
    $db->where('id', $id);
    $db->update('tbnotif', array('status' => $status));
}

function sendEmail($to, $subject, $message, $sender)
{
    $mail = new PHPMailer(true);
    $mail->IsSMTP();

    $mail->Host = "10.10.9.42";
    $mail->Port = 587;
    $mail->SMTPSecure = "tls";
    $mail->From = "mesflat@krakatausteel.com";
    $mail->FromName = "INSPECTION UTILITY TOOLS";
    /**
     * $mail->SMTPAuth = true;
     * $mail->SMTPSecure = 'tls';//ssl
     * $mail->Port = intval(587);
     * $mail->Host = trim("smtp.gmail.com");
     * $mail->Username = trim($sender);
     * $mail->Password = $password;// SMTP server password*/
    //$mail->IsSendmail();  // tell the class to use Sendmail
    $mail->AddReplyTo(trim($sender), "CRM INSPECTION");
    $mail->From = trim($sender);
    $mail->FromName = "CRM INSPECTION";

    $mail->AddAddress($to);
    $mail->Subject = $subject;
    $mail->MsgHTML($message);
    $mail->IsHTML(true);
    try {
        if ($mail->Send()) {
            return true;
        } else {
            return false;
        }
    } catch (Exception $e) {
        _log('EMAIL', 'EMAIL ERROR ' . $e->getMessage());
    }

}

function uploadFile()
{
    if (!isset($_FILES['uploads'])) {
        echo "No files uploaded!!";
        return;
    }
    $imgs = array();

    $files = $_FILES['uploads'];
    $cnt = count($files['name']);

    for ($i = 0; $i < $cnt; $i++) {
        if ($files['error'][$i] === 0) {
            $name = uniqid('img-' . date('Ymd') . '-');
            if (move_uploaded_file($files['tmp_name'][$i], 'uploads/' . $name) === true) {
                $imgs[] = array('url' => '/uploads/' . $name, 'name' => $files['name'][$i]);
            }

        }
    }

    $imageCount = count($imgs);

    if ($imageCount == 0) {
        echo 'No files uploaded!!  <p><a href="/">Try again</a>';
        return;
    }

    $plural = ($imageCount == 1) ? '' : 's';

    foreach ($imgs as $img) {
        printf('%s <img src="%s" width="50" height="50" /><br/>', $img['name'], $img['url']);
    }
}
