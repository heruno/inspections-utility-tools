#!/usr/bin/env php
<?php
ini_set('max_execution_time', 300);
ini_set('session.gc_maxlifetime', 28800);
date_default_timezone_set('Asia/Jakarta');
define('ENVIRONMENT', 'development');
if (defined('ENVIRONMENT')) {
    switch (ENVIRONMENT) {
        case 'development':
            error_reporting(E_ALL ^ E_NOTICE);
            break;
        case 'testing':
        case 'production':
            error_reporting(0);
            break;
        default:
            exit('The application environment is not set correctly.');
    }
}
require __DIR__ . '/Phpmailer/class.phpmailer.php';
require __DIR__ . '/Database/Database.php';
require __DIR__ . '/Slim/Slim.php';

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();
$argv = $GLOBALS['argv'];
array_shift($GLOBALS['argv']);
$pathInfo = '/' . implode('/', $argv);

$app = new Slim\Slim([
    'debug' => false,
]);

$app->environment = Slim\Environment::mock([
    'PATH_INFO' => $pathInfo
]);

$app->notFound(function () use ($app) {
    $url = $app->environment['PATH_INFO'];
    echo "Error: Cannot route to $url";
    $app->stop();
});

$app->error(function (\Exception $e) use ($app) {
    echo $e;
    $app->stop();
});

$app->get('/job', 'job');
$app->run();

function job()
{
    $db = new Database();
    $db->where('status', 1);
    $results = $db->get('tbnotif');
    $x = 0;
    foreach ($results as $result) {
        $data[$x]['id'] = $result['id'];
        $data[$x]['email'] = $result['email'];
        $data[$x]['sms'] = $result['sms'];
        $data[$x]['data'] = json_decode($result['ilot'], true);
        $data[$x]['data']['data'] = json_decode($result['insp'], true);
        $i = 0;
        foreach ($data[$x]['data']['data'] as $row) {
            $data[$x]['data']['data'][$i++]['label'] = getLabel($row['data']['data']['idparam']);
        }
        $x++;
    }
    for ($i = 0; $i < sizeof($data); $i++) {
        kirim_email($data[$i]);
    }

}

function getLabel($id)
{
    $db = new Database();
    $db->where('id', $id);
    $row = $db->getOne('dd_attribute', 'label');
    return $row['label'];
}

function _sms()
{

}

function _mail($_data)
{
    foreach ($_data as $_row) {
        kirim_email($_row);
    }
}

function cek_min_max($id, $val)
{
    $count = 1;
    $db = new Database();
    $db->where('id', $id);
    $row = $db->get('dd_attribute');
    switch ($row[0]['flag_msg']) {
        case 0:
            $count = 2;
            break;
        case 1:
            if ($val == $row[0]['value_min'])
                $count = 0;
            break;
        case 2:
            if ($val > $row[0]['value_max'])
                $count = 0;
            if ($val < $row[0]['value_min'])
                $count = 0;
            break;
    }
    return $count;
}

function json($data)
{
    header('application/json');
    return json_encode($data);
}

function _log($type = 'DEBUG', $text = "KOSONG")
{
    $log = array(
        'id' => null,
        'datetime' => date('Y-m-d H:i:s'),
        'type' => $type,
        'text' => $text);
    $db = new Database();
    $db->insert('log', $log);
}

function send($number, $text)
{
    $cmd = 'C:\Gammu\bin\gammu-smsd-inject.exe -c C:\Gammu\bin\smsdrc EMS "' . $number . '" -text "' . $text . '"';
    if (substr(php_uname(), 0, 7) == "Windows") {
        pclose(popen("start /B " . $cmd, "r"));
    } else {
        exec($cmd . " > /dev/null &");
    }
}

function kirim_email($row)
{
    foreach ($row as $field) {
        echo $field['idequipment'];
        $message = '<html><head><meta content="text/html; charset=ISO-8859-1" http-equiv="content-type"><title></title></head><body>';
        $message .= '<table rules="all" style="border:1px solid #666; width: 100%;text-wrap: normal" cellpadding="5"><tbody>';
        $message .= '<tr style="background-color: silver;border-bottom: 5px"> <td style="vertical-align: top; font-weight: bold; text-align: center;vertical-align: middle;width: 20px;"> <small>NO</small> </td> <td style="vertical-align: top; font-weight: bold; text-align: center;vertical-align: middle; width: 108px;"> <small>WAKTU INSPEKSI</small> </td> <td style="vertical-align: top; font-weight: bold; text-align: center;vertical-align: middle; width: 120px;"> <small>INSPEKTOR</small> </td> <td style="vertical-align: top; font-weight: bold; text-align: center;vertical-align: middle;width: 80px;"> <small>LINE</small> </td> <td style="vertical-align: top; font-weight: bold; text-align: center;vertical-align: middle;width: 80px;"> <small>KODE EQUIPMENT</small> </td> <td style="vertical-align: top; font-weight: bold; text-align: center;vertical-align: middle; width: 150px;"> <small>NAME EQUIPMENT</small> </td> <td style="vertical-align: top; font-weight: bold; text-align: center;vertical-align: middle; width: 150px;"> <small>CEK LIST</small> </td> <td style="vertical-align: top; font-weight: bold; text-align: center;vertical-align: middle; width: 80px;"> <small>NILAI STANDAR</small> </td> <td style="vertical-align: top; font-weight: bold; text-align: center;vertical-align: middle; width: 80px;"> <small>HASIL INSPEKSI</small> </td> <td style="vertical-align: top; font-weight: bold; text-align: center;vertical-align: middle; width: 150px;"><small>CATATAN</small></td></tr>';
        $total = 0;
        $no = 0;
        for ($i = 0; $i < sizeof($field['data']); $i++) {
            $msgg = cek_min_max($field['data'][$i]['idparam'], $field['data'][$i]['value']);
            $txt = '';
            switch ($msgg) {
                case 0:
                    $txt = 'GOOD CONDITIONS';
                    break;
                case 1:
                    $txt = 'NO GOOD CONDITIONS';
                    break;
                case 2:
                    $txt = '';
                    break;
            }
            $total = $i;
            $message .= '<tr> <td style="vertical-align: middle; text-align: center">'.$no++.'</td> <td style="vertical-align: middle; text-align: center">'. $field['localdt'] .'</td> <td style="vertical-align: middle; text-align: center">'. $field['idinspector'] .'</td> <td style="vertical-align: middle; text-align: center">'. $field['dinas'] .'</td> <td style="vertical-align: middle; text-align: center">'. $field['idequipment'] .'</td> <td style="vertical-align: middle;">'. $field['nama'] .'</td> <td style="vertical-align: middle;">'. $field['data'][$i]['label'] .'</td> <td style="vertical-align: middle; text-align: center">'. $field['data'][$i]['value_standar'] .'</td> <td style="vertical-align: middle;text-align: center">'. $field['data'][$i]['value'] .'</td> <td style="vertical-align: middle;">'. $field['note'] .'</td> </tr>';
        }
        $message .= "</table>";
        $message .= "</body></html><br />";
    }

    $subject = 'NOTIFICATION EQUIPMENT INFORMATION SYSTEM';
    foreach (getPhone() as $phone) {
        try {
            /*send($phone['phone'], 'KODE : ' . $field['idequipment'] . ' MEMILIKI ' . $total . ' KONDISI DILUAR STANDAR');*/
            send($phone['phone'], 'Terdapat ' . $total . ' kondisi abnormal, Equipment ' . $field['idequipment'] . ', silahkan cek email untuk lebih detail. SMS dari Inspeksi CRM');
            _log('SMS', 'SMS SENT ' . $phone['phone']);
            echo 'SMS SENT ' . $phone['phone'];
        } catch (Exception $e) {
            _log('SMS', 'SMS ERROR ' . $e->getMessage());
            echo 'SMS ERROR ' . $e->getMessage();
        }

    }
    foreach (getEmail() as $email) {

        if (sendEmail($email['email'], $subject, $message, 'mesflat@krakatausteel.com')) {
            _log('EMAIL', 'EMAIL SENT ' . $email['email']);
            updateNotifStatus($row['id'], 2);
            echo 'EMAIL SENT ' . $email['email'];
        } else {
            _log('EMAIL', 'SENDING FAILED ' . $email['email']);
            updateNotifStatus($row['id'], 3);
            echo 'SENDING FAILED ' . $email['email'];
        }
    }
}

function getEmail()
{
    try {
        $db = new Database();
        $data = $db->get('mkaryawan', null, array('email'));
        return $data;
    } catch (exception $e) {
        return array("email" => "wolu.88@gmail.com");
    }
}

function getPhone()
{
    try {
        $db = new Database();
        $data = $db->get('mkaryawan', null, array('no_telp'));
        return $data;
    } catch (exception $e) {
        return array("phone" => "085697469122");
    }
}

function updateNotifStatus($id, $status)
{
    $db = new Database();
    $db->where('id', $id);
    $db->update('tbnotif', array('status' => $status));
}

function sendEmail($to, $subject, $message, $sender)
{
    $mail = new PHPMailer(true);
    $mail->IsSMTP();

    $mail->Host = "10.10.9.42";
    $mail->Port = 587;
    $mail->SMTPSecure = "tls";
    $mail->From = "mesflat@krakatausteel.com";
    $mail->FromName = "INSPECTION UTILITY TOOLS";
    /**
     * $mail->SMTPAuth = true;
     * $mail->SMTPSecure = 'tls';//ssl
     * $mail->Port = intval(587);
     * $mail->Host = trim("smtp.gmail.com");
     * $mail->Username = trim($sender);
     * $mail->Password = $password;// SMTP server password*/
    //$mail->IsSendmail();  // tell the class to use Sendmail
    $mail->AddReplyTo(trim($sender), "CRM INSPECTION");
    $mail->From = trim($sender);
    $mail->FromName = "CRM INSPECTION";

    $mail->AddAddress($to);
    $mail->Subject = $subject;
    $mail->MsgHTML($message);
    $mail->IsHTML(true);
    try {
        if ($mail->Send()) {
            return true;
        } else {
            return false;
        }
    } catch (Exception $e) {
        _log('EMAIL', 'EMAIL ERROR ' . $e->getMessage());
    }

}

function uploadFile()
{
    if (!isset($_FILES['uploads'])) {
        echo "No files uploaded!!";
        return;
    }
    $imgs = array();

    $files = $_FILES['uploads'];
    $cnt = count($files['name']);

    for ($i = 0; $i < $cnt; $i++) {
        if ($files['error'][$i] === 0) {
            $name = uniqid('img-' . date('Ymd') . '-');
            if (move_uploaded_file($files['tmp_name'][$i], 'uploads/' . $name) === true) {
                $imgs[] = array('url' => '/uploads/' . $name, 'name' => $files['name'][$i]);
            }

        }
    }

    $imageCount = count($imgs);

    if ($imageCount == 0) {
        echo 'No files uploaded!!  <p><a href="/">Try again</a>';
        return;
    }

    $plural = ($imageCount == 1) ? '' : 's';

    foreach ($imgs as $img) {
        printf('%s <img src="%s" width="50" height="50" /><br/>', $img['name'], $img['url']);
    }
}
