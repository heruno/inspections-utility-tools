<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/********************************************************************************************************/
$autoload['packages']   = array();
$autoload['libraries']  = array('MY_Session','database','session','template','auth');
$autoload['helper']     = array('url','file','date');
$autoload['config']     = array();
$autoload['language']   = array();
$autoload['model']      = array();
/********************************************************************************************************/
/* End of file autoload.php */
/* Location: ./application/config/autoload.php */