<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/********************************************************************************************************/
$config['base_url'] = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
$config['base_url'] .= '://' . $_SERVER['HTTP_HOST'];
$config['base_url'] .= str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);
/********************************************************************************************************/
$config['index_page'] = '';
$config['uri_protocol'] = 'AUTO';
$config['url_suffix'] = '.php';
$config['language'] = 'english';
$config['charset'] = 'UTF-8';
$config['enable_hooks'] = true;
$config['subclass_prefix'] = 'MY_';
$config['permitted_uri_chars'] = 'a-z 0-9~%.:_\-';
$config['allow_get_array'] = true;
$config['enable_query_strings'] = false; //false
$config['controller_trigger'] = 'c';
$config['function_trigger'] = 'm';
$config['directory_trigger'] = 'd'; // experimental not currently in use
$config['log_threshold'] = 1;
$config['log_path'] = 'application/logs/';
$config['log_date_format'] = 'Y-m-d H:i:s';
$config['cache_path'] = 'application/cache/';
$config['encryption_key'] = 'heruno_utomo_18_02_1985';
/********************************************************************************************************/
$config['sess_cookie_name'] = 'krakatau_steel';
$config['sess_expiration'] = time() + 10000000; //0;//7200//
$config['sess_expire_on_close'] = true;
$config['sess_encrypt_cookie'] = true;
$config['sess_use_database'] = true;
$config['sess_table_name'] = 'ci_sessions';
$config['sess_match_ip'] = false;
$config['sess_match_useragent'] = true;
$config['sess_time_to_update'] = 300; //300
/********************************************************************************************************/
$config['cookie_prefix'] = "";
$config['cookie_domain'] = "";
$config['cookie_path'] = "/";
$config['cookie_secure'] = false;
/********************************************************************************************************/
$config['global_xss_filtering'] = false;
$config['csrf_protection'] = false;
$config['csrf_token_name'] = 'csrf_token_test_name'; //csrf_test_name
$config['csrf_cookie_name'] = 'csrf_cookie_simkes_online'; //csrf_cookie_name
$config['csrf_expire'] = 7200;
$config['compress_output'] = false; //heruno before false
$config['time_reference'] = 'local'; //befor local update by heruno
$config['rewrite_short_tags'] = false;
$config['proxy_ips'] = '';
/* End of file config.php */
/* Location: ./application/config/config.php */
