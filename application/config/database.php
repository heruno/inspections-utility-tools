<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
$db['simkes']['hostname'] = 'localhost';
$db['simkes']['username'] = 'app';
$db['simkes']['password'] = 'app';
$db['simkes']['database'] = 'simkes';
$db['simkes']['dbdriver'] = 'mysqli';
$db['simkes']['dbprefix'] = '';
$db['simkes']['pconnect'] = TRUE;
$db['simkes']['db_debug'] = FALSE;
$db['simkes']['cache_on'] = FALSE;
$db['simkes']['cachedir'] = APPPATH.'cache/db/';
$db['simkes']['char_set'] = 'utf8';
$db['simkes']['dbcollat'] = 'utf8_unicode_ci';
$db['simkes']['swap_pre'] = '';
$db['simkes']['autoinit'] = TRUE;
$db['simkes']['stricton'] = FALSE;

/* End of file database.php */
/* Location: ./application/config/database.php */
$ini_array     = parse_ini_file("config.ini", true);
$active_group  = $ini_array['database']['active_group'];
$query_builder = $ini_array['database']['query_builder'];
$db[$active_group]  = $ini_array[$active_group];