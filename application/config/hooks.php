<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$hook['display_override'][] = array(
	'class' => '',
	'function' => 'compress',
	'filename' => 'compress.php',
	'filepath' => 'hooks'
	);
/**
$hook['post_controller'][] = array(
    'class' => 'AuthHooks',
    'function' => 'authHooks',
    'filename' => 'AuthHooks.php',
    'filepath' => 'hooks'
);
$hook['post_controller_constructor'][] = array(
    'class'    => 'ProfilerEnabler',
    'function' => 'enableProfiler',
    'filename' => 'hooks.profiler.php',
    'filepath' => 'hooks',
    'params'   => array()
);
 * */
/* End of file hooks.php */
/* Location: ./application/config/hooks.php */