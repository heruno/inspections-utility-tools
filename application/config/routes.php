<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
session_start();
//$route['default_controller'] = "users";

$route['default_controller'] = "welcome";
$route['translate_uri_dashes'] = TRUE;

$route['about']  = 'welcome/about';
$route['help']   = 'welcome/help';
$route['logout'] = 'users/login/logout';
$route['users/logout'] = 'users/login/logout';
/**
$route['login']  = 'users/login/index';
$route['login_proses']  = 'users/login/proses';

*/
$route[md5('welcome')] = "welcome";
$route[md5('welcome')] = "welcome/index";
$route['404_override'] = 'error';
/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/
$route['api/example/users/(:num)'] = 'api/example/users/id/$1'; // Example 4
$route['api/example/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/example/users/id/$1/format/$3$4'; // Example 8

require_once( BASEPATH .'database/DB'. EXT );
$db =& DB();

$db->distinct('a.* , b.text, b.url');
$db->select();
$db->from('users_menu_roles_akses a');
$db->join('users_menu b', 'a.menu_id = b.menuId');
//$cookie = str_replace('180285', $_COOKIE['id_roles']);
//log_message('error','$cookie   '.$cookie);
if (isset($_COOKIE['8sois66g7dkh2turi94c0b9g91'])) {
    $db->where('roles_id', $_COOKIE['8sois66g7dkh2turi94c0b9g91']);
}elseif (isset($_SESSION['8sois66g7dkh2turi94c0b9g91'])){
    $db->where('roles_id', $_SESSION['8sois66g7dkh2turi94c0b9g91']);
}
$query = $db->get();
$result = $query->result();
//log_message('error', json_encode($result));
foreach ($result as $row) {
    if ($row->read == 0) {
        $route[$row->url . '/read']     = 'error';
        $route[$row->url . '/read_all'] = 'error';
        $route[$row->url . '/read_one'] = 'error';
        $route[$row->url]               = 'error';  
    }
    if ($row->add == 0) {
        $route[$row->url . '/create'] = 'error/add';
    }
    if ($row->edit == 0) {
        $route[$row->url . '/update'] = 'error/edit';
    }
    if ($row->delete == 0) {
        $route[$row->url . '/delete'] = 'error/delete';
    }
}
/* End of file routes.php */
/* Location: ./application/config/routes.php */