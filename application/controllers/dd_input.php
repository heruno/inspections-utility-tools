<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Controller dd_input
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 *
 *
 */
class Dd_input extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('dd_input_model');
    }
    public function read()
    {
        $this->json($this->dd_input_model->read());
    }
    public function read_one()
    {
        $this->json($this->dd_input_model->read_one());
    }
    public function read_all()
    {
        $this->json($this->dd_input_model->read_all());
    }
}