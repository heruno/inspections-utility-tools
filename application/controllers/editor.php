<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Editor extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

    }
    function index(){
         $this->template->display('editor');
    }
    function template(){
        $this->template->display('template');
    }
}