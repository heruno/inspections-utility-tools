<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Controller equipment
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 *
 *
 */
class Equipment extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('equipment_model');
    }

    public function index()
    {
        $this->template->display('equipment/equipment');
    }

    public function read()
    {
        $this->json($this->equipment_model->read());
    }

    public function read_one()
    {
        $this->json($this->equipment_model->read_one());
    }

    public function read_all()
    {
        $this->json($this->equipment_model->read_all());
    }

    public function create()
    {
        $result = $this->equipment_model->insert();
        if ($result == 1) {
            $this->json(json_encode(array('successful' => 'Insert Success')));
        }else if($this->equipment_model->unggahx()){
            $this->json(json_encode(array('successful' => 'Upload Success')));
        } else {
            $this->json(json_encode(array('unsuccessful' => 'Insert Error :' . $this->db->_error_number() . '  : ' . $this->db->_error_message())));
        }
    }

    public function update()
    {
        $result = $this->equipment_model->update();
        if ($result == 1) {
            $this->json(json_encode(array('successful' => 'Update Success')));
        } else if($this->equipment_model->unggahx()){
            $this->json(json_encode(array('successful' => 'Upload Success')));
        } else {
            $this->json(json_encode(array('unsuccessful' => 'Update Error')));
        }
    }

    public function delete()
    {
        $result = $this->equipment_model->delete();
        if ($result == 1) {
            $this->json(json_encode(array('successful' => 'Delete success')));
        } else {
            $this->json(json_encode(array('unsuccessful' => 'Delete Error :' . $this->db->_error_number() . ' : ' . $this->db->_error_message())));
        }
    }

    public function unggah()
    {
        $pesan = '';
        $target_dir = $destination_path = getcwd() . DIRECTORY_SEPARATOR . 'assets\\images\\upload\\';
        $target_file = $target_dir . basename($_FILES["file"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["file"]["tmp_name"]);
        if ($check !== false) {
            $pesan = "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            $pesan = "File is not an image.";
            $uploadOk = 0;
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            $pesan = "Sorry, file already exists.";
            $uploadOk = 0;
        }
        // Check file size
        if ($_FILES["file"]["size"] > 5000000) {
            $pesan = "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            $pesan = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $pesan = "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
        } else {
            if (@move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
                $pesan = basename($_FILES["file"]["name"]);
            } else {
                $pesan = "Sorry, there was an error uploading your file.";
            }
        }
        return $pesan;
    }
}
/* End of file equipment.php */
/* Location: ./application/controllers/equipment.php */