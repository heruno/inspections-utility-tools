<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Error extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

    }
    function index(){
         $this->template->display('error');
    }
    function add(){
        $this->json(json_encode(array('unsuccessful' => 'Anda Tidak memiliki Hak Akses untuk Tambah Data')));
    }
    function edit(){
        $this->json(json_encode(array('unsuccessful' => 'Anda Tidak memiliki Hak Akses untuk Update Data')));
    }
    function delete(){
        $this->json(json_encode(array('unsuccessful' => 'Anda Tidak memiliki Hak Akses untuk Hapus Data')));
    }
}