<?php
/**
 * User: heruno
 * Date: 01/10/2017
 * Time: 1:31
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Files extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function test()

    {
        //$vendorDir = dirname(dirname(__FILE__));
        //$baseDir = dirname($vendorDir);
        /**
         * $baseDir[] = __LINE__;
         * $baseDir[] =__FILE__;
         * $baseDir[] = __FUNCTION__;
         * $baseDir[] =__CLASS__;
         * $baseDir[] = __METHOD__;
         * $baseDir[] = __DIR__;
         * $baseDir[] = scandir(__DIR__);
         * $baseDir[] = scandir(__DIR__,1);
         * $baseDir[] = scandir(__DIR__,2);
         * $baseDir[] = APPPATH;
         * $baseDir[] = BASEPATH;
         */
        //$baseDir[] =__CLASS__;
        //$baseDir[] = __DIR__;
        foreach ($this->find_all_files(APPPATH.'/controllers') as $row) {
            $class = array_keys($row)[0];
            log_message('error','$class =>'.json_encode($row));
            foreach ($row as $key=>$val){
                foreach ($val as $method){
                    $baseDir[] =(string)"$class/$method";
                }
            }
        }
        $this->json(json_encode($baseDir, JSON_PRETTY_PRINT));
    }

    function find_all_files($dir)
    {
        $root = scandir($dir);
        $result = array();
        foreach ($root as $value) {
            if ($value === '.' || $value === '..') {
                continue;
            }
            if (is_file("$dir/$value")) {
                $path = "$dir\\$value";
                $result[] = $this->file_get_php_classes($path);
                continue;
            }
            foreach ($this->find_all_files("$dir\\$value") as $value) {
                $result[] = $value;
            }
        }
        return $result;
    }

    function file_get_php_classes($filepath,$onlypublic=true) {
        $php_code = file_get_contents($filepath);
        $classes = $this->get_php_classes($php_code, $onlypublic);
        return $classes;
    }

    function get_php_classes($php_code,$onlypublic) {
        $classes = array();
        $methods=array();
        $tokens = token_get_all($php_code);
        $count = count($tokens);
        for ($i = 2; $i < $count; $i++) {
            if ($tokens[$i - 2][0] == T_CLASS
                && $tokens[$i - 1][0] == T_WHITESPACE
                && $tokens[$i][0] == T_STRING) {
                $class_name = $tokens[$i][1];
                $methods[$class_name] = array();
            }
            if ($tokens[$i - 2][0] == T_FUNCTION
                && $tokens[$i - 1][0] == T_WHITESPACE
                && $tokens[$i][0] == T_STRING) {
                if ($onlypublic) {
                    if ( !in_array($tokens[$i-4][0],array(T_PROTECTED, T_PRIVATE))) {
                        $method_name = $tokens[$i][1];
                        $methods[$class_name][] = $method_name;
                    }
                } else {
                    $method_name = $tokens[$i][1];
                    $methods[$class_name][] = $method_name;
                }
            }
        }
        return $methods;
    }

}