<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Generate extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

    }
    function index(){
        $this->template->display('generator');
    }
    /**
    function generate()
    {
        try {
            $this->db->select('idequipment,uid,name,dinas');
            $data = $this->db->get('tbequipment');
            $totalUp = 0;
            $totalIn = 0;
            foreach ($data->result_array() as $rows) {
                $d = $this->generateOnline($rows['idequipment']);
                $count = $this->countTemplate($rows['idequipment']);
                if ($count > 0) {
                    $this->db->where('kode', $rows['idequipment']);
                    $this->db->update('template', array(
                        'kode' => $rows['idequipment'],
                        'uid' => $rows['uid'],
                        'dinas' => $rows['dinas'],
                        'header' => $d['header'],
                        'transform' => json_encode($d['transform']),
                        'data' => json_encode($d['data']),
                        'version' => date("Y-m-d H:i:s")
                    ));
                    $totalUp++;
                } else {
                    $this->db->insert('template', array(
                        'kode' => $rows['idequipment'],
                        'uid' => $rows['uid'],
                        'dinas' => $rows['dinas'],
                        'header' => $d['header'],
                        'transform' => json_encode($d['transform']),
                        'data' => json_encode($d['data']),
                        'version' => date("Y-m-d H:i:s")
                    ));
                    $totalIn++;
                }
            }
            echo "Berhasil Insert: $totalIn Update : $totalUp";
        } catch (exception $e) {
            return json(array('status' => 'error', 'msg' => $e->getMessage()));
        }
    }

    function generateOnline($id)
    {
        $ip = $_SERVER['SERVER_ADDR'];
        $idequipment = $id;
        try {
            $header = $this->getHeader($idequipment);
            if (sizeof($header > 0)) {
                $template = getTemplate('form-inspect-3');
                $data = array();
                $hasil1 = array(
                    "idequipment" => $header[0]['idequipment'],
                    "id" => 'idequipment',
                    "type" => "text",
                    "label" => "<b>Equipment Code</b>",
                    "class" => "easyui-textbox",
                    "name" => "idequipment",
                    "value" => $header[0]['idequipment'],
                    "data-options" => "readonly:true",
                    "style" => "width:100%");
                array_push($data, $hasil1);
                foreach (getByKatalog($idequipment) as $row) {
                    array_push($data, $row);
                }
                $result['header'] = $header[0]['name'];
                $result['transform'] = $template[0]['text'];
                $result['data'] = $data;
                return $result;
            }
        } catch (exception $e) {
            return $e->getMessage();
        }
    }

    function getTemplate($id)
    {
        $this->db = new Database();
        $this->db->where('id', $id);
        $hasil = $this->db->get('dd_template');
        return $hasil->result_array();
    }

    function getHeader($id)
    {
        $this->db->where('idequipment', $id);
        $hasil = $this->db->get('tbequipment');
        return $hasil->result_array();
    }*/
}