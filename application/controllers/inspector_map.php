<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Inspector_map extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->template->display('inspector_map/inspector_param');
    }

    public function create_batch()
    {
        $data = file_get_contents('php://input');
        $data = json_decode($data, true);
        $this->db->insert_batch('inspector_maps', $data);
        $result = $this->db->affected_rows();
        $this->output->set_output(json_encode(array('msg' => $result)));

    }

    public function delete_batch()
    {
        $data = file_get_contents('php://input');
        $data = json_decode($data, true);
        foreach ($data as $d) {
            $this->db->where('nik', $d['nik']);
            $this->db->where('id', $d['id']);
            $this->db->delete('inspector_maps');
        }
        $result = $this->db->affected_rows();
        $this->output->set_output(json_encode(array('msg' => $result)));
    }

    public function getMap()
    {
        $this->filter();
        $this->_order = 'asc';
        $this->_sort = 'id';
        $this->db->select('*');
        $this->db->where('nik', $this->uri->segment(3));
        $order = $this->input->post('order') ? $this->input->post('order') : $this->_order;
        $sort = $this->input->post('sort') ? $this->input->post('sort') : $this->_sort;
        $this->db->order_by($sort, $order);
        $result = $this->db->get('inspector_maps_vd');
        $this->output->set_output(json_encode($result->result_array()));
    }

    public function filter()
    {
        $before = array('"[', ']"', '\"');
        $after = array('[', ']', '"');
        if ($this->input->post('filterRules')) {
            $data = str_replace($before, $after, json_encode($this->input->post('filterRules')));
            foreach (json_decode($data, true) as $row) {
                $this->db->like($row['field'], $row['value']);
            }
        }
    }
}