<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Controller dinas
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 *
 *
 */
class Dinas extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('dinas_model');
    }
    public function index()
    {
        $data['form'] = 'dinas_form';
	   $data['datagrid'] = 'dinas_datagrid';
       $this->template->display('master/index',$data);
    }
    public function read()
    {
        $this->json($this->dinas_model->read());
    }
    public function read_one()
    {
        $this->json($this->dinas_model->read_one());
    }
    public function read_all()
    {
        $this->json($this->dinas_model->read_all());
    }
    public function create()
    {
        $result = $this->dinas_model->insert();
        if ($result == 1)
        {
            $this->json(json_encode(array('successful' => 'Insert Success')));
        } else
        {
            $this->json(json_encode(array('unsuccessful' => 'Insert Error :' . $this->db->
                    _error_number() . '  : ' . $this->db->_error_message())));
        }
    }
    public function update()
    {
        $result = $this->dinas_model->update();
        if ($result == 1)
        {
            $this->json(json_encode(array('successful' => 'Update Success')));
        } else
        {
            $this->json(json_encode(array('unsuccessful' => 'Update Error :' . $this->db->
                    _error_number() . ' : ' . $this->db->_error_message())));
        }
    }
    public function delete()
    {
        $result = $this->dinas_model->delete();
        if ($result == 1)
        {
            $this->json(json_encode(array('successful' => 'Delete success')));
        } else
        {
            $this->json(json_encode(array('unsuccessful' => 'Delete Error :' . $this->db->
                    _error_number() . ' : ' . $this->db->_error_message())));
        }
    }
}
/* End of file dinas.php */
/* Location: ./application/controllers/dinas.php */