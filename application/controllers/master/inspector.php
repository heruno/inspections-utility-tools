<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Controller inspector
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 *
 *
 */
class Inspector extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('inspector_model');
    }
    public function index()
    {
       $data['form'] = 'inspector_form';
	   $data['datagrid'] = 'inspector_datagrid';
       $this->template->display('master/index',$data);
    }
    public function read()
    {
        $this->json($this->inspector_model->read());
    }
    public function read_one()
    {
        $this->json($this->inspector_model->read_one());
    }
    public function read_all()
    {
        $this->json($this->inspector_model->read_all());
    }
    public function create()
    {
        $result = $this->inspector_model->insert();
        if ($result == 1)
        {
            $this->json(json_encode(array('successful' => 'Insert Success')));
        } else
        {
            $this->json(json_encode(array('unsuccessful' => 'Insert Error :' . $this->db->
                    _error_number() . '  : ' . $this->db->_error_message())));
        }
    }
    public function update()
    {
        $result = $this->inspector_model->update();
        if ($result == 1)
        {
            $this->json(json_encode(array('successful' => 'Update Success')));
        } else
        {
            $this->json(json_encode(array('unsuccessful' => 'Update Error :' . $this->db->
                    _error_number() . ' : ' . $this->db->_error_message())));
        }
    }
    public function delete()
    {
        $result = $this->inspector_model->delete();
        if ($result == 1)
        {
            $this->json(json_encode(array('successful' => 'Delete success')));
        } else
        {
            $this->json(json_encode(array('unsuccessful' => 'Delete Error :' . $this->db->
                    _error_number() . ' : ' . $this->db->_error_message())));
        }
    }
}
/* End of file inspector.php */
/* Location: ./application/controllers/inspector.php */