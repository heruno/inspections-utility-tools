<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Controller line
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 *
 *
 */
class Line extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('line_model');
    }
    public function index()
    {
        $data['form'] = 'line_form';
	   $data['datagrid'] = 'line_datagrid';
       $this->template->display('master/index',$data);
    }
    public function read()
    {
        $this->json($this->line_model->read());
    }
    public function read_one()
    {
        $this->json($this->line_model->read_one());
    }
    public function read_all()
    {
        $this->json($this->line_model->read_all());
    }
    public function create()
    {
        $result = $this->line_model->insert();
        if ($result == 1)
        {
            $this->json(json_encode(array('successful' => 'Insert Success')));
        } else
        {
            $this->json(json_encode(array('unsuccessful' => 'Insert Error :' . $this->db->
                    _error_number() . '  : ' . $this->db->_error_message())));
        }
    }
    public function update()
    {
        $result = $this->line_model->update();
        if ($result == 1)
        {
            $this->json(json_encode(array('successful' => 'Update Success')));
        } else
        {
            $this->json(json_encode(array('unsuccessful' => 'Update Error :' . $this->db->
                    _error_number() . ' : ' . $this->db->_error_message())));
        }
    }
    public function delete()
    {
        $result = $this->line_model->delete();
        if ($result == 1)
        {
            $this->json(json_encode(array('successful' => 'Delete success')));
        } else
        {
            $this->json(json_encode(array('unsuccessful' => 'Delete Error :' . $this->db->
                    _error_number() . ' : ' . $this->db->_error_message())));
        }
    }
}
/* End of file line.php */
/* Location: ./application/controllers/line.php */