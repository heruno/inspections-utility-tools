<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/*
 * Controller mkaryawan
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 *
 *
 */
class Mkaryawan extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('mkaryawan_model');
    }

    public function index(){
	   $data['form'] = 'mkaryawan_form';
	   $data['datagrid'] = 'mkaryawan_datagrid';
       $this->template->display('master/index',$data);
    }

    public function read(){
		$this->output->set_output(
        $this->mkaryawan_model->read()
        );
    }

    public function read_one(){
		$this->output->set_output(
        $this->mkaryawan_model->read_one()
        );
    }
    public function read_all(){
      $this->output->set_output(
      $this->mkaryawan_model->read_all()
      );
    }

    public function create(){
        log_message('error', 'tamnbah karyawan  ');
       $result = $this->mkaryawan_model->insert();
       if($result == 1){
		$this->output->set_output(json_encode(array('successful' => 'Insert Success')));
        }else{
		$this->output->set_output(json_encode(array('unsuccessful' => 'Insert Error :'.$this->db->_error_number().' : '.$this->db->_error_message())));
        }
    }

    public function update(){
       $result = $this->mkaryawan_model->update();
        if ($result == 1)
        {
            $this->json(json_encode(array('successful' => 'Update Success')));
        } else
        {
            $this->json(json_encode(array('unsuccessful' => 'Update Error :' . $this->db->
                    _error_number() . ' : ' . $this->db->_error_message())));
        }
    }

    public function delete(){
        log_message('error', 'tamnbah karyawan  ');
       $result = $this->mkaryawan_model->delete();
        if ($result == 1)
        {
            $this->json(json_encode(array('successful' => 'Delete success')));
        } else
        {
            $this->json(json_encode(array('unsuccessful' => 'Delete Error :' . $this->db->
                    _error_number() . ' : ' . $this->db->_error_message())));
        }
    }
}
/* End of file mkaryawan.php */
/* Location: ./application/controllers/mkaryawan.php */