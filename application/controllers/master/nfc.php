<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Controller unit
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 *
 *
 */
class Nfc extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('nfc_model');
    }

    public function index()
    {
        $data['form'] = 'nfc_form';
        $data['datagrid'] = 'nfc_datagrid';
        $this->template->display('master/index', $data);
    }

    public function read()
    {
        $this->json($this->nfc_model->read());
    }

    public function read_one()
    {
        $this->json($this->nfc_model->read_one());
    }

    public function read_all()
    {
        $this->json($this->nfc_model->read_all());
    }

    public function create()
    {
        $result = $this->nfc_model->insert();
        if ($result == 1) {
            $this->json(json_encode(array('successful' => 'Insert Success')));
        } else {
            $this->json(json_encode(array('unsuccessful' => 'Insert Error :' . $this->db->
                _error_number() . '  : ' . $this->db->_error_message())));
        }
    }

    public function update()
    {
        $result = $this->nfc_model->update();
        if ($result == 1) {
            $this->json(json_encode(array('successful' => 'Update Success')));
        } else {
            $this->json(json_encode(array('unsuccessful' => 'Update Error :' . $this->db->
                _error_number() . ' : ' . $this->db->_error_message())));
        }
    }

    public function delete()
    {
        $result = $this->nfc_model->delete();
        if ($result == 1) {
            $this->json(json_encode(array('successful' => 'Delete success')));
        } else {
            $this->json(json_encode(array('unsuccessful' => 'Delete Error :' . $this->db->
                _error_number() . ' : ' . $this->db->_error_message())));
        }
    }

    public function unggah()
    {
        $result = $this->nfc_model->unggah();
        if ($result) {
            $this->json(json_encode(array(
                'successful' => 'Upload Berhasil',
                'data' => $result
            )));
        } else {
            $this->json(json_encode(array(
                'unsuccessful' => $result,
                'data' => $result
            )));
        }
    }

    function curl_get_contents($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    function imageToBase64($image)
    {
        $imageData = base64_encode($this->curl_get_contents($image));
        $mime_types = array(
            'pdf' => 'application/pdf',
            'doc' => 'application/msword',
            'odt' => 'application/vnd.oasis.opendocument.text ',
            'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'gif' => 'image/gif',
            'jpg' => 'image/jpg',
            'jpeg' => 'image/jpeg',
            'png' => 'image/png',
            'bmp' => 'image/bmp'
        );
        $ext = pathinfo($image, PATHINFO_EXTENSION);
        if (array_key_exists($ext, $mime_types)) {
            $a = $mime_types[$ext];
        }
        return 'data:'. $a .';base64,' . $imageData;
    }
    function getImage($filename)
    {
        $image = new Imagick();
        $image->readImage($filename);
        $image->setImageFormat("png");
        header("Content-type: image/jpeg");
        echo $image->getImageBlob();
    }
    function generateImg64()
    {
        ini_set('memory_limit', '1024M');
        ini_set('allow_url_fopen ', 'open');
        $result = $this->db->get('nfc');
        foreach ($result->result_array() as $row) {
            $path = getcwd() . DIRECTORY_SEPARATOR . 'assets\\images\\upload\\';
            if (file_exists($path.$row['image'])) {
                $target_file = 'http://localhost/equipment/assets/images/upload/' .$row['image'];
                $this->db->where(array(
                    'nomor'=>$row['nomor']
                ))->update('nfc', array(
                    'image64' => $this->imageToBase64($target_file)
                ));
                echo $this->db->affected_rows();
                echo '<br />';
            }

        }
    }
    function convert()
    {
        $path = getcwd() . DIRECTORY_SEPARATOR . 'assets\\images\\upload\\old';
        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
        foreach ($iterator as $file) {
            if (!$file->isDir()) {
                $f = basename($file);
                echo "IECapt --url=http://localhost/equipment/master/nfc/image/$f --out=$f";
                echo '<br />';
                // exec($url);
            }
        }
    }
    function resizeImage($imagePath, $width, $height, $filterType, $blur, $bestFit, $cropZoom) {
        $imagick = new Imagick(realpath($imagePath));
        $imagick->resizeImage($width, $height, $filterType, $blur, $bestFit);
        $cropWidth = $imagick->getImageWidth();
        $cropHeight = $imagick->getImageHeight();
        if ($cropZoom) {
            $newWidth = $cropWidth / 2;
            $newHeight = $cropHeight / 2;

            $imagick->cropimage(
                $newWidth,
                $newHeight,
                ($cropWidth - $newWidth) / 2,
                ($cropHeight - $newHeight) / 2
            );
            $imagick->scaleimage(
                $imagick->getImageWidth() * 4,
                $imagick->getImageHeight() * 4
            );
        }
        header("Content-Type: image/jpg");
        echo $imagick->getImageBlob();
    }
    function image($img){
        echo '<html><head><meta name="viewport" content="width=device-width, minimum-scale=0.1"><title>Tag046.jpg (1028×786)</title></head><body style="margin: 0px; background: #0e0e0e;"><img style="-webkit-user-select: none;background-position: 0px 0px, 10px 10px;background-size: 20px 20px;background-image:linear-gradient(45deg, #eee 25%, transparent 25%, transparent 75%, #eee 75%, #eee 100%),linear-gradient(45deg, #eee 25%, white 25%, white 75%, #eee 75%, #eee 100%);cursor: zoom-in;" src="'.base_url('assets/images/upload/old').'/'.$img.'" width="782" height="598"></body></html>';
    }
}
/* End of file nfc.php */
/* Location: ./application/controllers/nfc.php */