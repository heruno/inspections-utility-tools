<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Controller notification
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 *
 *
 */
class Notification extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('notification_model');
    }

    public function index()
    {
        $data['form'] = 'notification_form';
        $data['datagrid'] = 'notification_datagrid';
        $this->template->display('master/index', $data);
    }

    public function read()
    {
        $this->json($this->notification_model->read());
    }

    public function read_one()
    {
        $this->json($this->notification_model->read_one());
    }

    public function read_all()
    {
        $this->json($this->notification_model->read_all());
    }

    public function create()
    {
        $result = $this->notification_model->insert();
        if ($result == 1) {
            $this->json(json_encode(array('successful' => 'Insert Success')));
        } else {
            $this->json(json_encode(array('unsuccessful' => 'Insert Error :' . $this->db->
                _error_number() . '  : ' . $this->db->_error_message())));
        }
    }

    public function update()
    {
        $result = $this->notification_model->update();
        if ($result == 1) {
            $this->json(json_encode(array('successful' => 'Update Success')));
        } else {
            $this->json(json_encode(array('unsuccessful' => 'Update Error :' . $this->db->
                _error_number() . ' : ' . $this->db->_error_message())));
        }
    }

    public function delete()
    {
        $result = $this->notification_model->delete();
        if ($result == 1) {
            $this->json(json_encode(array('successful' => 'Delete success')));
        } else {
            $this->json(json_encode(array('unsuccessful' => 'Delete Error :' . $this->db->
                _error_number() . ' : ' . $this->db->_error_message())));
        }
    }
}
/* End of file notification.php */
/* Location: ./application/controllers/notification.php */