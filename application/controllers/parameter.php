<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Controller parameter
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 *
 *
 */
class Parameter extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('parameter_model');
    }
    public function index()
    {
        $data['form'] = 'parameter_form';
        $data['datagrid'] = 'parameter_datagrid';
        $this->template->display('attribute/index', $data);
    }
    public function read()
    {
        $this->json($this->parameter_model->read());
    }
    public function read_one()
    {
        $this->json($this->parameter_model->read_one());
    }
    public function read_all()
    {
        $this->json($this->parameter_model->read_all());
    }
    public function create()
    {
        $result = $this->parameter_model->insert();
        if ($result == 1)
        {
            $this->json(json_encode(array('successful' => 'Insert Success')));
        } else
        {
            $this->json(json_encode(array('unsuccessful' => 'Insert Error :' . $this->db->
                    _error_number() . '  : ' . $this->db->_error_message())));
        }
    }
    public function update()
    {
        $result = $this->parameter_model->update();
        if ($result == 1)
        {
            $this->json(json_encode(array('successful' => 'Update Success')));
        } else
        {
            $this->json(json_encode(array('unsuccessful' => 'Update Error' )));
        }
    }
    public function delete()
    {
        $result = $this->parameter_model->delete();
        if ($result == 1)
        {
            $this->json(json_encode(array('successful' => 'Delete success')));
        } else
        {
            $this->json(json_encode(array('unsuccessful' => 'Delete Error :' . $this->db->
                    _error_number() . ' : ' . $this->db->_error_message())));
        }
    }
}
/* End of file parameter.php */
/* Location: ./application/controllers/parameter.php */