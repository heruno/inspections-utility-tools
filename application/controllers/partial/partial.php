<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
* Controller Partial
* @created on : 2014-09-24 09:06:14
* @author Heruno Utomo <wolu.88@gmail.com>
* Copyright 2014
*
*
*/
class Partial extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

    }
    function get_roles($id){
        $this->db->where('roles_id', $id);
        $result = $this->db->get('roles');
        $roles =  $result->row_array();
        return $roles['name'];
    }
    public function smenu()
    {
        $this->load->model('partial/tree_model', 'tree');
        return $this->tree->show_menu();
    }
    public function welcome(){
        $this->template->display('partial/welcome');
    }
    public function tree(){
        $data = array(
        'roles' =>$this->get_roles($this->session->userdata('id_roles')),
        'title' => 'SIMKES ONLINE v0.1.0',
        'tree'=> $this->menu());
        $this->template->display('partial/tree',$data);
    }
    public function header()
    {
        $this->template->display('partial/header');
    }
    public function footer()
    {
        $this->template->display('partial/footer');
    }
    public function menu()
    {
        $this->load->model('partial/tree_model', 'tree');
        return $this->tree->show_menu();
    }
    /**
    public function menu()
    {
        $this->load->model('partial/tree_model', 'tree');
        $this->output
        ->set_content_type('application/json')
        ->set_output($this->tree->show_menu());
    }*/
    public function update()
    {
        $this->load->model('partial/tree_model', 'tree');
        $result = $this->tree->dnd_update();
        if ($result == 1)
        {
            return true;
        } else
        {
            return false;
        }
    }
}
