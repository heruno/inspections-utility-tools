<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Controller photo
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 *
 *
 */
class Photo extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('photo_model');
    }
    public function read()
    {
        $this->json($this->photo_model->read());
    }
    public function read_one()
    {
        $this->json($this->photo_model->read_one());
    }
    public function read_all()
    {
        $this->json($this->photo_model->read_all());
    }
    public function create()
    {
        $result = $this->photo_model->insert();
        if ($result == 1)
        {
            $this->json(json_encode(array('successful' => 'Insert Success')));
        } else
        {
            $this->json(json_encode(array('unsuccessful' => 'Insert Error :' . $this->db->
                    _error_number() . '  : ' . $this->db->_error_message())));
        }
    }
    public function update()
    {
        $result = $this->photo_model->update();
        if ($result == 1)
        {
            $this->json(json_encode(array('successful' => 'Update Success')));
        } else
        {
            $this->json(json_encode(array('unsuccessful' => 'Update Error :' . $this->db->
                    _error_number() . ' : ' . $this->db->_error_message())));
        }
    }
    public function delete()
    {
        $result = $this->photo_model->delete();
        if ($result == 1)
        {
            $this->json(json_encode(array('successful' => 'Delete success')));
        } else
        {
            $this->json(json_encode(array('unsuccessful' => 'Delete Error :' . $this->db->
                    _error_number() . ' : ' . $this->db->_error_message())));
        }
    }
}
/* End of file photo.php */
/* Location: ./application/controllers/photo.php */