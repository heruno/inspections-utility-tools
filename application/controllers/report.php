<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Controller equipment
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 *
 *
 */
class Report extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('report_model');
    }
    public function generatexls(){
        $this->load->library('excelgen');
        $this->excelgen
        ->set_query($this->report_model->read_xls())
        ->set_column(array('inspectionid', 'localdt', 'inputdttm', 'idequipment', 'localid', 'idinspector', 'dinas', 'shift', 'status', 'conditions', 'note', 'nama_equipment', 'nama_inspector', 'idinspection', 'idparam', 'value', 'state', 'notification', 'label', 'flag_msg', 'value_min', 'value_max', 'value_standar', 'uom'))
        ->set_header(array('Inspection ID', 'Date Lokal', 'Date Time', 'Eqiupment ID', 'Local ID', 'Inspector', 'Dinas', 'Shift', 'Status', 'Conditions', 'Note', 'Nama Equipment', 'Nama Inspector', 'Inspection ID', 'param ID', 'Value', 'State', 'Notification', 'Label', 'Flag msg', 'Value Min', 'Value Max', 'Value Standar', 'UOM'))
        ->exportTo2007('Equipment'.date('YmdHis')) ;
    }
    
    public function generatepdf()
    {
        $this->load->library('dpdf');
        $data['row']    = $this->report_model->read_pdf();
        $html = $this->load->view('report/pdf',$data, TRUE);
        $this->dpdf->pdf_create($html, "Report", array(0,0,660,850), 'portrait');
    }
    public function index()
    {
       $this->template->display('report/report');
    }
}
/* End of file equipment.php */
/* Location: ./application/controllers/equipment.php */