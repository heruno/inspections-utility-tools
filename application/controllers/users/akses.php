<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Akses extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('users/nodes_akses_model','nodes_akses_model');
    }

    public function index()
    {
        $this->template->display('users/akses');
    }

    public function read()
    {
        $this->output
            ->set_content_type('application/json')
            ->set_header('Cache-Control: no-cache, must-revalidate')
            ->set_header('Expires: ' . date('r', time() + (86400 * 365)))
            ->set_output(
                $this->nodes_akses_model->read()
            );
    }

    public function read_one()
    {
        $this->output
            ->set_content_type('application/json')
            ->set_header('Cache-Control: no-cache, must-revalidate')
            ->set_header('Expires: ' . date('r', time() + (86400 * 365)))
            ->set_output(
                $this->nodes_akses_model->read_one()
            );
    }

    public function read_all()
    {
        $this->output
            ->set_content_type('application/json')
            ->set_header('Cache-Control: no-cache, must-revalidate')
            ->set_header('Expires: ' . date('r', time() + (86400 * 365)))
            ->set_output(
                $this->nodes_akses_model->read_all()
            );
    }

    public function create_file()
    {
        try {
            if (write_file('assets/akses.json', $this->nodes_akses_model->read_all())) {
                $this->output->set_output(
                    json_encode(array('successful' => 'Generate Success'))
                );
            }
        } catch (Exception $e) {
            $this->output->set_output(
                json_encode(array('unsuccessful' => 'Generate Error :' . $e->getMessage()))
            );
        }
        return false;
    }

    public function create()
    {
        $result = $this->nodes_akses_model->insert();
        if ($result == 1) {
            $this->output->set_output(json_encode(array('msg' => 'Insert Success')));
        } else {
            $this->output->set_output(json_encode(array('isError' => true, 'msg' => 'Insert Error :'
                . $this->db->_error_number() . ' : ' . $this->db->_error_message())));
        }
    }

    public function update()
    {
        $data = file_get_contents('php://input');
        log_message('INFO', 'KONTEN UPDATE MENU AKSES : ' . $data);
        $result = $this->nodes_akses_model->update();
        if ($result == 1) {
            $this->output->set_output(json_encode(array('msg' => 'Update Success')));
        } else {
            $this->output->set_output(json_encode(array('isError' => true, 'msg' => 'Update Error :'
                . $this->db->_error_number() . ' : ' . $this->db->_error_message())));
        }
    }

    public function delete()
    {
        $data = file_get_contents('php://input');
        log_message('INFO', 'KONTEN DELETE MENU AKSES : ' . $data);
        $result = $this->nodes_akses_model->delete();
        log_message('INFO', 'DELETE MENU AKSES :' . $result);
        if ($result == 1) {
            $this->output->set_output(json_encode(array('isError' => false, 'msg' => 'Delete success')));
        } else {
            $this->output->set_output(json_encode(array('isError' => true, 'msg' => 'Delete Error ')));
        }

    }
}
/* End of file aaksesusermenu.php */
/* Location: ./application/controllers/aaksesusermenu.php */