<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Edit_password extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    
    }
    public function index()
    {
        $data['nip'] = $this->session->userdata('id_user');
        $this->template->display('users/edit_password', $data);
    }
    public function action_edit_password()
    {
        $this->output->set_output(edit_password());
    }
    function edit_password()
    {
        if($this->cek_pass($this->input->post('id_user'))== 1){
        $this->db->where('id_user', $this->input->post('id_user'));
       	$this->db->update($this->_table, array('password' =>md5($this->input->post('password'))));
		if($this->db->affected_rows()== 1){
		  return 'Ubah Password Succsess!';
		  }else{
          return 'Ubah Password Error!';
        }
        }else{
           return 'Terjadi kesalahan, Silahkan Masukan Password yang benar!';
        }
    }

    function cek_pass($id_user)
    {
        $this->db->where('id_user', $id_user);
        $this->db->where('password', md5($this->input->post('oldpassword')));
        $result = $this->db->get($this->_table);
        return $result->num_rows();
    }
    function cek_username(){
        $this->db->where('username', $this->input->post('username'));
        $result = $this->db->get($this->_table);
        return $result->num_rows();
    }
}