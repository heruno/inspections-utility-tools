<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('users/login_model','login');
    }

    public function index()
    {
        if ($this->session->userdata("login") == TRUE) {
            redirect(md5('welcome'));
        } else {
            $data['error_login'] = '<p>Silahkan Masukan Username & Password Anda</p>';
            $this->template->load('layout/layout_login','users/login',$data);
        }
    }

    function logout()
    {
        $this->session->sess_destroy();
        unset($_COOKIE['8sois66g7dkh2turi94c0b9g91']);
        $this->db->cache_delete_all();
        $data['error_login'] = '<p>Anda Keluar Dari Sistem</p>';
        $this->template->load('layout/layout_login','users/login',$data);
    }

    function proses()
    {
        $user = $this->input->post('username');
        $pass = $this->input->post('password');

        if ($user == "" || $pass == "") {
            $data['error_login'] = 'Klik Login setelah input User&Password';
            $this->template->load('layout/layout_login','users/login',$data);
        } else if ($this->login->proses_login($user, $pass) == TRUE) {
            redirect('');
        } else {
            $data['error_login'] = 'Mohon periksa user dan password anda';
            $this->template->load('layout/layout_login','users/login',$data);
        }
    }
}