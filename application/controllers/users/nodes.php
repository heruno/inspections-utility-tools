<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Nodes extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('users/nodes_model','nodes_model');
    }

    public function index()
    {
        $this->template->display('users/nodes');
    }

    public function read()
    {
        $this->output
            ->set_content_type('application/json')
            ->set_output($this->nodes_model->read());
    }

    public function read_one()
    {
        $this->output
            ->set_content_type('application/json')
            ->set_output($this->nodes_model->read_one());
    }

    public function read_all()
    {
        $this->output
            ->set_content_type('application/json')
            ->set_output($this->nodes_model->read_all());
    }

    public function create()
    {
        $result = $this->nodes_model->insert();
        if ($result == 1) {
            $this->output->set_output(json_encode(array('successful' => 'Insert Success')));
        } else {
            $this->output->set_output(json_encode(array('unsuccessful' => 'Insert Error :' . $this->db->_error_number() . ' : ' . $this->db->_error_message())));
        }
    }

    public function create_file()
    {
        $this->load->model('partial/tree_model', 'tree');
        try {
            if (write_file('assets/tree.json', $this->tree->show_menu())) {
                $this->output->set_output(
                    json_encode(array('successful' => 'Generate Success'))
                );
            }
        } catch (Exception $e) {
            $this->output->set_output(
                json_encode(array('unsuccessful' => 'Generate Error :' . $e->getMessage()))
            );
        }
        return false;
    }

    public function update()
    {
        $result = $this->nodes_model->update();
        if ($result == 1) {
            $this->output->set_output(json_encode(array('successful' => 'Update Success')));
        } else {
            $this->output->set_output(json_encode(array('unsuccessful' => 'Update Error :' . $this->db->_error_number() . ' : ' . $this->db->_error_message())));
        }
    }

    public function delete()
    {
        $id = $this->input->post('id');
        if ($id == 1) {
            $this->output->set_output(json_encode(array('unsuccessful' => 'Maaf Menu Utama Tidak dapat dihapus! :')));
            exit;
        }
        $result = $this->nodes_model->delete();
        if ($result == 1) {
            $this->output->set_output(json_encode(array('successful' => 'Delete success')));
        } else {
            $this->output->set_output(json_encode(array('unsuccessful' => 'Delete Error :' . $this->db->_error_number() . ' : ' . $this->db->_error_message())));
        }
    }
}
/* End of file nodes.php */
/* Location: ./application/controllers/nodes.php */