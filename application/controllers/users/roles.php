<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Roles extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
         $this->load->model('users/roles_model','roles');
    }
    public function index()
    {
        $this->template->display('users/roles');
       
    }
    public function create()
    {
        $result = $this->roles->insert();
        if ($result == 1)
        {
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array('successful' => 'Insert Success')));
        } else
        {
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array('unsuccessful' => 'Insert Error :' .
                    $this->db->_error_number() . ' : ' . $this->db->_error_message())));
        }
    }
    public function read()
    {
        $this->output
        ->set_content_type('application/json')
        ->set_output($this->roles->read());
    }
    public function read_all()
    {
        $this->output
        ->set_content_type('application/json')
        ->set_output($this->roles->read_all());
    }
    public function update()
    {
        $result = $this->roles->update();
        if ($result == 1)
        {
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array('successful' => 'Update Success')));
        } else
        {
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array('unsuccessful' => 'Update Error :' . $this->db->_error_number() . ' : ' . $this->
                    db->_error_message())));
        }
    }
    public function delete()
    {
        $result = $this->roles->delete();
        if ($result == 1)
        {
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array('successful' => 'Delete success')));
        } else
        {
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array('unsuccessful' => 'Delete Error :' . $this->db->_error_number() . ' : ' . $this->
                    db->_error_message())));
        }
    }
}