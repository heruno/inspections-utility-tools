<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Users_roles extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('users/users_roles_model', 'users_roles');
    }
    public function index()
    {
        $this->template->display('users/users_roles');
    }

    public function create()
    {
        $result = $this->users_roles->insert();
        if ($result == 1)
        {
            $this->output->set_content_type('application/json')->set_output(json_encode(array
                ('successful' => 'Insert Success')));
        } else
        {
            $this->output->set_content_type('application/json')->set_output(json_encode(array
                ('unsuccessful' => 'Insert Error :' . $this->db->_error_number() . ' : ' . $this->
                    db->_error_message())));
        }
    }

    public function create_batch()
    {
        $data = file_get_contents('php://input');
        $data = json_decode($data, true);
        $this->db->insert_batch('users_roles', $data);
        $result = $this->db->affected_rows();
        $this->output->set_output(json_encode(array('msg' => $result)));

    }

    public function delete_batch()
    {
        $data = file_get_contents('php://input');
        $data = json_decode($data, true);
        foreach ($data as $d)
        {
            $this->db->where('users_id', $d['users_id']);
            $this->db->where('roles_id', $d['roles_id']);
            $this->db->delete('users_roles');
        }
        $result = $this->db->affected_rows();
        $this->output->set_output(json_encode(array('msg' => $result)));
    }

    public function read()
    {
        $this->output->set_content_type('application/json')->set_output($this->
            users_roles->read());
    }

    public function read_all()
    {
        $this->output->set_content_type('application/json')->set_output($this->
            users_roles->read_all());
    }

    public function update()
    {
        $result = $this->users_roles->update();
        if ($result == 1)
        {
            $this->output->set_content_type('application/json')->set_output(json_encode(array
                ('successful' => 'Update Success')));
        } else
        {
            $this->output->set_content_type('application/json')->set_output(json_encode(array
                ('unsuccessful' => 'Update Error :' . $this->db->_error_number() . ' : ' . $this->
                    db->_error_message())));
        }
    }

    public function delete()
    {
        $result = $this->users_roles->delete();
        if ($result == 1)
        {
            $this->output->set_content_type('application/json')->set_output(json_encode(array
                ('successful' => 'Delete success')));
        } else
        {
            $this->output->set_content_type('application/json')->set_output(json_encode(array
                ('unsuccessful' => 'Delete Error :' . $this->db->_error_number() . ' : ' . $this->
                    db->_error_message())));
        }
    }
}
