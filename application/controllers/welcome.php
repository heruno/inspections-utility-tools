<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Welcome extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function ceksession(){
        return $this->session->userdata('login');
    }

    public function index()
    {
        $data = array(
        'roles' =>$this->get_roles($this->session->userdata('id_roles')),
        'title' => 'EQUIPMENT INFORMATION SYSTEM v0.1.0',
        'tree'=> $this->menu());
        $this->template->load('layout/layout1', 'welcome/welcome', $data);
    }
    function get_roles($id){
        $this->db->where('roles_id', $id);
        $result = $this->db->get('roles');
        $roles =  $result->result_array();
        return $roles;
    }
    public function menu()
    {
        $this->load->model('partial/tree_model', 'tree');
        return $this->tree->show_menu();
    }
    public function release()
    {
        $this->template->display('release');
    }

    public function log()
    {
        $this->load->library('logviewer');
        $this->logviewer->run();
    }
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
