<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('login') == false) {
            redirect('users/login', 'refresh');
        }
    }

    public function json($data)
    {
        return $this->output
            ->set_content_type('application/json')
            ->set_header('Cache-Control: no-cache, must-revalidate')
            ->set_header('Expires: ' . date('r', time() + (86400 * 365)))
            ->set_output($data);
    }
}