<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Description of SIMKES CORE MODEL
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 */
class MY_Model extends CI_Model
{
    var $_table = "";
    var $_view = "";
    var $_order = "asc";
    var $_sort = "";
    var $_page = 1;
    var $_rows = 10;
    var $_filter = array();
    var $_data = array();
    var $_param = array();
    var $_create = false;
    var $_update = false;
	
    public function __construct()
    {
        parent::__construct();
    }
    public function filter(){
        $before = array('"[',']"','\"');
        $after  = array('[',']','"');
        if($this->input->post('filterRules')){
            $data = str_replace($before, $after, json_encode($this->input->post('filterRules')));
            foreach(json_decode($data, true) as $row){
                 $this->db->like($row['field'], $row['value']);
            }
        }
    }
    public function read($get = array())
    {
        
        $this->_view = $this->_view ? $this->_view : $this->_table;
        $order = $this->input->post('order') ? $this->input->post('order') : $this->_order;
        $sort  = $this->input->post('sort') ? $this->input->post('sort') : $this->_sort;
        $page  = $this->input->post('page') ? intval($this->input->post('page')) : $this->_page;
        $rows  = $this->input->post('rows') ? intval($this->input->post('rows')) : $this->_rows;
        $this->filter();
        
        $offset = (($page - 1) * $rows);
        if (sizeof($get) > 0)
        {
            $this->db->where($get);
        }
        if (sizeof($this->_filter) > 0)
        {
            $this->db->where($this->_filter);
        }
        $this->db->order_by($sort, $order);
        $this->db->limit($rows, $offset);
        $result = $this->db->get($this->_view);
        $json['rows'] = $result->result_array();
        $json['total'] = $this->_total($get);
        return json_encode($json);
    }
    protected function _total($get = array())
    {
        $this->filter();
        if (sizeof($get) > 0){
            $this->db->where($get);
        }
        if (sizeof($this->_filter) > 0){
            $this->db->where($this->_filter);
        }
        $this->_view = $this->_view ? $this->_view : $this->_table;
        return $this->db->get($this->_view)->num_rows();
    }
    public function read_one($get = array())
    {
        $this->_view = $this->_view ? $this->_view : $this->_table;
        if (sizeof($get) > 0)
        {
            $this->db->where($get);
        }
        if (sizeof($this->_filter) > 0)
        {
            $this->db->where($this->_filter);
        }
        $this->db->where($this->_param);
        $result = $this->db->get($this->_view);
        return json_encode($result->row_array());
    }
    public function read_all($get = array())
    {
        $this->_view = $this->_view ? $this->_view : $this->_table;
        $this->filter();

        if (sizeof($this->_filter) > 0)
        {
            $this->db->where($this->_filter);
        }
        $order = $this->input->post('order') ? $this->input->post('order') : $this->
            _order;
        $sort = $this->input->post('sort') ? $this->input->post('sort') : $this->_sort;
        $this->db->order_by($sort, $order);
        $result = $this->db->get($this->_view);
        return json_encode($result->result_array());
    }
    public function insert($get = array())
    {
        if ($this->_create == true)
        {
            $create = array(
                'create_user' => $this->session->userdata('username'),
                'create_date' => date("Y-m-d H:i:s"),
                'create_ip' => $this->input->ip_address());
        } else
        {
            $create = array();
        }
        $data = array_merge($this->_param, $this->_data, $get, $create);
        $this->db->insert($this->_table, $data);
        return $this->db->affected_rows();
    }
    public function update($get = array())
    {
        if ($this->_update == true)
        {
            $update = array(
                'update_user' => $this->session->userdata('username'),
                'update_date' => date("Y-m-d H:i:s"),
                'update_ip' => $this->input->ip_address());
        } else
        {
            $update = array();
        }
        if (sizeof($get) > 0)
        {
            $this->db->where($get);
        }
        $this->db->where($this->_param);
        $data = array_merge($this->_data, $update);
        $this->db->update($this->_table, $data);
        return $this->db->affected_rows();
    }
    public function delete($get = array())
    {
        if (sizeof($get) > 0)
        {
            $this->db->where($get);
        }
        $this->db->where($this->_param);
        $this->db->delete($this->_table);
        return $this->db->affected_rows();
    }
    function cek_group($p){
        $this->db->where('roles_id', $p);
        $this->db->where('users_id' , $this->session->userdata('id_user'));
        $result = $this->db->get('users_roles');
        return $result->num_rows();
    }
    function delete_cache()
    {
        $uri1 = $this->uri->segment(1);
        $uri2 = $this->uri->segment(2) ? $this->uri->segment(2) : 'read';
        $path = APPPATH . 'cache/db/' . $uri1 . '+' . $uri2;
        switch ($uri2)
        {
            case 'create':
                $uri2 = 'read';
                break;
            case 'update':
                $uri2 = 'read';
                break;
            case 'delete':
                $uri2 = 'read';
                break;
            default:
                $uri2 = $uri2;
                break;
        }
        log_message('INFO', 'DELETE CACCHE PATH : ' . $path);
        if (is_dir($path))
        {
            $this->db->cache_delete($uri1, $uri2);
        }
    }
}
/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */