<?php

/**
 * User: heruno
 * Date: 24/10/2017
 * Time: 11:16
 */
class AuthHooks
{
    var $CI;

    function __construct()
    {
        $this->CI = &get_instance();
    }

    function dirClass($paramUrl)
    {
        if ($paramUrl == $this->CI->router->directory . $this->CI->router->class) {
            return true;
        } else {
            return false;
        }
    }

    function authHooks()
    {
        $dirclass = $this->CI->router->directory . $this->CI->router->class;
        foreach ($this->getDB($dirclass) as $row) {
                log_message('error', 'dirclass  ' . $dirclass);
                log_message('error', 'rowurl  ' . $row->url);
                if ($row->read == 0) {
                    if (substr($this->CI->router->method, 0, 4) == 'read') {
                        $this->CI->json(json_encode(array('unsuccessful' => 'Anda Tidak memiliki Hak Akses untuk Baca Data')));
                        return;
                    };
                }
                if ($row->add == 0) {
                    if ($this->CI->router->method == 'create') {
                        $this->CI->json(json_encode(array('unsuccessful' => 'Anda Tidak memiliki Hak Akses untuk Tambah Data')));
                        return;
                    };
                }
                if ($row->edit == 0) {
                    if ($this->CI->router->method == 'update') {
                        $this->CI->json(json_encode(array('unsuccessful' => 'Anda Tidak memiliki Hak Akses untuk Ubah Data')));
                        return;
                    };
                }
                if ($row->delete == 0) {
                    if ($this->CI->router->method == 'delete') {
                        $this->CI->json(json_encode(array('unsuccessful' => 'Anda Tidak memiliki Hak Akses untuk Hapus Data')));
                        return;
                    };
                }
           // }
        }
    }

    function getDB($oUrl)
    {
       // log_message('error','$oUrl  '.$oUrl);
        $this->CI->db->distinct('a.* , b.text, b.url');
        $this->CI->db->select();
        $this->CI->db->from('users_menu_roles_akses a');
        $this->CI->db->join('users_menu b', 'a.menu_id = b.menuId');
        $this->CI->db->where('url', $oUrl);
        if ($this->CI->session->userdata('id_roles')) {
            $this->CI->db->where('roles_id', $this->CI->session->userdata('id_roles'));
        }
        $query = $this->CI->db->get();
        $result = $query->result();
        return $result;
    }

}


