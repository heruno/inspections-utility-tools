<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Auth
{
    var $AT = null;
    function __construct()
    {
        $this->AT = &get_instance();
    }
    function process_login($username, $password){
        $query = "SELECT * FROM auser_vd  
                  WHERE username= '$username' 
                  AND password= MD5('$password') 
                  AND aktif = 1";
        $result = $this->AT->db->query($query);
        if ($result->num_rows == 1){
            $login = $result->row_array();
            $sess  = array(
                'nama'    => $login['nama'],
                'nip'     => $login['nip'],
                'id_user' => $login['id_user'],
                'username'=> $login['username'],
                'id_kec'  => $login['id_kec'],
                'id_pus'  => $login['id_pus'],
                'nm_pus'  => $login['nama_pus'],
                'nm_kec'  => $login['nama_kec'],
                'initial' => $login['initial'],
                'kode_provider' => $login['kode_provider'],
                'login'   => true);
            $_SESSION['id_user'] = $login['id_user'];
            $this->AT->session->sess_create();
            $this->AT->session->set_userdata($sess);
            return true;
        } else{
            return false;
        }
    }
    public function logout()
    {
        session_destroy();
        $this->AT->session->sess_destroy();
        $this->AT->session->sess_create();
        $this->AT->session->set_flashdata('login_notif', '<p>Anda Keluar Dari Sistem</p>');
        $this->AT->db->cache_delete_all();
        redirect('users');
    }
}