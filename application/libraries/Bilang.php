<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Bilang library
 *
 * @author    Heruno Utomo
 */
class Bilang
{
    function load($nilai)
    {
        return $this->terbilang($nilai);
    }

    function terbilang($satuan)
    {
        $huruf = array(
            "",
            "Satu",
            "Dua",
            "Tiga",
            "Empat",
            "Lima",
            "Enam",
            "Tujuh",
            "Delapan",
            "Sembilan",
            "Sepuluh",
            "Sebelas");
        if ($satuan < 12)
            return " " . $huruf[$satuan] ."Rupiah";
        elseif ($satuan < 20)
            return $this->terbilang($satuan - 10) . " Belas" ."Rupiah";
        elseif ($satuan < 100)
            return $this->terbilang($satuan / 10) . " Puluh" . $this->terbilang($satuan % 10) ."Rupiah";
        elseif ($satuan < 200)
            return " Seratus" . $this->terbilang($satuan - 100) ."Rupiah";
        elseif ($satuan < 1000)
            return $this->terbilang($satuan / 100) . " Ratus" . $this->terbilang($satuan % 100) ."Rupiah";
        elseif ($satuan < 2000)
            return " Seribu" . $this->terbilang($satuan - 1000);
        elseif ($satuan < 1000000)
            return $this->terbilang($satuan / 1000) . " Ribu" . $this->terbilang($satuan % 1000) ."Rupiah";
        elseif ($satuan < 1000000000)
            return $this->terbilang($satuan / 1000000) . " Juta" . $this->terbilang($satuan % 1000000) ."Rupiah";
        elseif ($satuan >= 1000000000)
            return "Hasil $this->terbilang tidak dapat di proses karena nilai uang terlalu besar!";
    }

}
/* End of file Bilang.php */
/* Location: ./application/libraries/Bilang.php */