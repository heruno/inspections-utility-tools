<?php
class DPdf
{
    function pdf_create($html, $filename, $paper, $orientation, $stream = true)
    {
        include_once (APPPATH."/third_party/dompdf/dompdf_config.inc.php");
        spl_autoload_register('DOMPDF_autoload');
        $dompdf = new DOMPDF();
        $dompdf->set_paper($paper, $orientation);
        $dompdf->load_html($html);
        $dompdf->render();
        if ($stream)
        {
            $dompdf->stream($filename.". pdf",array("Attachment" => 0));
        } else
        {
            $CI = &get_instance();
            $CI->load->helper("file");
            write_file($filename, $dompdf->output());
        }
    }
}