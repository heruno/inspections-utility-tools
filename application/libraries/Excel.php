<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 *======================================= 
 *@author     : Heruno
 *@Email      : wolu.88@gmail.com 
 */
require_once APPPATH . "/third_party/PHPEXCEL/PHPExcel.php";

class Excel extends PHPExcel
{
    public function __construct()
    {
        parent::__construct();
    }
}
