<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
 *  ======================================= 
 *  Author     : Heruno
 *  Email      : wolu.88@gmail.com 
 */
require_once APPPATH."/third_party/PHPPdf/fpdf.php";

class  Pdf extends FPDF  {
    public function __construct() {
        parent::__construct();
    }
}