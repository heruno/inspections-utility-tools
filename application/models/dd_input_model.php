<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of dd_input
 * @created on : 2014-09-28 19:17:53
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 */
class Dd_input_model extends MY_Model{
    function __construct(){
        parent::__construct();
        $this->_table = "dd_input";
        $this->_order = 'asc';
        $this->_sort  = 'class';
        $this->_page  = 1;
        $this->_rows  = 10;
              
        $this->_param = array(
        'class' => $this->input->post('class'),
        );
        $this->_data = array(
        'type'  => $this->input->post('type'),
        'input'  => $this->input->post('input')
        );
    }
}
/* End of file aagama.php */
/* Location: ./application/models/inspector_model.php */