<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Equipment
 * @created on : 2014-09-28 19:17:53
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 */
class Equipment_model extends MY_Model
{
    function __construct()
    {
        parent::__construct();
        $this->_table = "tbequipment";
        $this->_view = "equipment_vd";
        $this->_order = 'desc';
        $this->_sort = 'nomor';
        $this->_page = 1;
        $this->_rows = 10;

        if ($this->session->userdata('id_roles') > 2) {
            $this->_filter = array(
                'dinas' => $this->session->userdata('dinas'),
                'line' => $this->session->userdata('line')
            );
        }
        /*if ($this->uri->segment(4)) {
            $this->_filter = array(
                'dinas' => $this->uri->segment(4)
            );
        }*/
        $this->_param = array(
            'idequipment' => $this->input->post('idequipment')
        );
        $this->_data = array(
            'uid' => $this->input->post('uid'),
            'name' => $this->input->post('name'),
            'line' => $this->input->post('line'),
            'dinas' => $this->input->post('dinas'),
            'spec' => $this->input->post('spec')
        );
    }
}