<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Equipment
 * @created on : 2014-09-28 19:17:53
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 */
class Inspection_model extends MY_Model{
    function __construct(){
        parent::__construct();
        $this->_table = "tbinspection";
        $this->_view = "inspection_vd";
        $this->_order = 'asc';
        $this->_sort  = 'inputdt';
        $this->_page  = 1;
        $this->_rows  = 10;
        
        $this->_filter = array(
        'idinspection' => $this->uri->segment(3)
        );         
           
        $this->_param = array(
        'idinspection' => $this->input->post('idinspection'),
        'idequipment' => $this->input->post('idequipment'),
        );
        $this->_data = array(
        'idparam'  => $this->input->post('idparam'),
        'inputdt'  => $this->input->post('inputdt'),
        'inputtm'  => $this->input->post('inputtm'),
        'value'  => $this->input->post('value'),
        'state'  => $this->input->post('state'),
        'notification'  => $this->input->post('notification'),
        );
    }
}
/* End of file aagama.php */
/* Location: ./application/models/equipment_model.php */