<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Equipment
 * @created on : 2014-09-28 19:17:53
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 */
class Inspectionlot_model extends MY_Model
{
    function __construct()
    {
        parent::__construct();
        $this->_table = "tbinspectionlot";
        $this->_view = "inspectionlot_vd";
        $this->_order = 'desc';
        $this->_sort = 'inspectionid';
        $this->_page = 1;
        $this->_rows = 10;

        if ($this->uri->segment(3)) {
            $this->_filter = array(
                'DATE_FORMAT(localdt,"%Y-%m-%d")' => $this->uri->segment(3)
            );
        }

        $this->_param = array(
            'inspectionid' => $this->input->post('inspectionid'),
        );
        $this->_data = array(
            'inputdt' => $this->input->post('inputdt'),
            'inputtm' => $this->input->post('inputtm'),
            'idequipment' => $this->input->post('idequipment'),
            'idinspector' => $this->input->post('idinspector'),
            'shift' => $this->input->post('shift'),
            'conditions' => $this->input->post('conditions'),
        );
    }
}
/* End of file aagama.php */
/* Location: ./application/models/equipment_model.php */