<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Inspector
 * @created on : 2014-09-28 19:17:53
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 */
class Inspector_model extends MY_Model
{
    function __construct()
    {
        parent::__construct();
        $this->_table = "tbinspector";
        $this->_view = "inspector_vd";
        $this->_order = 'asc';
        $this->_sort = 'nama';
        $this->_page = 1;
        $this->_rows = 10;

        if ($this->session->userdata('id_roles') > 2) {
            $this->_filter = array(
                'dinas' => $this->session->userdata('dinas'),
                'line' => $this->session->userdata('line')
            );
        }

        /**
         * if ($this->uri->segment(4)) {
         * $this->_filter = array(
         * 'dinas' => $this->uri->segment(4)
         * );
         * }
         */

        $this->_param = array(
            'nik' => $this->input->post('nik'),
        );
        $pass = $this->input->post('pass');

        $this->_data = array(
            'nama' => $this->input->post('nama'),
            'dinas' => $this->input->post('dinas'),
            'line' => $this->input->post('line')
        );
        //'pass'      => ($this->cekUser($this->input->post('nik')) == $pass) ? $pass : md5($pass)
    }

    function cekUser($id)
    {
        $sql = $this->db
            ->where('nik', $id)
            ->get('tbinspector');
        $resutl = $sql->row_array();
        return $resutl['pass'];
    }
}
/* End of file aagama.php */
/* Location: ./application/models/inspector_model.php */