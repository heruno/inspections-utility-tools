<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of jabatan
 * @created on : 2014-09-28 19:17:53
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 */
class Jabatan_model extends MY_Model{
    function __construct(){
        parent::__construct();
        $this->_table = "tbjabatan";
        $this->_order = 'asc';
        $this->_sort  = 'idjabatan';
        $this->_page  = 1;
        $this->_rows  = 10;
              
        $this->_param = array(
        'idjabatan' => $this->input->post('idjabatan'),
        );
        $this->_data = array(
        'jabatan'  => $this->input->post('jabatan'),
        );
    }
}
/* End of file aagama.php */
/* Location: ./application/models/inspector_model.php */