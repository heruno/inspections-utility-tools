<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Mkaryawan
 * @created on : 2014-09-28 19:17:53
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 */
class Mkaryawan_model extends MY_Model{
    function __construct(){
        parent::__construct();
        $this->_table = "mkaryawan";
        $this->_view  = "mkaryawan_vd";
        $this->_order = 'asc';
        $this->_sort  = 'nip';
        $this->_page  = 1;
        $this->_rows  = 10;
        $this->_create = true;
        $this->_update=true;

        if ($this->session->userdata('id_roles') > 2) {
            $this->_filter = array(
                'dinas' => $this->session->userdata('dinas'),
                'line' => $this->session->userdata('line')
            );
        }

        $this->_param = array(
        'nip' => $this->input->post('nip')
        );

        $this->_data = array(
        'nama'          => $this->input->post('nama'),
        'no_telp'    => $this->input->post('no_telp'),
        'email'    => $this->input->post('email'),
        'line'    => $this->input->post('line'),
        'dinas'    => $this->input->post('dinas')
        );
        // 'idjabatan'    => $this->input->post('idjabatan'),
    }
}
/* End of file mkaryawan.php */
/* Location: ./application/models/mkaryawan.php */