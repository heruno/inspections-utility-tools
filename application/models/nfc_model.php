<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of jabatan
 * @created on : 2014-09-28 19:17:53
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 */
class Nfc_model extends MY_Model
{
    function __construct()
    {
        parent::__construct();

        $this->_table = "nfc";
        $this->_order = 'asc';
        $this->_sort = 'nomor';
        $this->_page = 1;
        $this->_rows = 10;

        $this->_param = array(
            'nomor' => $this->input->post('nomor'),
        );
        $this->_data = array(
            'kode' => $this->input->post('kode'),
        );
    }

    function unggah()
    {
        $pesssan = array();
        if (!empty($_FILES['attachment'])) {
            $path = "assets/images/upload/";
            $path = $path . basename($_FILES['attachment']['name']);
            $imageFileType = pathinfo($path, PATHINFO_EXTENSION);
            $ext = array('jpg', 'png', 'jpeg', 'gif');
            if (in_array($imageFileType, $ext)) {
                if (move_uploaded_file($_FILES['attachment']['tmp_name'], $path))
                {
                    $img64 = 'data: ' . $this->mime_content_type($path) . ';base64,' . base64_encode(file_get_contents($path));
                    $this->db->where(array(
                        'nomor'=>$this->input->post('nomor'),
                        'kode'=>$this->input->post('kode')
                    ))->update($this->_table, array(
                        'image' => $_FILES['attachment']['name'],
                        'image64' => $img64
                    ));
                    $pesssan = array(
                        'image'=>$_FILES['attachment']['name'],
                        'msg'=>'Gambar berhasil diupdate'
                    );
                } else {
                    $pesssan = array(
                        'image'=>'no.jpg',
                        'msg'=>'Create File Gagal'
                    );
                }
            }else{
                $pesssan = array(
                    'image'=>'no.jpg',
                    'msg'=>'Silahkan pilih file gambar'
                );
            }
        } else {
            $pesssan = array(
                'image'=>'no.jpg',
                'msg'=>'Tidak ada File Gambar yang diupload'
            );
        }
        return $pesssan;
    }

    function mime_content_type($filename)
    {

        $mime_types = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        $ext = strtolower(array_pop(explode('.', $filename)));
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        } elseif (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        } else {
            return 'application/octet-stream';
        }
    }
}
/* End of file aagama.php */
/* Location: ./application/models/inspector_model.php */