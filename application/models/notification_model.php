<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of notification
 * @created on : 2014-09-28 19:17:53
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 */
class Notification_model extends MY_Model{
    function __construct(){
        parent::__construct();
        $this->_table = "tnotification";
        $this->_order = 'desc';
        $this->_sort  = 'id';
        $this->_page  = 1;
        $this->_rows  = 10;

        $this->_param = array(
        'nik' => $this->input->post('nik'),
        );
        $this->_data = array(
        'email'  => $this->input->post('email'),
        'sms'  => $this->input->post('sms'),
        'unit'  => $this->input->post('unit')
        );
    }
}
/* End of file aagama.php */
/* Location: ./application/models/inspector_model.php */