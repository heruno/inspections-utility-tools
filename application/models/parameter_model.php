<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Parameter
 * @created on : 2014-09-28 19:17:53
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 */
class Parameter_model extends MY_Model
{
    function __construct()
    {
        parent::__construct();
        $this->_table = "dd_attribute";
        $this->_view = "dd_attribute_vd";
        $this->_order = 'asc';
        $this->_sort = 'idequipment';
        $this->_page = 1;
        $this->_rows = 10;

        $s4 = $this->uri->segment(4);
        $s5 = $this->uri->segment(5);
        $idu1 = $this->inspector_map($s4);
        if ($this->uri->segment(3) == 'equipment') {
            $this->_filter = array(
                'idequipment' => $s4
            );
        } else {
            $this->_filter = array(
                'dinas' => $this->uri->segment(3)
            );
            if ($s5 == 'source') {
                foreach ($idu1 as $id) {
                    $this->db->where_not_in('id', $id['id']);
                }
            }
        }


        $this->_param = array(
            'id' => $this->input->post('id'),
            'idequipment' => $this->input->post('idequipment'),
        );
        $this->_data = array(
            'label' => $this->input->post('label'),
            'type' => $this->input->post('type'),
            'class' => $this->input->post('class'),
            'flag_msg' => $this->input->post('flag_msg'),
            'value_min' => $this->input->post('value_min'),
            'value_max' => $this->input->post('value_max'),
            'value_standar' => $this->input->post('value_standar'),
            'value' => $this->input->post('value'),
            'uom' => $this->input->post('uom'),
            'data-options' => $this->input->post('data-options'),
            'style' => $this->input->post('style')
        );
    }

    function inspector_map($nik)
    {
        $this->db->select('id');
        $this->db->where('nik', $nik);
        $result = $this->db->get('inspector_maps');
        return $result->result_array();
    }
}
/* End of file aagama.php */
/* Location: ./application/models/parameter_model.php */