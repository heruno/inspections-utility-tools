<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Aagama
 * @created on : 2014-09-25 09:31:26
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 */
class Tree_model extends CI_Model{
    
    var $table ="users_menu";
    function __construct(){
        parent::__construct();
    }
    public function find_menu()
    {
        return json_encode($this->menu(0, $hasil=""));
    }
    public function show_menu()
    {
        log_message('error','[tree_model/show_menu] encode json '. json_encode($this->input->post()));
        $text = $this->uri->segment('4') ? $this->uri->segment('4') : 'EQUIPMENT SYSTEM';
        $id   = $this->tree->find_by($text);
        return json_encode($this->menu($id, $hasil=""));
    }
	private function menu($id=0, $hasil){
       foreach ($this->get_group() as $n)
        {
            $this->db->where_not_in('menuId', $n['menu_id']);
        }
        $this->db->where('parentId', $id);
        $this->db->where('active', 1);
		$this->db->order_by('text', 'asc');
        $w = $this->db->get($this->table);
        $hasil = array();
		foreach($w->result() as $row)
		{
		    $node = array();
            $node['id']   = $row->menuId;
            $node['parentId'] = $row->parentId;
            $node['text']     = $row->text;
            $node['iconCls']  = $row->iconCls;
            if($row->url != NULL OR $row->url != ''){
              $node['url']      = $row->url;
            }
            $node['state']    = $row->state;
            $node['active']   = $row->active;
            $node['children'] = $this->menu($row->menuId, $hasil);
            array_push($hasil, $node);
		}
		return $hasil;
	}
    function get_group(){
        $this->db->select('menu_id');
        $this->db->where('read', 0);
        $this->db->where('roles_id', $this->session->userdata('id_roles'));
        $result = $this->db->get('users_menu_roles_akses');
        return $result->result_array();
    }
    public function dnd_update()
    {
     /**   
        $param['id'] = $this->input->post('id');
        $data['parentId'] = $this->input->post('targetId');
        $this->db->where($param);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();*/
        return false;
    }
    function find_by($text)
    {
        
        //log_message('error','[tree_model/find_by] '.$text);
        $this->db->like('text', $text);
        $result = $this->db->get($this->table);
        $hasil  = $result->row();
        //log_message('error','[tree_model/find_by] '.json_encode($hasil));
        if(count($hasil->menuId)== 0){
            return $hasil->menuId;
        }else{
           if($hasil->parentId == 1){
             return $hasil->menuId;
           }else{
             return $hasil->parentId;
           }
        }
    }
}