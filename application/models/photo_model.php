<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Photo
 * @created on : 2014-09-28 19:17:53
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 */
class Photo_model extends MY_Model{
    function __construct(){
        parent::__construct();
        $this->_table = "photo";
        $this->_order = 'asc';
        $this->_sort  = 'id';
        $this->_page  = 1;
        $this->_rows  = 10;
         if($this->uri->segment(3)){
             $this->_filter = array(
                 'localid'=>$this->uri->segment(3)
             );
         }
        $this->_param = array(
        'id' => $this->input->post('id'),
        );
        $this->_data = array(
        'inspector'  => $this->input->post('inspector'),
        'dinas'  => $this->input->post('dinas'),
        'localid'  => $this->input->post('localid'),
        'src'  => $this->input->post('src')
        );
    }
}
/* End of file photo_model.php */
/* Location: ./application/models/photo_model.php */