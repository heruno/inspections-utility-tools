<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of email
 * @created on : 2014-09-28 19:17:53
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 */
class Report_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function read_pdf()
    {
        $sql = "SELECT * FROM `inspectionlot_vd` WHERE inputdt BETWEEN '" . $this->uri->segment(3) . "' AND '" . $this->uri->segment(4) . "'";
        $result = $this->db->query($sql);
        log_message('error', $this->db->last_query());
        return $result->result_array();
    }

    function read_detail_pdf($idequipment, $localid)
    {
        $this->db->where(array(
            'idequipment' => $idequipment,
            'localid' => $localid
        ));
        $result = $this->db->get('inspection_vd');
        log_message('error', $this->db->last_query());
        return $result->result_array();
    }

    function read_xls()
    {
        $sql = "SELECT * FROM `report_vd` WHERE inputdt BETWEEN '" . $this->uri->segment(3) . "' AND '" . $this->uri->segment(4) . "'";
        $result = $this->db->query($sql);
        log_message('error', 'last_query->' . $this->db->last_query());
        return $result;
    }
}
/* End of file aagama.php */
/* Location: ./application/models/report_model.php */