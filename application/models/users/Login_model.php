<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Login_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function proses_login($username, $password)
    {
        session_start();
        $where = "(users_id='$username' OR username='$username')";
        $this->db->where($where);
        $this->db->where('password', md5($password));
        $this->db->where('active', 1);
        $result = $this->db->get('users_vd');
        unset($_COOKIE['8sois66g7dkh2turi94c0b9g91']);
        if ($result->num_rows() == 1) {
            $login = $result->row_array();
            $rolesID = $this->get_roles($login['users_id']);
            $sess = array(
                'nama' => $login['nama'],
                'nip' => $login['nip'],
                'id_user' => $login['users_id'],
                'users_id' => $login['users_id'],
                'username' => $login['username'],
                'dinas' => $login['dinas'],
                'line' => $login['line'],
                'id_roles' => $rolesID,
                'login' => true);
            $_SESSION['8sois66g7dkh2turi94c0b9g91'] = $rolesID;
            $_COOKIE['8sois66g7dkh2turi94c0b9g91'] = $rolesID;
            setcookie('8sois66g7dkh2turi94c0b9g91', $rolesID, time() + (86400 * 30), "/");
            $this->session->set_userdata($sess);
            return true;
        } else {
            return false;
        }

    }

    function get_roles($id)
    {
        $this->db->where('users_id', $id);
        $result = $this->db->get('users_roles');
        $roles = $result->row_array();
        return $roles['roles_id'];
    }

}