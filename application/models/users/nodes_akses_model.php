<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Nodes_akses_model extends MY_Model
{
    function __construct()
    {
        parent::__construct();
        $this->_table = "users_menu_roles_akses";
        $this->_order = 'asc';
        $this->_sort = 'roles_id';
        $this->_page = 1;
        $this->_rows = 10;

        if ($this->uri->segment(4)) {
            $this->_filter = array(
                'roles_id' => $this->uri->segment(4)
            );
        }

        $idn = isset($_REQUEST['menu_id']) ? $_REQUEST['menu_id'] : '';
        $idu = isset($_REQUEST['roles_id']) ? $_REQUEST['roles_id'] : $this->uri->segment(4);

        $this->_param = array(
            'menu_id' => $this->input->post('menu_id') ? $this->input->post('menu_id') : $idn,
            'roles_id' => $this->input->post('roles_id') ? $this->input->post('roles_id') : $idu,
        );
        log_message('INFO', 'DELETE MENU AKSES : ' . $this->_param['menu_id'] . $this->input->post('roles_id'));

        $this->_data = array(
            'read' => $this->input->post('read'),
            'add' => $this->input->post('add'),
            'edit' => $this->input->post('edit'),
            'delete' => $this->input->post('delete'),
        );
    }

    function read($get = array())
    {

        $order = $this->input->post('order') ? $this->input->post('order') : $this->_order;
        $sort = $this->input->post('sort') ? $this->input->post('sort') : $this->_sort;
        $page = $this->input->post('page') ? intval($this->input->post('page')) : $this->_page;
        $rows = $this->input->post('rows') ? intval($this->input->post('rows')) : $this->_rows;
        $this->filter();
        $offset = (($page - 1) * $rows);
        $this->db->select('a.*, b.text as text, b.url as url');
        $this->db->from('users_menu_roles_akses a');
        $this->db->join('users_menu b', 'a.menu_id = b.menuId');
        if ($this->uri->segment(4)) {
            $this->_filter = array(
                'roles_id' => $this->uri->segment(4)
            );
        }
        if (sizeof($get) > 0) {
            $this->db->where($get);
        }
        if (sizeof($this->_filter) > 0) {
            $this->db->where($this->_filter);
        }

        $this->db->order_by($sort, $order);
        $this->db->limit($rows, $offset);
        $result = $this->db->get();
        $json['total'] = $this->_total($get);
        $json['rows'] = $result->result_array();
        return json_encode($json);
    }

    function read_all($get = array())
    {
        $this->filter();
        $this->db->select('a.*, b.text as text, b.url as url');
        $this->db->from('users_menu_roles_akses a');
        $this->db->join('users_menu b', 'a.menu_id = b.menuId');
        if ($this->uri->segment(4)) {
            $this->_filter = array(
                'roles_id' => $this->uri->segment(4)
            );
        }
        if (sizeof($this->_filter) > 0) {
            $this->db->where($this->_filter);
        }
        $order = $this->input->post('order') ? $this->input->post('order') : $this->_order;
        $sort = $this->input->post('sort') ? $this->input->post('sort') : $this->_sort;
        $this->db->order_by($sort, $order);
        $result = $this->db->get($this->_view);
        return json_encode($result->result_array());
    }
}