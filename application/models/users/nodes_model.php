<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Nodes
 * @created on : 2014-09-28 19:17:53
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 */
class Nodes_model extends MY_Model{
    function __construct(){
        parent::__construct();
        $this->_table = "users_menu";
        $this->_order = 'asc';
        $this->_sort  = 'text';
        $this->_page  = 1;
        $this->_rows  = 10;
        
        $this->_param = array(
        'menuId' => $this->input->post('menuId'),
        );
        $this->_data = array(
        'parentId'  => $this->input->post('parentId')? $this->input->post('parentId'):1,
        'text'      => $this->input->post('text'),
        'iconCls'   => $this->input->post('iconCls'),
        'url'       => $this->input->post('url'),
        'state'     => $this->input->post('state'),
        'active'     => $this->input->post('active')
        );
    }
    
}
/* End of file nodes.php */
/* Location: ./application/models/nodes.php */