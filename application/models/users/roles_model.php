<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of roles
 * @created on : 2014-09-28 19:17:53
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 */
class Roles_model extends MY_Model{
    function __construct(){
        parent::__construct();
        $this->_table = "roles";
        $this->_order = 'asc';
        $this->_sort  = 'roles_id';
        $this->_page  = 1;
        $this->_rows  = 10;
        
        $this->_param = array(
        'roles_id' => $this->input->post('roles_id'),
        );
        $this->_data = array(
        'name'  => $this->input->post('name')
        );
    }
    
}