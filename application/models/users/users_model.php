<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of users
 * @created on : 2014-09-28 19:17:53
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 */
class Users_model extends MY_Model
{
    /**
     *
     */
    function __construct()
    {
        parent::__construct();
        $this->_table = "users";
        $this->_view  = "users_vd";
        $this->_order = 'asc';
        $this->_sort = 'users_id';
        $this->_page = 1;
        $this->_rows = 10;
        
        $seg = $this->uri->segment(4);
        if ($seg) {
            $idu = $this->users_roles($seg);
            foreach ($idu as $u) {
                $this->db->where_not_in('users_id', $u['users_id']);
            }
        }
        $this->_param = array(
            'users_id' => $this->input->post('users_id'),
        );
        $this->_data = array(
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('password')),
            'active' => $this->input->post('active')
        );
    }

    function users_roles($idu)
    {
        $this->db->select('users_id');
        //$this->db->where('roles_id', $idu);
        $result = $this->db->get('users_roles');
        return $result->result_array();
    }

    function proses_login($username, $password)
    {
        $this->db->where('users_id', intval($username));
        $this->db->where('password', md5($password));
        $this->db->where('active', 1);
        $result = $this->db->get('users');

        if ($result->num_rows() == 1) {
            $login = $result->row_array();
            $sess = array(
                'id_user' => $login['users_id'],
                'login' => true);
            $_SESSION['id_user'] = $login['users_id'];
            $this->session->set_userdata($sess);
            return true;
        } else {
            log_message('error', 'proses login false');
            return false;
        }

    }

}