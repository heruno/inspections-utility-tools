<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of users_roles
 * @created on : 2014-09-28 19:17:53
 * @author Heruno Utomo <wolu.88@gmail.com>
 * Copyright 2014
 */
class Users_roles_model extends MY_Model
{
    function __construct()
    {
        parent::__construct();
        $this->_table = "users_roles";
        $this->_order = 'asc';
        $this->_sort = 'id';
        $this->_page = 1;
        $this->_rows = 10;

        $this->_filter = array(
            'roles_id' => $this->uri->segment(4)
        );

        $this->_param = array(
            'id' => $this->input->post('id'),
        );
        $this->_data = array(
            'roles_id' => $this->input->post('roles_id'),
            'users_id' => $this->input->post('users_id')
        );
    }

    function read($get = array())
    {
        $this->filter();
        $order = $this->input->post('order') ? $this->input->post('order') : $this->_order;
        $sort = $this->input->post('sort') ? $this->input->post('sort') : $this->_sort;
        $page = $this->input->post('page') ? intval($this->input->post('page')) : $this->_page;
        $rows = $this->input->post('rows') ? intval($this->input->post('rows')) : $this->_rows;
        $offset = (($page - 1) * $rows);
        $this->db->select('m.*');
        $this->db->select('a.username as username');
        $this->db->select('b.nama as nama');
        $this->db->from($this->_table . ' m');
        $this->db->join('users a', 'm.users_id = a.users_id');
        $this->db->join('mkaryawan b', 'm.users_id = b.nip');
        if (sizeof($get) > 0) {
            $this->db->where($get);
        }
        if (sizeof($this->_filter) > 0) {
            $this->db->where($this->_filter);
        }
        $this->db->order_by($sort, $order);
        $this->db->limit($rows, $offset);
        $result = $this->db->get();
        $json['total'] = $this->_total($get);
        $json['rows'] = $result->result_array();
        return json_encode($json);
    }

    function read_all($get = array())
    {
        $this->filter();
        $this->db->select('m.*');
        $this->db->select('a.username as username');
        $this->db->select('b.nama as nama');
        $this->db->from($this->_table . ' m');
        $this->db->join('users a', 'm.users_id = a.users_id');
        $this->db->join('mkaryawan b', 'm.users_id = b.nip');
        if (sizeof($this->_filter) > 0) {
            $this->db->where($this->_filter);
        }
        $order = $this->input->post('order') ? $this->input->post('order') : $this->_order;
        $sort = $this->input->post('sort') ? $this->input->post('sort') : $this->_sort;
        $this->db->order_by($sort, $order);
        $result = $this->db->get($this->_view);
        return json_encode($result->result_array());
    }

}