<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>CILEGON SURVEY</title>
    <link rel="icon" type="image/png" href="lib/images/logo.png" />
    <link href="lib/bootstrap/css/bootstrap.css" rel="stylesheet"/>
    <link href="lib/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet"/>
    <link href="lib/surveyjs/css/custom-checkbox.css" type="text/css" rel="stylesheet"/>
    <link href="lib/surveyjs/css/surveyeditor.css" type="text/css" rel="stylesheet"/>
    <link href="lib/surveyjs/css/custom-surveyeditor.css" type="text/css" rel="stylesheet"/>
    <link href="lib/jquery-slimscroll/jquery.slimscroll.min.js" type="text/css" rel="stylesheet"/>
    <link href="lib/jquery-spinner/css/bootstrap-spinner.min.css" type="text/css" rel="stylesheet"/>
    
    <script src="lib/jquery.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="lib/knockout-3.4.2.js"></script>
    <script src="lib/surveyjs/js/survey.ko.min.js"></script>
    <script src="lib/ace.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="lib/worker-json.js" type="text/javascript" charset="utf-8"></script>
    <script src="lib/mode-json.js" type="text/javascript" charset="utf-8"></script>
    <script src="lib/bootstrap-select/js/bootstrap-select.min.js"></script>
    <script src="lib/surveyjs/js/surveyeditor.min.js"></script>
    <script src="lib/jquery.scrollbar/jquery.scrollbar.js"></script>
    <script src="lib/jquery-spinner/js/jquery.spinner.min.js"></script>
    <style>
        .navbar-brand {
            padding: 0px;
        }
        .navbar-brand>img {
            height: 100%;
            padding: 15px;
            width: auto;
        }
        ::-webkit-scrollbar {
            width: 8px;
            height: 5px
        }

        ::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 3px rgba(0, 0, 0, 0.3);
            -webkit-border-radius: 10px;
            border-radius: 10px
        }

        ::-webkit-scrollbar-thumb {
            -webkit-border-radius: 10px;
            border-radius: 10px;
            background: rgba(24, 166, 137, 0.8);
            -webkit-box-shadow: inset 0 0 3px rgba(0, 0, 0, 0.5)
        }

        ::-webkit-scrollbar-thumb:window-inactive {
            background: rgba(0, 165, 81, 0.4)
        }
		.scroll-top-btn {
		display: none;
		position: fixed;
		bottom: 20px;
		right: 30px;
		z-index: 99;
		cursor: pointer;
		width: 50px;
		height: 50px;
		background-image: url(lib/images/Top_50x50.svg);
	}
    </style>
    <script>
        $(function () {
            setTimeout(function () {
                $('.page-loader-wrapper').fadeOut();
            }, 50);   
            window.onscroll = function () { scrollFunction() };

            function scrollFunction() {
                if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
                    document.getElementById("scrollTopBtn").style.display = "block";
                } else {
                    document.getElementById("scrollTopBtn").style.display = "none";
                }
            }
            window.topFunction = function() {
                $('html, body').animate({ scrollTop: 0 }, 'fast');
            }
        });
    </script>

</head>
<body style="zoom:80%;padding: 20px;border: 0px;">
<section style="background-color: white; padding: 10px">
    <form class="form-horizontal">
        <div class="row clearfix">
            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="nama_skm">SKM</label>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" id="nama_skm" class="form-control" placeholder="Masukan Nama SKM">
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix"></div>
    </form>
    <div id="editor" class="survey-editor"></div>
    <script src="assets/js/editor.js"></script>
</section>
</body>
</html>