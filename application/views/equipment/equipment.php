<div class="easyui-layout" data-options="fit:true" style="margin-top: 2px;margin-bottom: 2px;">
    <!--layout center-->
    <div data-options="region:'center',border:false" >
        <!--equipment datagrid-->
        <div class="easyui-layout" data-options="fit:true">
        <div data-options="region:'north',split:true,border:false" style="height: 50%;">
        <?php include('equipment_datagrid.php');?>
        <?php include('equipment_form.php');?>
        </div>
        <!--parameter datagrid-->
        <div data-options="region:'center',border:false" style="height: 50%;">
        <?php include('parameter_datagrid.php');?>
        <?php include('parameter_form.php');?>
        </div>
        </div>
    </div>
    <!--layout right-->
    <div data-options="region:'east',split:true,title:'Image'" style="width:300px;">
        <!--image-->
        <div class="easyui-panel" data-options="border:false" style="height: 50%; overflow: hidden; padding:2px">
            <a id="a_equipment" data-lightbox="roadtrip" href="<?=base_url()?>assets/images/upload/no.jpg">
                <img id="img_equipment" src="<?=base_url()?>assets/images/upload/no.jpg" style="height: 100%; width: 100%;" />
            </a>
        </div>
        <!--specification-->
        <div class="easyui-panel" data-options="border:false,title:'Specification'" style="height: 50%; padding:2px">
        <div id="spec_text" style="width: 100%;">
        </div>
        </div>
    </div>
</div>