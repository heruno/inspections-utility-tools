<!--
/**
 * Description of equipment
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com> 
 * Copyright 2014    
 */
-->
<table id="datagrid_equipment" title="<i class=icon-save icon-large></i> Equipment List" class="easyui-datagrid" style="height:auto; width: 100%;"
       data-options="
                singleSelect : true,
                fit:true,
                url: 'equipment/read',
				method: 'post',
                tools:'#t_equip',
                toolbar:'#tool_button_equipment',
                rownumbers:true,
                pagination:true,
                fitColumns:true,
				multiSort:true,
                striped:true,
                pageSize:20,
                view:groupview,
                groupField:'nomor',
                groupFormatter:function(value,rows){
                    return 'NFC '+ value + ' - (' + rows.length + ' Item)';
                },
                onLoadSuccess:function(){
				    $(this).datagrid('selectRow',0);
                    var gcount = $(this).datagrid('options').view.groups.length;
                    for(var i=0; i<gcount; i++){
                        $(this).datagrid('collapseGroup', i);
                    }
                    $(this).datagrid('expandGroup');
			     },
                onSelect: function(){
                    var dgs = $(this).datagrid('getSelected');
                    console.log(dgs);
                    paramequipment = dgs.idequipment;
                    $('#datagrid_parameter').datagrid({
                    remoteFilter:true,url:'<?= base_url() ?>parameter/read_all/equipment/'+dgs.idequipment
                    },'reload');
                    var urlImg = '<?=base_url('assets/images/upload/')?>/';
                    var fileImg = dgs.image || 'no.jpg';
                    $('#a_equipment').attr('href',urlImg+fileImg);
                    $('#img_equipment').attr('src',urlImg+fileImg);
                    $('#spec_text').html(dgs.spec);
                    $('#idequipment').textbox('setValue',dgs.equipmentid);
                }">
    <thead>
    <tr>
	    <th data-options="field:'idequipment',width:10,align:'center',sortable:true">Equipment ID</th>
        <th data-options="field:'name',width:40,align:'left',sortable:true">Equipment Name</th>
        <th data-options="field:'uid',width:10,align:'center',sortable:true,hidden:false">UID</th>
        <th data-options="field:'nomor',width:10,align:'center',sortable:true,hidden:false">Nomor</th>
        <th data-options="field:'line',width:10,align:'left',sortable:true,hidden:true">Kode Line</th>
        <th data-options="field:'nama_line',width:10,align:'left',sortable:true">Line</th>
        <th data-options="field:'dinas',width:10,align:'left',sortable:true,hidden:true">Kode Dinas</th>
        <th data-options="field:'nama_dinas',width:10,align:'left',sortable:true">Dinas</th>
        <th data-options="field:'spec',width:40,align:'left',sortable:true,hidden:true">Spec</th>
        <th data-options="field:'image',width:40,align:'left',sortable:true,hidden:true">Images</th>
    </tr>
    </thead>
</table>
<div id="t_equip">
&nbsp;<a href="javascript:void(0)" title="ADD" onclick="javascript:myCreate('#dialog_equipment','#form_equipment','equipment/create','Add Equipment')"><i class="icon-plus icon-large"   style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
&nbsp;<a href="javascript:void(0)" title="UPDATE" onclick="javascript:myUpdate('#datagrid_equipment','#dialog_equipment','#form_equipment','equipment/update','Update Equipment')"><i class="icon-pencil icon-large" style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
&nbsp;<a href="javascript:void(0)" title="DELETE" onclick="javascript:myDelete('#datagrid_equipment','equipment/delete', 'equipment/read', 'Hapus Equipment')"><i class="icon-remove icon-large" style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
&nbsp;<a href="javascript:void(0)" title="FILTER" onclick="javascript:filter('#datagrid_equipment','equipment/read')"><i class="icon-filter icon-large" style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
</div>
<!--div id="tool_button_equipment" style="padding:2px;">
    <a href="javascript:void(0)" onclick="javascript:myCreate('#dialog_equipment','#form_equipment','equipment/create','Tambah Data Agama')"
       title="Tambah Data." class="easyui-linkbutton easyui-tooltip c1"><i class="icon-file"></i> Tambah</a>
    <a href="javascript:void(0)" onclick="javascript:myUpdate('#datagrid_equipment','#dialog_equipment','#form_equipment','equipment/update','Update Data Agama')"
       title="Ubah Data." class="easyui-linkbutton easyui-tooltip c8"><i class="icon-edit"></i> Ubah</a>
    <a href="javascript:void(0)" onclick="javascript:myDelete('#datagrid_equipment','equipment/delete', 'equipment/read', 'Hapus Data Agama')"
       title="Hapus Data." class="easyui-linkbutton easyui-tooltip c5"><i class="icon-remove"></i> Hapus</a>
    <a href="javascript:void(0)" title="Filter Data." class="easyui-splitbutton easyui-tooltip pull-right c2" data-options="menu:'#menu_filter_equipment',plain:false">
    <i class="icon-filter"></i> Filter</a>
    <div id="menu_filter_equipment" style="width:100px;">
        <div data-options="iconCls:'icon-filter'" onclick="javascript:filter('#datagrid_equipment','equipment/read')">
        <span>Filter</span>
        </div>
        <div data-options="iconCls:'icon-remove'" onclick="javascript:cancelFilter('#datagrid_equipment','equipment/read')">
        <span>Cancel</span>
        </div>
    </div>
</div-->
<script type="text/javascript">
    var paramequipment;
    mydgrid.onCmenu('#datagrid_equipment');
</script>