<!-- /** * Description of equipment * @created on : 2014-09-26 05:01:55 * @author Heruno Utomo <wolu.88@gmail.com> 
* Copyright 2014    
*/
-->
<div id="dialog_equipment" class="easyui-dialog" modal="true" iconCls="icon-save" closed="true" buttons="#dialog_button_equipment" resizable="true" style="width:800px;">
	<div style="padding:5px">
		<form id="form_equipment" class="easyui-form" method="post" enctype="multipart/form-data">
			<fieldset>
				<legend class="c1">
					Form Equipment
				</legend>
				<p>
					<label for="idequipment">
						CODE :
					</label>
					<input class="easyui-textbox" type="text" name="idequipment" id="idequipment" required="true" size="50"/>
				</p>
				<p>
					<label for="uid">
						CARD ID :
					</label>
					<input class="easyui-combobox" data-options="
					panelWidth:300,
					mode:'local',
					fitColumns:true,
					url: 'master/nfc/read_all',
					valueField: 'kode',
					textField: 'nomor'" type="text" name="uid" id="uid" required="true" size="50"/>
				</p>
				<p>
					<label for="name">
						NAME :
					</label>
					<input class="easyui-textbox" type="text" name="name" id="name" required="true" size="50"/>
				</p>
                <p>
                    <label for="dinas">DINAS :</label>
                    <input class="easyui-combobox" data-options="
                        panelWidth:300,
                        mode:'local',
                        fitColumns:true,
                        url: 'master/dinas/read_all',
                        valueField: 'code',
                        textField: 'name'" type="text" name="dinas" id="dinas" required="true" size="50"/>
                </p>
				<p>
					<label for="line">
						LINE :
					</label>
					<input class="easyui-combobox" data-options="
					panelWidth:300,
					mode:'local',
					fitColumns:true,
					url: 'master/line/read_all',
					valueField: 'code',
					textField: 'name'" type="text" name="line" id="line" required="true" size="50"/>
				</p>
				</td>
			</fieldset>
		</form>
	</div>
</div>
<div id="dialog_button_equipment">
	<a href="javascript:void(0)" title="Save Data." class="easyui-linkbutton easyui-tooltip c1" onclick="javascript:mySave('#datagrid_equipment','#dialog_equipment','#form_equipment','equipment/read')"><i class="icon-save icon-medium"></i> Save</a>
	<a href="javascript:void(0)" title="Close Form." class="easyui-linkbutton easyui-tooltip c2" onclick="javascript:myClose('#dialog_equipment')"><i class=" icon-remove"></i> Close</a>
</div>