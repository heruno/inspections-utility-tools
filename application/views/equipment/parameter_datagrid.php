<table id="datagrid_parameter" title="<i class=icon-save icon-large></i> Healty Condition" class="easyui-datagrid" style="height:auto; width: 100%;"
       data-options="
                singleSelect : true,
                fit:true,
                url: 'parameter/read',
				method: 'post',
                rownumbers:true,
                tools:'#t_param',
                pagination:true,
                fitColumns:true,
				multiSort:true,
                striped:true">
    <thead>
    <tr>           
	    <th data-options="field:'id',width:5,align:'center',sortable:true,hidden:true">ID</th>
        <th data-options="field:'idequipment',width:5,align:'center',sortable:true,hidden:true">EID</th>
        <th data-options="field:'label',width:30,align:'left',sortable:true,formatter:function(val){
	       return '<b>'+val+'</b>';
	    }">PARAMETER</th>
        <th data-options="field:'type',width:10,align:'center',sortable:true">TYPE</th>
        <th data-options="field:'class',width:20,align:'center',sortable:true,hidden:true">CLASS</th>
        <th data-options="field:'flag_msg',width:5,align:'center',sortable:true,hidden:false">FLAG</th>
        <th data-options="field:'value_min',width:5,align:'center',sortable:true,hidden:false">LOW</th>
        <th data-options="field:'value_max',width:5,align:'center',sortable:true,hidden:false">HIGH</th>
        <th data-options="field:'value',width:10,align:'center',sortable:true,hidden:true">DEFAULT</th>
        <th data-options="field:'uom',width:10,align:'center',sortable:true">UNIT</th>
        
        <th data-options="field:'data-options',width:20,align:'left',sortable:true,hidden:true">OPTIONS</th>
        <th data-options="field:'style',width:20,align:'left',sortable:true,hidden:true">STYLE</th>
        <th data-options="field:'img',width:20,align:'center',sortable:true,hidden:true">IMG</th>
    </tr>
    </thead>
</table>
<div id="t_param">
&nbsp;<a href="javascript:void(0)" title="ADD" onclick="javascript:myCreate('#dialog_parameter','#form_parameter','parameter/create','Add parameter');getIdEquipment();"><i class="icon-plus icon-large"   style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
&nbsp;<a href="javascript:void(0)" title="UPDATE" onclick="javascript:myUpdate('#datagrid_parameter','#dialog_parameter','#form_parameter','parameter/update','Update parameter')"><i class="icon-pencil icon-large" style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
&nbsp;<a href="javascript:void(0)" title="DELETE" onclick="javascript:myDelete('#datagrid_parameter','parameter/delete', 'parameter/read/'+paramequipment, 'Hapus parameter')"><i class="icon-remove icon-large" style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
&nbsp;<a href="javascript:void(0)" title="FILTER" onclick="javascript:filter('#datagrid_parameter','parameter/read_all/equipment/'+paramequipment)"><i class="icon-filter icon-large" style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
</div>
<script type="text/javascript">
function getIdEquipment(){
    var ide = $('#datagrid_equipment').datagrid('getSelected');
    if(ide) $('#form_parameter').form('load',{idequipment:ide.idequipment});
}
$(function(){
    mydgrid.onCmenu('#datagrid_parameter');
});
</script>