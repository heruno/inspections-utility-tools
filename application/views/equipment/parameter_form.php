<!--
/**
 * Description of parameter
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com> 
 * Copyright 2014    
 */
-->
<style>
</style>
<div id="dialog_parameter" class="easyui-dialog"
     modal="true"
     iconCls="icon-save"
     closed="true"
     buttons="#dialog_button_parameter"
     resizable="true"
     style="width:90%; padding:10px">
    <form id="form_parameter" class="easyui-form" method="post">
        <fieldset style="padding:10px">
            <legend class="c1">Form Parameter</legend>
            <input type="hidden" name="id"/>
            <table style="width: 100%;">
                <tr>
                    <td>
                        <label for="idequipment">Equipment Code :</label>
                        <input class="easyui-textbox" type="text" readonly="readonly" name="idequipment"
                               id="idequipment" required="true" size="50"/>
                    </td>
                    <td>
                        <label for="label">Label :</label>
                        <input class="easyui-textbox" type="text" name="label" id="label" required="true" size="50"/>
                    </td>
                </tr>
                <tr>
                    </td>
                    <td>
                        <label for="class">Class :</label>
                        <input class="easyui-combobox" data-options="
                        panelWidth:300,
                        mode:'local',
                        fitColumns:true,
                        url: 'dd_input/read_all',
                        valueField: 'class',
                        textField: 'title',
                        onSelect:function(r){
                         $('#type').combobox('setValue', r.type); 
                        }" type="text" name="class" id="class" required="true" size="50"/>
                    </td>
                    <td>
                        <label for="type">Type :</label>
                        <input class="easyui-combobox" data-options="
                		valueField: 'label',
                		textField: 'value',
                		data: [{
                			label: 'text',
                			value: 'text'
                		},{
                			label: 'number',
                			value: 'number'
                		},{
                			label: 'date',
                			value: 'date'
                		}]" type="text" name="type" id="type" required="true" size="50"/>
                    </td>
                </tr>
                <tr>

                    <td>
                        <label for="value_max">Max Value :</label>
                        <input class="easyui-textbox" type="text" name="value_max" id="value_max" required="true"
                               size="50"/>
                    </td>
                    <td>
                        <label for="value_min">Min Value :</label>
                        <input class="easyui-textbox" type="text" name="value_min" id="value_min" required="true"
                               size="50"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="flag_msg">Notification Flag :</label>
                        <input class="easyui-combobox" type="text" data-options="
                		valueField: 'value',
                		textField: 'label',
                		data: [{
                			label: '0 = TIDAK ADA KALKULASI',
                			value: '0'
                		},{
                			label: '1 = NOTIFIKASI JIKA NILAI RENDAH DARI STANDAR',
                			value: '1'
                		},{
                			label: '2 = NOTIFIKASI JIKA NILAI RENDAN ATAU LEBIH TINGGI DARI STANDAR',
                			value: '2'
                		}]" name="flag_msg" id="flag_msg" required="true" size="50"/>
                    </td>
                    <td>
                        <label for="value">Default Value:</label>
                        <input class="easyui-textbox" type="text" name="value" id="value" size="50"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="uom">Unit of Measure :</label>
                        <input type="text" class="easyui-textbox" name="uom" id="uom" required="true" size="50"/>
                    </td>
                    <td>
                        <!--label for="img">Image :</label>
                        <input class="easyui-textbox" type="text" name="img" id="img" required="true" size="50"/-->
                        <label for="value_standar">Value Standar :</label>
                        <input type="text" class="easyui-textbox" name="value_standar" id="value_standar" size="50"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="style">Style :</label>
                        <input class="easyui-textbox" type="text" name="style" id="style" required="true" size="50"
                               style="height:160px;" data-options="multiline:true"/>
                    </td>
                    <td>
                        <label for="data-options">Data Options :</label>
                        <input class="easyui-textbox" type="text" name="data-options" id="data-options" required="true"
                               size="50" style="height: 160px;" data-options="multiline:true"/>
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>
</div>
<div id="dialog_button_parameter">
    <a href="javascript:void(0)" title="Save Data." class="easyui-linkbutton easyui-tooltip c1"
       onclick="javascript:mySave('#datagrid_parameter','#dialog_parameter','#form_parameter','parameter/read/'+paramequipment)"><i
                class="icon-save icon-medium"></i> Save</a>
    <a href="javascript:void(0)" title="Close Form." class="easyui-linkbutton easyui-tooltip c2"
       onclick="javascript:myClose('#dialog_parameter')"><i class=" icon-remove"></i> Close</a>
</div>