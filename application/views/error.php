<style>
.error-page {
text-align: center;
padding: 20px;
}
</style>
<div class="content">
<div class="error-page">
	<header class="error-page__header">
		<img class="error-page__header-image" src="<?=base_url('assets/images/sad-computer.gif')?>" alt="Sad computer"/>
		<h1 class="error-page__title nolinks">
			Page Not Found
		</h1>
	</header>
	<p class="error-page__message">
		The page you are looking for could not be found.
	</p>
</div>
</div>
