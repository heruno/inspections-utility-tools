<div class="easyui-panel" data-options="fit:true" style="padding-top:3px;border:0px">
    <div id="log">

    </div>
</div>
<script type="text/javascript">
    $(function ()
    {
        $.messager.confirm('GENERATE ONLINE', 'APAKAH ANDA AKAN KONFIRMASI GENERATE ONLINE?', function(r){
            if (r){
                var win = $.messager.progress({
                    title:'Silahkan tunggu sampai progress selesai',
                    msg:'Loading data...'
                });
                $.get('api/generate', function (data) {
                    $('#log').html(data);
                    $.messager.progress('close');
                });
            }
        });

    });
</script>