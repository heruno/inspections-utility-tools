<style>
    .capture img{
        height: 64px;
        width: 64px;
        margin : 5px;
        border: 1px solid #3f51b5;
        border-radius: 5px;
    }
</style>
<div class="easyui-layout" data-options="fit:true">
    <!--equipment datagrid-->
    <div class="easyui-layout" data-options="fit:true">
        <div data-options="region:'north',split:true,border:false" style="height: 50%;margin-top:2px">
            <?php include('inspection_header_datagrid.php'); ?>
        </div>
        <!--parameter datagrid-->
        <div data-options="region:'center',border:false" style="height: 50%;">
            <div class="easyui-layout" data-options="fit:true">
                <div data-options="region:'center',border:false" style="height: 70%;">
                    <?php include('inspection_detail_datagrid.php'); ?>
                </div>
                <div data-options="region:'east',split:true" style="width:400px;padding:2px">
                    <div id="capture" class="easyui-tabs" style="width:100%;height:100%" data-options="fit:true">
                        <div id="notex" title="NOTE" style="padding:10px">
                        </div>
                        <div id="photox" title="PHOTO">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--script type="text/javascript">
    $(document).ready(function () {
        $('#capture').tabs({
            border: false,
            onSelect: function (title) {
                lightbox.option({
                    'resizeDuration': 200,
                    'wrapAround': true
                })
            }
        });
    });
</script-->