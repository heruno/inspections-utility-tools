<table id="datagrid_inspection" title="<i class=icon-save icon-large></i> Healty Condition" class="easyui-datagrid" style="height:auto; width: 100%;"
       data-options="
                singleSelect : true,
                fit:true,
                url: 'inspection/read_all',
				method: 'post',
                tools:'#t_detail',
                rownumbers:true,
                pagination:false,
                fitColumns:true,
				multiSort:true,
                striped:true">
    <thead>
    <tr> 
        <th data-options="field:'idparam',width:50,align:'center',sortable:true,hidden:true" rowspan="2">ID</th> 
        <th data-options="field:'label',sortable:true,width:250,formatter:function(val){
	       return '<b>'+val+'</b>';
	    }" rowspan="2">Parameters</th>
        <th colspan="2">Inspection</th> 
        <th colspan="2">Threshold</th> 
        <th data-options="field:'state',width:90,align:'center',sortable:true,formatter:function(a,b){
            if(b.state == 0) return '<i class=icon-remove icon-large style=color:#3f51b5;font-size:17px;></i>';
            if(b.state == 1) return '<i class=icon-ok icon-large style=color:lime;font-size:17px></i>';
            if(b.state == 2) return '<i style=color:blue;font-size:15px>-</i>';
        }" rowspan="2">Conditions</th> 
        
        <th data-options="field:'remarks',width:250,sortable:true,formatter:function(a,b){
            if(b.state == 0) return '<i style=color:#3f51b5;font-size:15px;>Send Notifications</i>';
            if(b.state == 1) return '<i style=color:lime;font-size:15px>Good Conditions</i>';
            if(b.state == 2) return '<i style=color:blue;font-size:15px>-</i>';
        }" rowspan="2">Remarks</th> 
    </tr> 
    <tr> 
        <th data-options="field:'value',width:90,align:'center'">Result</th> 
        <th data-options="field:'uom',width:90,align:'center'">Unit Measure</th>  
        <th data-options="field:'value_min',width:50,align:'center'">Min</th> 
        <th data-options="field:'value_max',width:50,align:'center'">Max</th> 
    </tr>
    </thead>
</table>
<div id="t_detail">
<a title="Filter" href="javascript:void(0)" onclick="javascript:filter('#datagrid_inspection','inspection/read_all/'+paraminsp)"><i class="icon-filter icon-large" style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
</div>
<script type="text/javascript">
$(function(){
    mydgrid.onCmenu('#datagrid_inspection');
});
</script>