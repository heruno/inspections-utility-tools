<!--
/**<strong></strong>
 * Description of inspectionlot
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com> 
 * Copyright 2014    
 */
-->
<table id="datagrid_inspectionlot" title="<i class=icon-save icon-large></i> inspectionlot List" class="easyui-datagrid"
       style="height:auto; width: 100%;"
       data-options="
                singleSelect : true,
                fit:true,
                url: 'inspectionlot/read',
				method: 'post',
                rownumbers:true,
                tools:'#t_header',
                toolbar:'#tool_button_inspection',
                pagination:true,
                fitColumns:true,
				multiSort:true,
                striped:true,
                view:groupview,
                groupField:'name',
                groupFormatter:function(value,rows){
                    return value + ' - (' + rows.length + ' Item)';
                },
                onLoadSuccess:function(){
                var gcount = $(this).datagrid('options').view.groups.length;
                    for(var i=0; i<gcount; i++){
                        $(this).datagrid('collapseGroup', i);
                    }
				    $(this).datagrid('selectRow',0);
			     },onSelect: function(index,row){
			         $('#notex').html(row.note ? row.note : '-');
			          getPhoto(row.localid);
			          /*
			         paraminsp = row.inspectionid;
                    $('#datagrid_inspection').datagrid({
                        remoteFilter: true,
                        url:'<?= base_url() ?>inspection/read_all/'+row.inspectionid,
                        rowStyler: function(index,row)
                        {
                		if (row.label == 'Note')
                		{
                            var opts = $(this).datagrid('options');
                            opts.finder.getTr(this, index).hide();
                		}
                	}
                    },'reload');*/

                }">
    <thead>
    <tr>
        <th data-options="field:'localid',width:10,align:'center',sortable:true,hidden:true">ID</th>
        <th data-options="field:'inspectionid',width:10,align:'center',sortable:true,hidden:true">ID</th>
        <th data-options="field:'idequipment',width:40,align:'center',sortable:true,hidden:true">EquipID</th>
        <th data-options="field:'name',width:150,align:'left',sortable:true,hidden:true,formatter:function(val){
	       return '<b>'+val+'</b>';
	    }">Equipment
        </th>
        <th data-options="field:'localdt',width:40,align:'center',sortable:true,hidden:false">Actual DateTime</th>
        <th data-options="field:'inputdt',width:40,align:'center',sortable:true,hidden:true">Sync Date</th>
        <th data-options="field:'inputtm',width:40,align:'center',sortable:true,hidden:true">Sync Time</th>
        <th data-options="field:'idinspector',width:40,align:'center',sortable:true,hidden:true">Inspector</th>
        <th data-options="field:'nama',width:100,align:'center',sortable:true,hidden:false">Inspector</th>
        <th data-options="field:'shift',width:40,align:'center',sortable:true,hidden:false">Shift</th>
        <th data-options="field:'status',width:40,align:'center',sortable:true,hidden:false,formatter:function(val){
            if(val == 1) return '<b>RUNNING</b>';
            if(val == 2) return '<b>STANDBY</b>';
        }">Status
        </th>
        <th data-options="field:'note',width:40,align:'center',sortable:true,hidden:true">Note</th>
        <!--th data-options="field:'conditions',width:40,align:'center',sortable:true,hidden:false,formatter:function(a,b){
            if(b.conditions == 0)  return '<i class=icon-remove icon-large style=color:#3f51b5;font-size:17px;></i>';
            if(b.conditions == 1)  return '<i class=icon-ok icon-large style=color:lime;font-size:17px></i>';
        }">Conditions</th-->
    </tr>
    </thead>
</table>
<div id="tool_button_inspection" style="padding:2px;">
    <label style="width: auto">Filter Tanggal</label>
    <input id="filter-equipment" class="easyui-datebox" style="width:20%;" />
</div>
<div id="t_header">
    <a title="Filter" href="javascript:void(0)"
       onclick="javascript:filter('#datagrid_inspectionlot','inspectionlot/read')">
        <i class="icon-filter icon-large"
           style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i>
    </a>
</div>
<script type="text/javascript">
    var paraminsp;
    mydgrid.onCmenu('#datagrid_inspectionlot');
    $('#filter-equipment').datebox({
        onSelect: function(date){
            var tgl =date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
            $('#datagrid_inspectionlot').datagrid({
                remoteFilter: true,
                url:'<?= base_url() ?>inspectionlot/read/'+tgl
            },'reload');
        }
    });
    function getPhoto(localid) {
        $("#photox").empty();
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: '<?=base_url('photo/read_all')?>/' + localid,
            crossDomain: true,
            success: function (result) {
                $.each(result, function (index, key) {
                    var img = document.createElement("img");
                    var a = document.createElement("a");
                    var dl = document.createAttribute("data-lightbox");
                    var cl = document.createAttribute("class");
                    img.name = "potox";
                    img.style = "width:25%;height:auto;margin:2px";
                    dl.value = "roadtrip";
                    cl.value = "capture";
                    a.setAttributeNode(dl);
                    a.setAttributeNode(cl);
                    var foo = document.getElementById("photox");
                    img.src = '<?=base_url('api/uploads/capture')?>/' + key.src;
                    a.href = '<?=base_url('api/uploads/capture')?>/' + key.src;
                    a.appendChild(img);
                    foo.appendChild(a);
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest, textStatus, errorThrown);
            }
        });
    }
</script>