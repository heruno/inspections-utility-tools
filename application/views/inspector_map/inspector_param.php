<div style="padding:10px">
    <fieldset class="c7">
        <table style="width: 100%">
            <tr>
                <td style="width: 45%">
                    <b style="padding: 2px">DINAS</b> <br/>
                    <input id="list-dinas" class="easyui-combobox pull-left" style="width: 100%"
                           id="params_id"
                           name="params_id"
                           data-options="
                    url:'<?= base_url() ?>master/dinas/read_all',
                    method:'get',
                    valueField:'code',
                    textField:'name',
                    panelHeight:'auto',
                    onSelect:openMapInspector,
                    onSelect: function (v,i) {
                    $('#list-inspector').combobox({
                        url: '<?= base_url() ?>master/inspector/read_all/' + v.code
                        }, 'reload');
                    }"/>
                </td>
                <td style="width:10%;"></td>
                <td style="width: 45%">
                    <b style="padding: 2px">INSPECTOR</b> <br/>
                    <input id="list-inspector" class="easyui-combobox pull-left" style="width: 100%"
                           id="params_id"
                           name="params_id"
                           data-options="
                    method:'get',
                    valueField:'nik',
                    textField:'nama',
                    panelHeight:'auto',
                    onSelect:openMapInspector,
                    onSelect: function (v,i) {
                    reload_map_params_inspector(v.nik);
                    }"/>
                </td>
            </tr>
        </table>
    </fieldset>

    <table style="width: 100%;">
        <tr>

            <td style="width: 47%;">
                <?php include('inspector_param_datagrid.php'); ?>
            </td>
            <td style="width:6%;">
                <a href="javascript:void(0)" onclick="javascript:addMapInspector()" class="easyui-linkbutton c1"
                   style="width: 45px;"><i class="icon-circle-arrow-right large"></i></a>
                <br/>&nbsp;<br/>
                <a href="javascript:void(0)" onclick="javascript:delMapInspector()" class="easyui-linkbutton c5"
                   style="width: 45px;"><i class="icon-circle-arrow-left large"></i></a>
            </td>
            <td style="width: 47%;">
                <?php include('inspector_param_map_datagrid.php'); ?>
            </td>

        </tr>
    </table>
</div>
<script type="text/javascript">
    var ur_datagrid_inspector = $('#datagrid_inspector');
    var ur_datagrid_map_inspector = $('#datagrid_map_inspector');

    function reload_map_params_inspector(nik) {
        var dinas = $('#list-dinas').combobox('getValue');
        ur_datagrid_inspector.datagrid({
            remoteFilter: true,
            url: '<?=base_url()?>parameter/read_all/'+dinas+'/' + nik +'/source'
        }, 'reload');

        ur_datagrid_map_inspector.datagrid({
            remoteFilter: true,
            url: '<?=base_url()?>inspector_map/getMap/'+ nik
        }, 'reload');

        ur_datagrid_inspector.datagrid('enableFilter');
        ur_datagrid_map_inspector.datagrid('enableFilter');
    }

    function openMapInspector() {
        var val = $('#params_id').combobox('getValue');
        if (val) {
            reload_map_params_inspector(val);
        } else {
            $.messager.alert('Warning', 'Silahkan pilih Data ', 'warning');
            $('#params_id').combobox('clear');
        }
    }

    function addMapInspector() {
        var data = [];
        var v_id = $('#list-inspector').combobox('getValue');
        var rows2 = ur_datagrid_inspector.datagrid('getSelections');

        var x;
        for (x = 0; x < rows2.length; x++) {
            var field = {};
            field['nik'] = v_id;
            field['id'] = rows2[x].id;
            data.push(field);
        }
        alert(JSON.stringify(data));
        if (rows2.length == data.length) {
            $.ajax({
                url: '<?=base_url()?>inspector_map/create_batch',
                type: "POST",
                data: JSON.stringify(data),
                processData: false,
                contentType: "application/json; charset=UTF-8",
                success: function (data) {
                    var result = JSON.parse(data);
                    if (result.msg) {
                        reload_map_params_inspector(v_id);
                    }
                }
            });
        }
    }

    function delMapInspector() {
        var datax = [];
        var v_id = $('#list-inspector').combobox('getValue');
        var rows1 = ur_datagrid_map_inspector.datagrid('getSelections');
        var x;

        for (x = 0; x < rows1.length; x++) {
            var fieldx = {};
            fieldx['nik'] = v_id;
            fieldx['id'] = rows1[x].id;
            datax.push(fieldx);
        }
        if (rows1.length == datax.length) {
            $.ajax({
                url: '<?=base_url('inspector_map/delete_batch')?>',
                type: "POST",
                data: JSON.stringify(datax),
                processData: false,
                contentType: "application/json; charset=UTF-8",
                success: function (data) {
                    var result = JSON.parse(data);
                    if (result.msg) {
                        reload_map_params_inspector(v_id);
                    }
                }
            });
        }
    }
</script>