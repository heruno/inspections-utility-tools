<table id="datagrid_inspector" title="<i class=icon-save icon-large></i> List Paramater Equipment" class="easyui-datagrid"
       style="height:400px; width: 99%;"
       data-options="
                singleSelect : false,
				method: 'post',
                fitColumns:true,
				multiSort:true,
                striped:true,
                view:groupview,
                groupField:'name',
                groupFormatter:function(value,rows){
                    return value + ' - ' + rows.length + ' Item(s)';
                }">
    <thead>
    <tr>
        <th data-options="field:'ck',checkbox:true"></th>
        <th data-options="field:'idequipment',width:50,align:'center',sortable:true,hidden:true">ID</th>
        <th data-options="field:'id',width:100,align:'left',sortable:true,hidden:true">ID</th>
        <th data-options="field:'label',width:100,align:'left',sortable:true">Label</th>
    </tr>
    </thead>
</table>