<!DOCTYPE html>
<html>
	<head>
		<title><?=$title?>-<?=$_SERVER[ "HTTP_HOST"]?></title>
					<meta charset="utf-8"/>
					<meta name="viewport"content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0"/>
					<meta http-equiv="Cache-Control"content="no-cache, no-store, must-revalidate"/>
					<meta http-equiv="Cache-Directive"content="no-cache"/>
					<meta http-equiv="Pragma-directive"content="no-cache"/>
					<meta http-equiv="Pragma"content="no-cache"/>
					<meta http-equiv="Expires"content="0"/>
					<meta name="robots"content="noindex,follow"/>
					<meta http-equiv="X-UA-Compatible"content="IE=edge"/>
					<meta name="description"content="Krakatau-IT"/>
					<link rel="shortcut icon"href="<?=base_url( 'assets/images/serang.png')?>"type="image/x-icon"/>
					<link rel="stylesheet"type="text/css"href="<?=base_url('assets/css/font-awesome/css/font-awesome.php')?>"/>
					<link rel="stylesheet"type="text/css"href="<?=base_url('assets/css/themes/bootstrap/easyui.php')?>"/>
					<link rel="stylesheet"type="text/css"href="<?=base_url('assets/css/themes/icon.php')?>"/>
					<link rel="stylesheet"type="text/css"href="<?=base_url('assets/css/style.php')?>"/>
					<link rel="stylesheet"type="text/css"href="<?=base_url('assets/lightbox/css/lightbox.css')?>"/>

					<script type="text/javascript" src="<?=base_url( 'assets/js/js.php')?>"></script>
					<script type="text/javascript" src="<?=base_url( 'assets/lightbox/js/lightbox.js')?>"></script>

					<!--script src="<?=base_url('assets/highcharts/js/highcharts.js')?>"></script>
					<script src="<?=base_url('assets/highcharts/js/modules/exporting.js')?>"></script-->
                    <style>
                    .tabs-header,
					.tabs-tool{
                        background-color:transparent;
                     }
                    </style>
					</head>
					<?php flush();?>
						<body class="easyui-layout">
							<div id="header"class="palette-Indigo bg"data-options="href:'partial/partial/header',region:'north',border:false"></div>
							<div id="sidebar"data-options="href:'partial/partial/tree',region:'west',split:true,title:'<i class=icon-sitemap icon-large></i> FUNCTION EXPLORER'"></div>
							<div id="footer"class="palette-Indigo bg"data-options="href:'partial/partial/footer',region:'south',border:false"></div>
							<?php $this->template->display('partial/tab_menu');?>
								<div data-options="region:'center',split:true" style="padding:2px;">
									<div id="tt"class="easyui-tabs"data-options="fit:true,border:false, tools:'#tab-tools', plain:false">
										<?=$contents;?>
									</div>
								</div>
						</body>
						
						</html>