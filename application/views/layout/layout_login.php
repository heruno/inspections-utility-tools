<!doctype html>
<html>
	<head>
		<title>EQUIPMENT INFORMATION SYSTEM</title>
		<meta charset="UTF-8"/>
		<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
		<meta http-equiv="Cache-Directive" content="no-cache"/>
		<meta http-equiv="Pragma-directive" content="no-cache"/>
		<meta http-equiv="Pragma" content="no-cache"/>
		<meta http-equiv="Expires" content="0"/>
		<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"/>
		<meta name="robots" content="noindex,follow"/>
		<meta name="description" content="Equipment Management System"/>
		<link rel="shortcut icon" href="<?=base_url('assets/images/serang.png')?>" type="image/x-icon"/>
		<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/themes/bootstrap/easyui.css')?>"/>
		<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/style.php')?>"/>
		<script type="text/javascript" src="<?=base_url('assets/js/jquery.min.js')?>"></script>
		<script type="text/javascript" src="<?=base_url('assets/js/jquery.easyui.min.js')?>"></script>
	</head>
	<body class="easyui-layout">
		<div data-options="region:'center',border:false" class="palette-Indigo bg" style="padding: 1px;">
		<?php print $contents;?>
		</div>
		<div id="footer" class="palette-Indigo bg" data-options="region:'south',border:false">
			<small>Copyright &copy; 2016 | PT.KRAKATAU INFORMATION TECHNOLOGY | All Rights Reserved.</small>
		</div>
	</body>

</html>