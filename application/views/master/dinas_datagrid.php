<table id="datagrid_dinas" title="<i class=icon-save icon-large></i> Daftar dinas" class="easyui-datagrid" style="height:auto; width: 100%;"
       data-options="
                singleSelect : true,
                fit:true,
                url:'master/dinas/read',
				method: 'post',
                tools:'#tool_button_dinas',
                rownumbers:true,
                pagination:true,
                fitColumns:true,
				multiSort:true,
                striped:true">
    <thead>
    <tr> 
        <th data-options="field:'code',width:10,align:'center',sortable:true,hidden:false">KODE DINAS</th>
        <th data-options="field:'name',width:20,align:'left',sortable:true">NAMA DINAS</th>
              
        <!-----tile coloumn---------------------->
        <th data-options="field:'create_user',width:0,align:'left',sortable:true,hidden:true">Create user</th>
        <th data-options="field:'create_date',width:0,align:'left',sortable:true,hidden:true">Create date</th>
        <th data-options="field:'create_ip',width:0,align:'left',sortable:true,hidden:true">Create ip</th>
        <th data-options="field:'update_user',width:0,align:'left',sortable:true,hidden:true">Update user</th>
        <th data-options="field:'update_date',width:0,align:'left',sortable:true,hidden:true">Update date</th>
        <th data-options="field:'update_ip',width:0,align:'left',sortable:true,hidden:true">Update ip</th>
		<!-----tile coloumn---------------------->
    </tr>
    </thead>
</table>
<div id="tool_button_dinas" style="padding:10px;">
&nbsp;<a href="javascript:void(0)" title="ADD" onclick="javascript:myCreate('#dialog_dinas','#form_dinas','master/dinas/create','Add dinas')"><i class="icon-plus icon-large"   style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
&nbsp;<a href="javascript:void(0)" title="UPDATE" onclick="javascript:myUpdate('#datagrid_dinas','#dialog_dinas','#form_dinas','master/dinas/update','Update dinas')"><i class="icon-pencil icon-large" style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
&nbsp;<a href="javascript:void(0)" title="DELETE" onclick="javascript:myDelete('#datagrid_dinas','master/dinas/delete', 'master/dinas/read', 'Hapus dinas')"><i class="icon-remove icon-large" style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
&nbsp;<a href="javascript:void(0)" title="FILTER" onclick="javascript:filter('#datagrid_dinas','master/dinas/read')"><i class="icon-filter icon-large" style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>

</div>