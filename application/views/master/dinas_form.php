<div id="dialog_dinas" class="easyui-dialog"
     modal="true"
     iconCls="icon-save"
     closed="true"
     buttons="#dialog_button_dinas"
     resizable="true"
     style="width:800px;">
    <div style="padding:5px">
        <form id="form_dinas" class="easyui-form" method="post">
            <fieldset>
                <legend class="c1">FORM DINAS</legend>
                <p>
                    <label for="code">KODE DINAS :</label>
                    <input class="easyui-textbox" type="text" name="code" id="code" required="true" size="50"/>
                </p>
                <p>
                    <label for="dinas">NAMA DINAS :</label>
                    <input class="easyui-textbox" type="text" name="name" id="name" required="true" size="50"/>
                </p>
            </fieldset>
        </form>
    </div>
</div>
<div id="dialog_button_dinas">
<a href="javascript:void(0)" title="Save Data." class="easyui-linkbutton easyui-tooltip c1" onclick="javascript:mySave('#datagrid_dinas','#dialog_dinas','#form_dinas','master/dinas/read')"><i class="icon-save icon-medium"></i> Save</a>
<a href="javascript:void(0)" title="Tutup Form." class="easyui-linkbutton easyui-tooltip c2" onclick="javascript:myClose('#dialog_dinas')"><i class=" icon-remove"></i> Close</a>
</div>