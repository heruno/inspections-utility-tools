<!--
/**
 * Description of inspector
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com> 
 * Copyright 2014    
 */
-->
<table id="datagrid_inspector" title="<i class=icon-save icon-large></i> Daftar Karyawan" class="easyui-datagrid" style="height:auto; width: 100%;"
       data-options="
                singleSelect : true,
                fit:true,
                url:'master/inspector/read',
				method: 'post',
                tools:'#tool_button_inspector',
                rownumbers:true,
                pagination:true,
                fitColumns:true,
				multiSort:true,
                striped:true">
    <thead>
    <tr>
	    <th data-options="field:'nik',width:20,align:'center',sortable:true">NIK</th>
        <th data-options="field:'nama',width:40,align:'left',sortable:true">Nama</th>
        <th data-options="field:'dinas',width:20,align:'center',sortable:true,hidden:true">KodeDinas</th>
        <th data-options="field:'line',width:20,align:'center',sortable:true,hidden:true">KodeLine</th>
        <th data-options="field:'nama_dinas',width:20,align:'center',sortable:true">Dinas</th>
        <th data-options="field:'nama_line',width:20,align:'center',sortable:true">Line</th>
        <!--th data-options="field:'pass',width:30,align:'left',sortable:true">Password</th-->
              
    </tr>
    </thead>
</table>
<div id="tool_button_inspector" style="padding:10px;">
&nbsp;<a href="javascript:void(0)" title="ADD" onclick="javascript:myCreate('#dialog_inspector','#form_inspector','master/inspector/create','Add inspector')"><i class="icon-plus icon-large"   style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
&nbsp;<a href="javascript:void(0)" title="UPDATE" onclick="javascript:myUpdate('#datagrid_inspector','#dialog_inspector','#form_inspector','master/inspector/update','Update inspector')"><i class="icon-pencil icon-large" style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
&nbsp;<a href="javascript:void(0)" title="DELETE" onclick="javascript:myDelete('#datagrid_inspector','master/inspector/delete', 'master/inspector/read', 'Hapus inspector')"><i class="icon-remove icon-large" style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
&nbsp;<a href="javascript:void(0)" title="FILTER" onclick="javascript:filter('#datagrid_inspector','master/inspector/read')"><i class="icon-filter icon-large" style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
</div>