<!--
/**
 * Description of inspector
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com> 
 * Copyright 2014    
 */
-->
<div id="dialog_inspector" class="easyui-dialog"
     modal="true"
     iconCls="icon-save"
     closed="true"
     buttons="#dialog_button_inspector"
     resizable="true"
     style="width:800px;">
    <div style="padding:5px">
        <form id="form_inspector" class="easyui-form" method="post">
            <fieldset>
                <legend class="c1">FORM INSPECTOR</legend>
                <p>
                    <label for="nik">NIK :</label>
                    <input class="easyui-textbox" type="text" name="nik" id="nik" required="true" size="50"/>
                </p>
                <p>
                    <label for="nama">NAMA :</label>
                    <input class="easyui-textbox" type="text" name="nama" id="nama" required="true" size="50"/>
                </p>

                <p>
                    <label for="dinas">Dinas :</label>
                    <input class="easyui-combobox" data-options="
                                panelWidth:300,
                                mode:'local',
                                fitColumns:true,
                                url: 'master/dinas/read_all',
                                valueField: 'code',
                                textField: 'name'" type="text" name="dinas" required="true" size="50"/>
                </p>
                <p>
                    <label for="line">Line :</label>
                    <input class="easyui-combobox" data-options="
                                panelWidth:300,
                                mode:'local',
                                fitColumns:true,
                                url: 'master/line/read_all',
                                valueField: 'code',
                                textField: 'name'" type="text" name="line" required="true" size="50"/>
                </p>
            </fieldset>
        </form>
    </div>
</div>
<div id="dialog_button_inspector">
<a href="javascript:void(0)" title="Save Data." class="easyui-linkbutton easyui-tooltip c1" onclick="javascript:mySave('#datagrid_inspector','#dialog_inspector','#form_inspector','master/inspector/read')"><i class="icon-save icon-medium"></i> Simpan</a>
<a href="javascript:void(0)" title="Tutup Form." class="easyui-linkbutton easyui-tooltip c2" onclick="javascript:myClose('#dialog_inspector')"><i class=" icon-remove"></i> Tutup</a>
</div>