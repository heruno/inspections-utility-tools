<div id="dialog_jabatan" class="easyui-dialog"
     modal="true"
     iconCls="icon-save"
     closed="true"
     buttons="#dialog_button_jabatan"
     resizable="true"
     style="width:800px;">
    <div style="padding:5px">
        <form id="form_jabatan" class="easyui-form" method="post">
            <fieldset>
                <legend class="c1">Form jabatan</legend>
                <p>
                    <label for="idjabatan">ID JABATAN :</label>
                    <input class="easyui-textbox" type="text" name="idjabatan" id="idjabatan" required="true" size="50"/>
                </p>
                <p>
                    <label for="jabatan">Jabatan :</label>
                    <input class="easyui-textbox" type="text" name="jabatan" id="jabatan" required="true" size="50"/>
                </p>
            </fieldset>
        </form>
    </div>
</div>
<div id="dialog_button_jabatan">
<a href="javascript:void(0)" title="Save Data." class="easyui-linkbutton easyui-tooltip c1" onclick="javascript:mySave('#datagrid_jabatan','#dialog_jabatan','#form_jabatan','master/jabatan/read')"><i class="icon-save icon-medium"></i> Save</a>
<a href="javascript:void(0)" title="Tutup Form." class="easyui-linkbutton easyui-tooltip c2" onclick="javascript:myClose('#dialog_jabatan')"><i class=" icon-remove"></i> Close</a>
</div>
<script type="text/javascript">
$('#id_jabatan').combogrid({
    panelWidth:300,
    mode:'local',
    fitColumns:true,
    url: 'attribute/ajabatan/read_all',
    idField: 'id_jabatan',
    textField: 'nama',
    columns: [[
        {field:'id_jabatan',title:'ID',width:200,sortable:true,hidden:true},
        {field:'nama',title:'Nama',width:300,sortable:true}
    ]]
});
</script>