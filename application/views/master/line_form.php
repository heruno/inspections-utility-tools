<div id="dialog_line" class="easyui-dialog"
     modal="true"
     iconCls="icon-save"
     closed="true"
     buttons="#dialog_button_line"
     resizable="true"
     style="width:800px;">
    <div style="padding:5px">
        <form id="form_line" class="easyui-form" method="post">
            <fieldset>
                <legend class="c1">Form Unit</legend>
                <p>
                    <label for="idline">KODE UNIT :</label>
                    <input class="easyui-textbox" type="text" name="code" id="code" required="true" size="50"/>
                </p>
                <p>
                    <label for="line">NAMA UNIT :</label>
                    <input class="easyui-textbox" type="text" name="name" id="name" required="true" size="50"/>
                </p>
            </fieldset>
        </form>
    </div>
</div>
<div id="dialog_button_line">
<a href="javascript:void(0)" title="Save Data." class="easyui-linkbutton easyui-tooltip c1" onclick="javascript:mySave('#datagrid_line','#dialog_line','#form_line','master/line/read')"><i class="icon-save icon-medium"></i> Save</a>
<a href="javascript:void(0)" title="Tutup Form." class="easyui-linkbutton easyui-tooltip c2" onclick="javascript:myClose('#dialog_line')"><i class=" icon-remove"></i> Close</a>
</div>
<script type="text/javascript">
$('#id_line').combogrid({
    panelWidth:300,
    mode:'local',
    fitColumns:true,
    url: 'attribute/aline/read_all',
    idField: 'id_line',
    textField: 'nama',
    columns: [[
        {field:'id_line',title:'ID',width:200,sortable:true,hidden:true},
        {field:'nama',title:'Nama',width:300,sortable:true}
    ]]
});
</script>