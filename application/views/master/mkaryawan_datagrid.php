<!--
/**
 * Description of mkaryawan
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com> 
 * Copyright 2014    
 */
-->
<table id="datagrid_mkaryawan" title="<i class=icon-save icon-large></i> Daftar Karyawan" class="easyui-datagrid" style="height:auto; width: 100%;"
       data-options="
                singleSelect : true,
                fit:true,
                url:'master/mkaryawan/read',
				method: 'post',
                tools:'#tool_button_mkaryawan',
                rownumbers:true,
                pagination:true,
                fitColumns:true,
				multiSort:true,
                striped:true">
    <thead>
    <tr>
	    <th data-options="field:'nip',width:10,align:'center',sortable:true">NIP</th>
        <th data-options="field:'nama',width:20,align:'left',sortable:true">Nama</th>
        <th data-options="field:'no_telp',width:20,align:'left',sortable:true">Phone</th>
        <th data-options="field:'email',width:20,align:'left',sortable:true">Email</th>
        <th data-options="field:'line',width:10,align:'center',sortable:true,hidden:true">Kode Line</th>
        <th data-options="field:'dinas',width:10,align:'center',sortable:true,hidden:true">Kode Dinas</th>
        <th data-options="field:'nama_line',width:10,align:'center',sortable:true,hidden:false">Line</th>
        <th data-options="field:'nama_dinas',width:10,align:'center',sortable:true,hidden:false">Dinas</th>
        <!--th data-options="field:'jabatan',width:20,align:'left',sortable:true">Jabatan</th-->
        <!-----tile coloumn---------------------->
        <th data-options="field:'create_user',width:0,align:'left',sortable:true,hidden:true">Create user</th>
        <th data-options="field:'create_date',width:0,align:'left',sortable:true,hidden:true">Create date</th>
        <th data-options="field:'create_ip',width:0,align:'left',sortable:true,hidden:true">Create ip</th>
        <th data-options="field:'update_user',width:0,align:'left',sortable:true,hidden:true">Update user</th>
        <th data-options="field:'update_date',width:0,align:'left',sortable:true,hidden:true">Update date</th>
        <th data-options="field:'update_ip',width:0,align:'left',sortable:true,hidden:true">Update ip</th>
		<!-----tile coloumn---------------------->
    </tr>
    </thead>
</table>
<div id="tool_button_mkaryawan" style="padding:10px;">
&nbsp;<a href="javascript:void(0)" title="ADD" onclick="javascript:myCreate('#dialog_mkaryawan','#form_mkaryawan','master/mkaryawan/create','Add mkaryawan')"><i class="icon-plus icon-large"   style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
&nbsp;<a href="javascript:void(0)" title="UPDATE" onclick="javascript:myUpdate('#datagrid_mkaryawan','#dialog_mkaryawan','#form_mkaryawan','master/mkaryawan/update','Update mkaryawan')"><i class="icon-pencil icon-large" style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
&nbsp;<a href="javascript:void(0)" title="DELETE" onclick="javascript:myDelete('#datagrid_mkaryawan','master/mkaryawan/delete', 'master/mkaryawan/read', 'Hapus mkaryawan')"><i class="icon-remove icon-large" style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
&nbsp;<a href="javascript:void(0)" title="FILTER" onclick="javascript:filter('#datagrid_mkaryawan','master/mkaryawan/read')"><i class="icon-filter icon-large" style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
</div>