<!--
/**
 * Description of mkaryawan
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com> 
 * Copyright 2014    
 */
-->
<div id="dialog_mkaryawan" class="easyui-dialog"
     modal="true"
     iconCls="icon-save"
     closed="true"
     buttons="#dialog_button_mkaryawan"
     resizable="true"
     style="width:700px;">
    <div style="padding:10px">
        <form id="form_mkaryawan" class="easyui-form" method="post">
            <fieldset>
                <legend class="c1">Form Karyawan</legend>
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 50%;">
                            <p>
                                <label for="nip">NIK :</label>
                                <input class="easyui-textbox" type="text" name="nip" id="nip" required="true"
                                       size="50"/>
                            </p>

                            <p>
                                <label for="nama">Nama :</label>
                                <input class="easyui-textbox" type="text" name="nama" id="nama" required="true"
                                       size="50"/>
                            </p>

                            <p>
                                <label for="no_telp">Phone :</label>
                                <input class="easyui-textbox" type="text" name="no_telp" id="no_telp" required="true"
                                       size="50"/>
                            </p>

                            <p>
                                <label for="email">Email :</label>
                                <input class="easyui-textbox" type="text" name="email" id="email" required="true"
                                       size="50" data-options="required:true,validType:'email'"/>
                            </p>

                            <p>
                                <label for="dinas">Dinas :</label>
                                <input class="easyui-combobox" data-options="
                                panelWidth:300,
                                mode:'local',
                                fitColumns:true,
                                url: 'master/dinas/read_all',
                                valueField: 'code',
                                textField: 'name'" type="text" name="dinas"  required="true" size="50"/>
                            </p>
                            <p>
                                <label for="line">Line :</label>
                                <input class="easyui-combobox" data-options="
                                panelWidth:300,
                                mode:'local',
                                fitColumns:true,
                                url: 'master/line/read_all',
                                valueField: 'code',
                                textField: 'name'" type="text" name="line"  required="true" size="50"/>
                            </p>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
<div id="dialog_button_mkaryawan">
    <a href="javascript:void(0)" title="Save Data." class="easyui-linkbutton easyui-tooltip c1"
       onclick="javascript:mySave('#datagrid_mkaryawan','#dialog_mkaryawan','#form_mkaryawan','master/mkaryawan/read')"><i
            class="icon-save icon-medium"></i> Simpan</a>
    <a href="javascript:void(0)" title="Tutup Form." class="easyui-linkbutton easyui-tooltip c2"
       onclick="javascript:myClose('#dialog_mkaryawan')"><i class=" icon-remove"></i> Tutup</a>
</div>

<script type="text/javascript">


</script>