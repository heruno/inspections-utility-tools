<table id="datagrid_nfc" title="<i class=icon-save icon-large></i> Daftar nfc" class="easyui-datagrid"
       style="height:auto; width: 100%;"
       data-options="
                singleSelect : true,
                fit:true,
                url:'master/nfc/read',
				method: 'post',
                tools:'#tool_button_nfc',
                rownumbers:true,
                pagination:true,
                fitColumns:true,
				multiSort:true,
                striped:true">
    <thead>
    <tr>
        <th data-options="field:'nomor',width:10,align:'center',sortable:true,hidden:false">NOMOR</th>
        <th data-options="field:'kode',width:20,align:'center',sortable:true">KODE</th>
        <th data-options="field:'image',width:10,align:'center',sortable:true">FOTO LOKASI</th>
        <th data-options="field:'image64',width:10,align:'center',sortable:true,hidden:true">FOTO 64</th>
    </tr>
    </thead>
</table>
<div id="tool_button_nfc" style="padding:10px;">
    <a href="javascript:void(0)" title="UPDATE" onclick="javascript:openTagNfc()">
        <i class="icon-eye-open" style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i>
    </a>
</div>
<script type="text/javascript">
    function openTagNfc() {
        var row = $('#datagrid_nfc').datagrid('getSelected');
        if (row) {
            $('#dialog_nfc').dialog('open').dialog('setTitle', 'FORM NFC');
            ;
            $('#form_nfc').form('load', row);
            $('#a_nfc').attr('href','<?=base_url()?>assets/images/upload/'+row.image);
            $('#img_nfc').attr('src','<?=base_url()?>assets/images/upload/'+row.image);
        } else {
            $.messager.alert('warning', 'Silahkan pilih salah satu', 'warning');
        }

    }
    function uploadNfc() {
        $('#form_nfc').form('submit', {
            url: 'master/nfc/unggah',
            onSubmit: function() {
                return $(this).form('validate');
            },
            success: function(result) {
                var result = JSON.parse(result);
                if (result.successful) {
                    $('#a_nfc').attr('href','<?=base_url()?>assets/images/upload/'+result.data.image);
                    $('#img_nfc').attr('src','<?=base_url()?>assets/images/upload/'+result.data.image);
                    $.messager.show({
                        title: 'Berhasil',
                        msg: result.data.msg,
                        timeout: 5000,
                        showType: 'show'
                    });
                    $('#datagrid_nfc').datagrid({
                        url: 'master/nfc/read'
                    }, 'reload');
                } else {
                    $.messager.alert('Warning', result.data.msg, 'warning');
                }
            }
        });
    }
</script> 