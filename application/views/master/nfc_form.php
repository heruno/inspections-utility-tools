<div id="dialog_nfc" class="easyui-dialog"
     modal="true"
     iconCls="icon-save"
     closed="true"
     buttons="#dialog_button_nfc"
     resizable="true"
     style="width:600px;height: 450px">
    <div style="padding:5px">
        <form id="form_nfc"class="easyui-form" method="post" enctype="multipart/form-data">
            <fieldset>
                <legend class="c1">FORM TAG NFC</legend>
                <p>
                    <label for="nomor">NOMOR :</label>
                    <input class="easyui-textbox" type="text" name="nomor" id="nomor" required="true" size="50" readonly/>
                </p>
                <p>
                    <label for="kode">KODE :</label>
                    <input class="easyui-textbox" type="text" name="kode" id="kode" required="true" size="50" readonly/>
                </p>
                <p style="text-align: center">
                    <a id="a_nfc" size="50" data-lightbox="roadtrip" href="<?=base_url()?>assets/images/upload/no.jpg">
                        <img id="img_nfc" src="<?=base_url()?>assets/images/upload/no.jpg" style="height:220px; width:auto" />
                    </a>
                </p>
                <p>
                    <label for="image">
                        IMAGE :
                    </label>
                    <input class="easyui-filebox" name="attachment" id="attachment"  data-options="prompt:'Pilih File'" size="50"/>
                </p>
            </fieldset>
        </form>
    </div>
</div>
<div id="dialog_button_nfc">
<a href="javascript:void(0)" title="Save Data." class="easyui-linkbutton easyui-tooltip c1" onclick="javascript:uploadNfc()"><i class="icon-save icon-medium"></i> Save</a>
<a href="javascript:void(0)" title="Tutup Form." class="easyui-linkbutton easyui-tooltip c2" onclick="javascript:myClose('#dialog_nfc')"><i class=" icon-remove"></i> Close</a>
</div>