<table id="datagrid_notification" title="<i class=icon-save icon-large></i> Daftar Email" class="easyui-datagrid" style="height:auto; width: 100%;"
       data-options="
                singleSelect : true,
                fit:true,
                url:'master/notification/read',
				method: 'post',
                tools:'#tool_button_notification',
                rownumbers:true,
                pagination:true,
                fitColumns:true,
				multiSort:true,
                striped:true">
    <thead>
    <tr> 
	    <th data-options="field:'nik',width:20,align:'center',sortable:true">NIK</th>
        <th data-options="field:'notification',width:50,align:'left',sortable:true">EMAIL</th>
        <th data-options="field:'sms',width:10,align:'left',sortable:true">SMS</th>
        <th data-options="field:'unit',width:20,align:'center',sortable:true">UNIT</th>
              
        <!-----tile coloumn---------------------->
        <th data-options="field:'create_user',width:0,align:'left',sortable:true,hidden:true">Create user</th>
        <th data-options="field:'create_date',width:0,align:'left',sortable:true,hidden:true">Create date</th>
        <th data-options="field:'create_ip',width:0,align:'left',sortable:true,hidden:true">Create ip</th>
        <th data-options="field:'update_user',width:0,align:'left',sortable:true,hidden:true">Update user</th>
        <th data-options="field:'update_date',width:0,align:'left',sortable:true,hidden:true">Update date</th>
        <th data-options="field:'update_ip',width:0,align:'left',sortable:true,hidden:true">Update ip</th>
		<!-----tile coloumn---------------------->
    </tr>
    </thead>
</table>
<div id="tool_button_notification" style="padding:10px;">
&nbsp;<a href="javascript:void(0)" title="ADD" onclick="javascript:myCreate('#dialog_notification','#form_notification','master/notification/create','Add notification')"><i class="icon-plus icon-large"   style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
&nbsp;<a href="javascript:void(0)" title="UPDATE" onclick="javascript:myUpdate('#datagrid_notification','#dialog_notification','#form_notification','master/notification/update','Update notification')"><i class="icon-pencil icon-large" style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
&nbsp;<a href="javascript:void(0)" title="DELETE" onclick="javascript:myDelete('#datagrid_notification','master/notification/delete', 'master/notification/read', 'Hapus notification')"><i class="icon-remove icon-large" style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>
&nbsp;<a href="javascript:void(0)" title="FILTER" onclick="javascript:filter('#datagrid_notification','master/notification/read')"><i class="icon-filter icon-large" style="color: #3f51b5; display:block;background: transparent;margin:2px;text-decoration: none;"></i></a>

</div>