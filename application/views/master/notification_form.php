<div id="dialog_notification" class="easyui-dialog"
     modal="true"
     iconCls="icon-save"
     closed="true"
     buttons="#dialog_button_notification"
     resizable="true"
     style="width:800px;">
    <div style="padding:5px">
        <form id="form_notification" class="easyui-form" method="post">
            <fieldset>
                <legend class="c1">Form Email</legend>
                <p>
                    <label for="nik">NIK :</label>
                    <input class="easyui-textbox" type="text" name="nik" id="nik" required="true" size="50"/>
                </p>

                <p>
                    <label for="notification">EMAIL :</label>
                    <input class="easyui-textbox" type="text" name="notification" id="notification" required="true"
                           size="50"/>
                </p>

                <p>
                    <label for="notification">SMS :</label>
                    <input class="easyui-textbox" type="text" name="notification" id="notification" required="true"
                           size="50"/>
                </p>
                </td>
                <p>
                    <label for="idjabatan">UNIT :</label>
                    <input class="easyui-combobox" data-options="
                        panelWidth:300,
                        mode:'local',
                        fitColumns:true,
                        url: 'master/jabatan/read_all',
                        valueField: 'idjabatan',
                        textField: 'jabatan'" type="text" name="idjabatan" id="idjabatan" required="true" size="50"/>
                </p>
            </fieldset>
        </form>
    </div>
</div>
<div id="dialog_button_notification">
    <a href="javascript:void(0)" title="Save Data." class="easyui-linkbutton easyui-tooltip c1"
       onclick="javascript:mySave('#datagrid_notification','#dialog_notification','#form_notification','master/notification/read')"><i
            class="icon-save icon-medium"></i> Simpan</a>
    <a href="javascript:void(0)" title="Tutup Form." class="easyui-linkbutton easyui-tooltip c2"
       onclick="javascript:myClose('#dialog_notification')"><i class=" icon-remove"></i> Tutup</a>
</div>