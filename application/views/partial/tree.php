<!--div id="tab-sidebar" class="easyui-tabs" fit="true" plain="true" border="false">
<div title="<i class=icon-sitemap icon-medium></i>" style="padding:10px">
	</div>
</div-->
<div class="easyui-panel" style="border: 0px; padding: 5px;">
<!--input class="easyui-searchbox" data-options="prompt:'Please Input Value',searcher:treeSearch" style="width:100%;"/-->  
</div>
<div class="easyui-panel" data-options="fit:true" style="border: 0px; padding: 2px;">
<ul id="tree" class="easyui-tree" style="margin: 5px;">
		</ul>
</div>
<div id="mm" class="easyui-menu" style="width:120px" style="border-radius: 10px">
	<div onclick="reload()" data-options="iconCls:'icon-refresh icon-medium'">
		<span>
			Refresh
		</span>
	</div>
	<div class="menu-sep">
	</div>
	<div onclick=expandAll() data-options="iconCls:'icon-resize-full icon-medium'">
		<span>
			Expand
		</span>
	</div>
	<div onclick="collapseAll()" data-options="iconCls:'icon-resize-small icon-medium'">
		<span>
			Collapse
		</span>
	</div>
</div>
<script type="text/javascript">/*<![CDATA[*/
	$(document).ready(function() {
		$('#tt').tabs({
			onAdd : function(title,index){
				console.log('add:'+title+'('+index+')');
			},
			onLoad:function(panel){
				console.log(panel.panel('options').title+' is loaded')
			}
		});
		$("#tree").tree({
			data: <?= $tree ?> ,
			animate : true,
			lines : true,
			dnd: true,
			onClick: function(b) {
				/** console.log(b);*/
				var icon = b.iconCls;
				if (icon == ""){
				icon = "icon-file";
				}
                
				if (b.url!=null){
				if ($("#tt").tabs("exists", b.text)) {
					$("#tt").tabs("select", b.text)
				} else {
					 try {
    					$("#tt").tabs("add", {
    						type: "post",
    						title: b.text,
    						href: b.url,
    						iconCls: icon +" icon-large",
    						closable: true 
    					})
					} catch(e) {
						console.log('asem:'+e);
						$.messager.show({
							title:'Error',
							msg:e,
							timeout:5000,
							showType:'slide'
						});
					}
				}
				}
			},
			formatter: function(c) {
				var b = c.text;
				if (c.children != "") {
					b += "&nbsp;<span style='color:#3f51b5;font-weight:bold'>(" + c.children.length + ")</span>"
				}
				return b
			},
			onDrop: function(e, d, b) {
				var c = $("#tree").tree("getNode", e).id;
				$.ajax({
					url: "partial/partial/update",
					type: "post",
					dataType: "json",
					data: {
						id: d.id,
						targetId: c,
						point: b
					}
				})
			},
			onContextMenu: function(c, b) {
				c.preventDefault();
				$(this).tree("select", b.target);
				$("#mm").menu("show", {
					left: c.pageX,
					top: c.pageY
				})
			} 
		})
	});
	function treeSearch(value) {
		$('#tree').tree({
			url: 'partial/partial/menu/' + value
		});
	}
	function reload() {
		$('#tree').tree('reload');
	}
	function collapseAll() {
		var node = $('#tree').tree('getSelected');
		if (node) {
			$('#tree').tree('collapseAll', node.target);
		} else {
			$('#tree').tree('collapseAll');
		}
	}
	function expandAll() {
		var node = $('#tree').tree('getSelected');
		if (node) {
			$('#tree').tree('expandAll', node.target);
		} else {
			$('#tree').tree('expandAll');
		}
	}
 /*]]>*/</script>