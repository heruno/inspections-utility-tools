<style>
    body {
        font: normal 11px auto "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
        color: #4f6b72;
        padding: 5px;
    }

    a {
        color: #c75f3e;
    }

    .mytable {
        width: 100%;
        padding: 0;
        margin: 0;
    }

    caption {
        padding: 0 0 5px 0;
        width: 700px;
        font: italic 11px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
        text-align: right;
    }

    th {
        font: bold 11px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
        color: #4f6b72;
        border: 1px solid #C1DAD7;
        letter-spacing: 2px;
        text-transform: uppercase;
        text-align: left;
        padding: 6px;
        background: #CAE8EA url(images/bg_header.jpg) no-repeat;
    }

    th.nobg {
        border-top: 0;
        border-left: 0;
        border-right: 1px solid #C1DAD7;
        background: none;
    }

    td {
        border: 1px solid #C1DAD7;
        background: #fff;
        padding: 6px;
        color: #4f6b72;
    }

    td.alt {
        background: #F5FAFA;
        color: #797268;
    }

    th.spec {
        border-left: 1px solid #C1DAD7;
        border-top: 0;
        background: #fff url(images/bullet1.gif) no-repeat;
        font: bold 10px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
    }

    th.specalt {
        border-left: 1px solid #C1DAD7;
        border-top: 0;
        background: #f5fafa url(images/bullet2.gif) no-repeat;
        font: bold 10px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
        color: #797268;
    }
</style>
<?php
$noh = 1;
foreach ($row as $val): ?>
<table class="mytable" cellspacing="0" summary="Report Inspection (<?= $val['name'] ?>)">
    <caption>Report Inspection Equipment</caption>
    <tr>
        <th rowspan="2" scope="col" abbr="NO">NO</th>
        <th rowspan="2" scope="col" abbr="Parameter">EQUIPMENT</th>
        <th rowspan="2" scope="col" style="text-align: center;">DATE TIME</th>
        <th colspan="3" scope="col" style="text-align: center;">INSPECTOR</th>
        <th rowspan="2" scope="col" abbr="SHIFT" style="text-align: center;">SHIFT</th>
        <th rowspan="2" scope="col" abbr="CONDITION" style="text-align: center;">CONDITION</th>
    </tr>
    <tr>
        <th scope="col" abbr="Min" style="text-align: center;">NIP</th>
        <th scope="col" abbr="Maz" style="text-align: center;">NAMA</th>
        <th scope="col" abbr="Maz" style="text-align: center;">DINAS</th>
    </tr>
    <tr>
        <td style="text-align: center;"><?= $noh++ ?></td>
        <td scope="row" abbr="<?= $val['name'] ?>" class="spec"><?= $val['name'] ?></td>
        <td style="text-align: center;"><?= $val['localdt'] ?></td>
        <td style="text-align: center;"><?= $val['idinspector'] ?></td>
        <td style="text-align: center;"><?= $val['nama'] ?></td>
        <td style="text-align: center;"><?= $val['shift'] ?></td>
        <td style="text-align: center;"><?= $val['dinas'] ?></td>
        <td style="text-align: center;"><?= $val['conditions'] ?></td>
    </tr>
    <tr style="border: 1px solid;">
        <td colspan="8">
            <table class="mytable" cellspacing="0">
                <caption>Detail Report Inspection (<?= $val['name'] ?>)</caption>
                <tr>
                    <th rowspan="2" scope="col" abbr="NO">NO</th>
                    <th rowspan="2" scope="col" abbr="Parameter">Parameter</th>
                    <th colspan="2" scope="col" style="text-align: center;">Inspection</th>
                    <th colspan="2" scope="col" style="text-align: center;">Threshold</th>
                    <th rowspan="2" scope="col" abbr="Condition" style="text-align: center;">Condition</th>
                    <th rowspan="2" scope="col" abbr="Remarks" style="text-align: center;">Remarks</th>
                </tr>
                <tr>
                    <th scope="col" abbr="Value" style="text-align: center;">Value</th>
                    <th scope="col" abbr="Unit Of Measure" style="text-align: center;">Unit Of Measure</th>
                    <th scope="col" abbr="Min" style="text-align: center;">Min</th>
                    <th scope="col" abbr="Maz" style="text-align: center;">Max</th>
                </tr>
                <?php $no = 1; ?>
                <?php foreach ($this->report_model->read_detail_pdf($val['idequipment'], $val['localid']) as $valx): ?>
                    <tr>
                        <td style="text-align: center;"><?= $no++ ?></td>
                        <th scope="row" abbr="<?= $valx['label'] ?>" class="spec"><?= $valx['label'] ?></th>
                        <td style="text-align: center;"><?= $valx['value'] ?></td>
                        <td style="text-align: center;"><?= $valx['uom'] ?></td>
                        <td style="text-align: center;"><?= $valx['value_min'] ?></td>
                        <td style="text-align: center;"><?= $valx['value_max'] ?></td>
                        <td style="text-align: center;"><?= $valx['value_min'] ?></td>
                        <td style="text-align: center;"><?= $valx['value_max'] ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </td>
    </tr>
</table><br/>
<?php endforeach; ?>