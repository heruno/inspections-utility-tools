<div class="easyui-panel" data-options="title:'<small>Date</small>'" style="padding: 15px; text-align: center;overflow: hidden">
    <table style="width: 100%">
        <tr>
            <td style="text-align: left">FROM</td>
            <td>
                <input id="from_date" class="easyui-datebox" label="Select DateTime From / To:" labelPosition="top"  style="width:100%;" />
            </td>
        </tr>
        <tr>
            <td style="text-align: left">TO</td>
            <td>
                <input id="to_date" class="easyui-datebox" label="Select DateTime:" labelPosition="top"  style="width:100%;" />
            </td>
        </tr>
    </table>
</div>
<br />
<!--div class="easyui-panel" data-options="title:'<small>To</small>'" style="padding: 5px; text-align: center;overflow: hidden">

</div-->
<br />
<!--div class="easyui-panel" data-options="title:'<small>Print/View PDF</small>'" style="padding: 5px; text-align: center;overflow: hidden">
<a href="javascript:void(0)" onclick="javascript:generatePdf()" class="easyui-linkbutton c5">PDF</a>
</div-->
<br />
<div class="easyui-panel" data-options="title:'<small>Download Excel</small>'" style="padding: 5px; text-align: center;overflow: hidden">
<a href="javascript:void(0)" onclick="javascript:generateExcel()" class="easyui-linkbutton c1">Excel</a>
</div>
<script type="text/javascript">
    function formatTgl(dt) {
        var tanggal = new Date(dt);
        var y = tanggal.getFullYear();
        var m = tanggal.getMonth()+1;
        var d = tanggal.getDate();
        return y+'-'+m+'-'+d;
    }

 function generateExcel(){
    var from_date = formatTgl($('#from_date').datebox('getValue'));
    var to_date = formatTgl($('#to_date').datebox('getValue'));
    if(from_date && to_date){
    var report_url = '<?=base_url('report/generatexls')?>/'+from_date.replace(' ','/')+'/'+to_date.replace(' ','/');
    window.location = report_url;
    }else{
        $.messager.alert('warning', 'From Date & To Date Not Null', 'warning');
    }
}
function generatePdf(){
    var from_date = formatTgl($('#from_date').datebox('getValue'));
    var to_date = formatTgl($('#to_date').datebox('getValue'));
    if(from_date && to_date){
    var report_url = '<?=base_url('report/generatepdf')?>/'+from_date+'/'+to_date;
    $('#page_pdf').attr('src', report_url);
    }else{
      $.messager.alert('warning', 'From Date & To Date Not Null', 'warning');  
    }
}
</script>