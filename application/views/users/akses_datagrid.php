<table id="t_nodes" style="height:100%; width: 100%;">
</table>
<div id="tool_nodes" style="padding:10px;">
    <a href="javascript:void(0)" class="easyui-linkbutton c1 gta-button"
       onclick="javascript:$('#t_nodes').edatagrid('addRow',0)" style="width: 100px; margin-right: 5px;">ADD</a>
    <a href="javascript:void(0)" class="easyui-linkbutton c6 gta-button"
       onclick="javascript:$('#t_nodes').edatagrid('saveRow')" style="width: 100px; margin-right: 5px;">SAVE</a>
    <a href="javascript:void(0)" class="easyui-linkbutton c7 gta-button"
       onclick="javascript:$('#t_nodes').edatagrid('cancelRow')" style="width: 100px; margin-right: 5px;">CANCEL</a>
    <a href="javascript:void(0)" class="easyui-linkbutton c5 gta-button"
       onclick="javascript:$('#t_nodes').edatagrid('destroyRow')" style="width: 100px; margin-right: 5px;">DELETE</a>
    <input id="cc1" class="easyui-textbox" style="height: 30px; width: 250px;"/>
    <!--a href="javascript:void(0)" onclick="javascript:myGenerate('users/akses/create_file')"
           title="Generate File Akses.Json" class="easyui-linkbutton easyui-tooltip c2"><i class="icon-file"></i> Generate</a-->
    <a href="javascript:void(0)" title="Filter Data." class="easyui-splitbutton easyui-tooltip pull-right c2"
       data-options="menu:'#menu_t_nodes',plain:false">
        <i class="icon-filter"></i> Filter</a>

    <div id="menu_t_nodes" style="width:100px;">
        <div data-options="iconCls:'icon-filter'" onclick="javascript:filter('#t_nodes','users/akses/read/'+rowid)">
            <span>Filter</span></div>
        <div data-options="iconCls:'icon-remove'"
             onclick="javascript:cancelFilter('#t_nodes','users/akses/read/'+rowid)">
            <span>Cancel</span></div>
    </div>
</div>
<script type="text/javascript">
    var lastIndex;
    var rowid;
    $(function () {
        $('#cc1').combobox({
            valueField: 'roles_id',
            textField: 'name',
            url: '<?= base_url() ?>users/roles/read_all',
            required: true,
            onSelect: function (rec) {
                rowid = rec.roles_id;
                $('#t_nodes').edatagrid({
                    url: '<?= base_url() ?>users/akses/read/' + rowid,
                    saveUrl: '<?= base_url() ?>users/akses/create/' + rowid
                }, 'reload');
            }
        });
        $('#t_nodes').edatagrid({
            iconCls: 'icon-edit',
            toolbar: '#tool_nodes',
            remoteFilter: true,
            rownumbers: true,
            pagination: true,
            fit: true,
            fitColumns: true,
            multiSort: true,
            striped: true,
            singleSelect: true,
            idField: 'ID_NODES',
            /**url:'akses/read'+rowid,*/
            updateUrl: '<?= base_url() ?>users/akses/update',
            destroyUrl: '<?= base_url() ?>users/akses/delete',
            columns: [[
                {
                    field: 'menu_id', title: 'MENU ID', align: 'center', sortable: true, width: 100,
                    editor: {
                        type: 'combobox',
                        options: {
                            valueField: 'menuId',
                            textField: 'text',
                            url: '<?= base_url() ?>users/nodes/read_all',
                            required: true
                        }
                    }
                },
                {field: 'roles_id', title: 'ID ROLES', align: 'left', sortable: true, width: 100, hidden: true},
                {field: 'text', title: 'Menu', align: 'left', sortable: true, width: 100},
                {field: 'url', title: 'URL', align: 'left', sortable: true, width: 100},
                {
                    field: 'read', title: 'Read', width: 30, align: 'center',
                    formatter: function (a) {
                        if (a == 1) return '<i class=icon-ok style=font-size:12px;color:lime></i>';
                        if (a == 0) return '<i class=icon-remove style=font-size:12px;color:red></i>';
                    },
                    editor: {
                        type: 'checkbox',
                        options: {
                            on: 1,
                            off: 0
                        }
                    }
                },
                {
                    field: 'add', title: 'Add', width: 30, align: 'center',
                    formatter: function (a) {
                        if (a == 1) return '<i class=icon-ok style=font-size:12px;color:lime></i>';
                        if (a == 0) return '<i class=icon-remove style=font-size:12px;color:red></i>';
                    },
                    editor: {
                        type: 'checkbox',
                        options: {
                            on: 1,
                            off: 0
                        }
                    }
                },
                {
                    field: 'edit', title: 'Edit', width: 30, align: 'center',
                    formatter: function (a) {
                        if (a == 1) return '<i class=icon-ok style=font-size:12px;color:lime></i>';
                        if (a == 0) return '<i class=icon-remove style=font-size:12px;color:red></i>';
                    },
                    editor: {
                        type: 'checkbox',
                        options: {
                            on: 1,
                            off: 0
                        }
                    }
                },
                {
                    field: 'delete', title: 'Delete', width: 30, align: 'center',
                    formatter: function (a) {
                        if (a == 1) return '<i class=icon-ok style=font-size:12px;color:lime></i>';
                        if (a == 0) return '<i class=icon-remove style=font-size:12px;color:red></i>';
                    },
                    editor: {
                        type: 'checkbox',
                        options: {
                            on: 1,
                            off: 0
                        }
                    }
                }
            ]],
            destroyMsg: {
                norecord: {
                    title: 'Warning',
                    msg: 'Tidak ada data yang dipilih.'
                },
                confirm: {
                    title: 'Confirm',
                    msg: 'Apakah Anda yakin ingin menghapus ?'
                }
            },
            onError: function (index, row) {
                if (row.isError) {
                    return;
                    /**$.messager.alert('Error', row.msg, 'error');*/
                } else {
                    $.messager.show({
                        title: 'Berhasil',
                        msg: row.msg,
                        timeout: 5000,
                        showType: 'show'
                    });
                }
            },
            onBeginEdit: function (rowIndex) {
                var editors = $('#t_nodes').datagrid('getEditors', rowIndex);
                /**   var val = $('#cc1').combobox('getValue');
                 $(editors[1].target).textbox('setValue',val);*/
            }
        });
    });
</script>