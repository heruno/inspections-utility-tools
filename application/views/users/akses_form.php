<!--
/**
 * Description of aaksesusermenu
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com> 
 * Copyright 2014    
 */
-->
<div id="dialog_aaksesusermenu" class="easyui-dialog"
     modal="true"
     iconCls="icon-save"
     closed="true"
     buttons="#dialog_button_aaksesusermenu"
     resizable="true"
     style="width:600px;">
    <div style="padding:10px">
        <form id="form_aaksesusermenu" class="easyui-form" method="post">
            <fieldset>
                <legend class="c1">Form Aksesusermenu</legend>
                <p>
                    <label for="id_nodes">Id form :</label>
                    <input class="easyui-textbox" type="text" name="id_nodes" id="id_nodes" required="true" size="50"/>
                </p>
                <p>
                    <label for="id_user">Id user :</label>
                    <input class="easyui-textbox" type="text" name="id_user" id="id_user" required="true" size="50"/>
                </p>
                <p>
                    <label for="read">Read :</label>
                    <input class="easyui-textbox" type="text" name="read" id="read" required="true" size="50"/>
                </p>
                <p>
                    <label for="add">Add :</label>
                    <input class="easyui-textbox" type="text" name="add" id="add" required="true" size="50"/>
                </p>
                <p>
                    <label for="edit">Edit :</label>
                    <input class="easyui-textbox" type="text" name="edit" id="edit" required="true" size="50"/>
                </p>
                <p>
                    <label for="delete">Delete :</label>
                    <input class="easyui-textbox" type="text" name="delete" id="delete" required="true" size="50"/>
                </p>
            </fieldset>
        </form>
    </div>
</div>
<div id="dialog_button_aaksesusermenu">
<a href="javascript:void(0)" title="Save Data." class="easyui-linkbutton easyui-tooltip" onclick="javascript:mySave('#datagrid_aaksesusermenu','#dialog_aaksesusermenu','#form_aaksesusermenu','attribute/aaksesusermenu/read')"><i class="icon-save icon-medium"></i> Simpan</a>
<a href="javascript:void(0)" title="Tutup Form." class="easyui-linkbutton easyui-tooltip" onclick="javascript:myClose('#dialog_aaksesusermenu')"><i class=" icon-remove"></i> Tutup</a>
</div>