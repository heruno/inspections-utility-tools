<fieldset class="c2" style="margin: 5%; padding: 10%;">
	<form id="form_edit_password" action="attribute/auser/action_edit_password" class="easyui-form" method="post">
		<input type="hidden" name="id_user" value="<?=$this->session->userdata('id_user')?>"/>
		<p>
			<label for="oldpassword">
				Old-Password :
			</label>
			<input class="easyui-textbox easyui-validatebox" type="password" name="oldpassword" id="oldpassword" required="true" style="width:70%;"/>
		</p>
		<p>
			<label for="password">
				New-Password :
			</label>
			<input class="easyui-textbox easyui-validatebox" type="password" name="password" id="password" required="true" style="width:70%;"/>
		</p>
		<p>
			<label for="repassword">
				Re New-Password :
			</label>
			<input class="easyui-textbox easyui-validatebox" type="password" name="repassword" id="repassword" required="true" validType="equals['#password']" style="width:70%;"/>
		</p>
		<center>
			<span style="color: red;" id="msg_pass">
			</span>
			<input type="submit" value="UBAH PASSWORD" style="height: ;"/>
		</center>
	</form>
</fieldset>
<script type="text/javascript">
	
$(function(){
            $('#form_edit_password').form({
                success:function(data){
                    $.messager.alert('Info', data, 'info');
                }
            });
        });

</script>