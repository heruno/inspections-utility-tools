<table id="datagrid_map_agroup" title="<i class=icon-save icon-large></i> Daftar Group" class="easyui-datagrid" style="height:350px; width: 99%;"
       data-options="singleSelect : false,
				method: 'post',
                fitColumns:true,
				multiSort:true,
                striped:true">
    <thead>
    <tr>
        <th data-options="field:'ck',checkbox:true"></th>
        <th data-options="field:'id_user',width:30,align:'center',sortable:true,hidden:true">ID/NIP</th>
	    <th data-options="field:'id_group',width:50,align:'center',sortable:true">Id group</th>
        <th data-options="field:'nama',width:100,align:'left',sortable:true">Nama</th>
    </tr>
    </thead>
</table>