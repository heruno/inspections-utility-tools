<table id="datagrid_nodes" class="easyui-datagrid" style="width: 98%;"
       data-options="
                singleSelect : true,
                url:'<?=base_url()?>users/nodes/read',
				method: 'post',
                toolbar:'#tool_button_nodes',
                rownumbers:true,
                pagination:true,
                fitColumns:true,
                fit:true,
				multiSort:true,
                striped:true,
                showFooter: true">
    <thead>
    <tr>
	    <th data-options="field:'menuId',width:100,align:'center',sortable:true,hidden:true">Menu ID</th>
        <th data-options="field:'parentId',width:100,align:'center',sortable:true,hidden:true">Parent ID</th>
        <th data-options="field:'text',width:200,align:'left',sortable:true">Text</th>
        <th data-options="field:'iconCls',width:100,align:'center',sortable:true,hidden:false">IconCls</th>
        <th data-options="field:'url',width:150,align:'left',sortable:true">Url</th>
        <th data-options="field:'state',width:50,align:'center',sortable:true">State</th>
        <th data-options="field:'active',width:50,align:'center',sortable:true">Active</th>
    </tr>
    </thead>
</table>
<div id="tool_button_nodes" style="padding:10px;">
    <a href="javascript:void(0)" onclick="javascript:myCreate('#dialog_nodes','#form_nodes','<?=base_url()?>users/nodes/create','Add Menu')"
       title="Entry Data." class="easyui-linkbutton easyui-tooltip c1 gta-button"><i class="icon-file"></i> Add</a>
    <a href="javascript:void(0)" onclick="javascript:myUpdate('#datagrid_nodes','#dialog_nodes','#form_nodes','<?=base_url()?>users/nodes/update','Update Menu')"
       title="Edit Data." class="easyui-linkbutton easyui-tooltip c8 gta-button"><i class="icon-edit"></i> Edit</a>
    <a href="javascript:void(0)" onclick="javascript:myDelete('#datagrid_nodes','<?=base_url()?>users/nodes/delete', '<?=base_url()?>users/nodes/read_all', 'Delete menu')"
       title="Delete Data." class="easyui-linkbutton easyui-tooltip c5 gta-button"><i class="icon-remove"></i> Delete</a>
    <!--a href="javascript:void(0)" onclick="javascript:myGenerate('nodes/create_file')"
       title="Generate File Tree.Json" class="easyui-linkbutton easyui-tooltip c2"><i class="icon-file"></i> Generate</a-->
  <a href="javascript:void(0)" title="Filter Data." class="easyui-splitbutton easyui-tooltip c2" data-options="menu:'#menu_filter_nodes',plain:false">
   <i class="icon-filter"></i> Filter</a>
   <div id="menu_filter_nodes" style="width:100px;">
        <div data-options="iconCls:'icon-filter'" onclick="javascript:filter('#datagrid_nodes','users/nodes/read')">
        <span>Filter</span>
        </div>
        <div data-options="iconCls:'icon-remove'" onclick="javascript:cancelFilter('#datagrid_nodes','users/nodes/read')">
        <span>Cancel</span>
        </div>
    </div>
</div>
<script type="text/javascript">
$(function(){mydgrid.onCmenu('#datagrid_nodes');});
</script>