<div id="dialog_nodes" class="easyui-dialog"
     modal="true"
     iconCls="icon-save"
     closed="true"
     buttons="#dialog_button_nodes"
     resizable="true"
     style="width:600px;">
    <div style="padding:10px">
        <form id="form_nodes" class="easyui-form" method="post">
            <fieldset>
                <legend class="c1">Form Odes</legend>
                    <input type="hidden" name="menuId" id="menuId" required="false"/>
                    <p>
                    <label for="text">Text :</label>
                    <input class="easyui-textbox" type="text" name="text" id="text" required="true" size="50"/>
                </p>
                <p>
                    <label for="parentId">Parent ID :</label>
                    <input class="easyui-combobox" size="50" required="true" name="parentId"
                    data-options="
                        prompt:'Parent',
                        url:'<?=base_url()?>users/nodes/read_all',
                        method:'get',
                        valueField:'menuId',
                        textField:'text',
                        panelHeight:'auto'"/>
                </p>
                <p>
                    <label for="iconCls">IconCls :</label>
                    <input class="easyui-textbox" type="text" name="iconCls" id="iconCls" size="50"/>
                </p>
                <p>
                    <label for="url">Url :</label>
                    <input class="easyui-textbox" type="text"  name="url" id="url" size="50"/>
                </p>
                <p>
                    <label for="state">State :</label>
                    <select class="easyui-combobox" name="state" style="width: 100px;">
                     <option value="">Pilih State</option>
                    <option value="open">Open</option>
                    <option value="closed">Closed</option>
                    </select>
                </p>
                <p>
                    <label for="active">Active :</label>
                    <select class="easyui-combobox" name="active" style="width: 100px;">
                    <option value="1">Enable</option>
                    <option value="0">Disable</option>
                    </select>
                </p>
                <!--p>
                    <label for="group">Group User :</label>
                    <input class="easyui-combobox" size="50" name="group"
                    data-options="
                        prompt:'Group',
                        url:'attribute/agroup/read_all',
                        method:'get',
                        valueField:'id_group',
                        textField:'nama',
                        panelHeight:'auto'"/>
                </p-->
            </fieldset>
        </form>
    </div>
</div>
<div id="dialog_button_nodes">
<a href="javascript:void(0)" title="Save Data." class="easyui-linkbutton easyui-tooltip" onclick="javascript:mySave('#datagrid_nodes','#dialog_nodes','#form_nodes','<?=base_url()?>users/nodes/read')"><i class="icon-save icon-medium"></i> Save</a>
<a href="javascript:void(0)" title="Close Form." class="easyui-linkbutton easyui-tooltip" onclick="javascript:myClose('#dialog_nodes')"><i class=" icon-remove"></i> Close</a>
</div>