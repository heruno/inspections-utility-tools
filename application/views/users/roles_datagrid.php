<table id="datagrid_roles" title="Daftra Roles" class="easyui-datagrid" style="width: 98%;"
       data-options="
                singleSelect : true,
                url:'<?=base_url()?>users/roles/read',
				method: 'post',
                toolbar:'#tool_button_roles',
                rownumbers:true,
                pagination:true,
                fitColumns:true,
                fit:true,
				multiSort:true,
                striped:true,
                showFooter: true">
    <thead>
    <tr>
	    <th data-options="field:'roles_id',width:50,align:'center',sortable:true,hidden:false">ID</th>
        <th data-options="field:'name',width:200,align:'left',sortable:true,hidden:false">Name</th>
    </tr>
    </thead>
</table>
<div id="tool_button_roles" style="padding:10px;" class="c7">
    <a href="javascript:void(0)" onclick="javascript:myCreate('#dialog_roles','#form_roles','<?=base_url()?>users/roles/create','Tambah Data Nodes')"
       title="Entry Data." class="easyui-linkbutton easyui-tooltip c1 gta-button"><i class="icon-file"></i> Add</a>
    <a href="javascript:void(0)" onclick="javascript:myUpdate('#datagrid_roles','#dialog_roles','#form_roles','<?=base_url()?>users/roles/update','Update Data Nodes')"
       title="Edit Data." class="easyui-linkbutton easyui-tooltip c8 gta-button"><i class="icon-edit"></i> Edit</a>
    <a href="javascript:void(0)" onclick="javascript:myDelete('#datagrid_roles','<?=base_url()?>users/roles/delete', '<?=base_url()?>users/roles/read', 'Hapus Data Nodes')"
       title="Delete Data." class="easyui-linkbutton easyui-tooltip c5 gta-button"><i class="icon-remove"></i> Delete</a>
</div>
<script type="text/javascript">
$(function(){mydgrid.onCmenu('#datagrid_roles');});
</script>