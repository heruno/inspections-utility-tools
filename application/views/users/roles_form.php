<!--
/**
 * Description of roles
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com> 
 * Copyright 2014    
 */
-->
<div id="dialog_roles" class="easyui-dialog"
     modal="true"
     iconCls="icon-save"
     closed="true"
     buttons="#dialog_button_roles"
     resizable="true"
     style="width:600px;">
    <div style="padding:10px">
        <form id="form_roles" class="easyui-form" method="post">
            <fieldset>
                <legend class="c1">FORM MASA DATA</legend>
                <p>
                    <label for="roles_id">ROLES ID :</label>
                    <input class="easyui-textbox" type="text" name="roles_id" id="roles_id" required="true" size="50"/>
                </p>
                <p>
                    <label for="name">NAMA :</label>
                    <input class="easyui-textbox" type="text" name="name" id="name" required="true" size="50"/>
                </p>
            </fieldset>
        </form>
    </div>
</div>
<div id="dialog_button_roles">
<a href="javascript:void(0)" 
    title="Save Data." 
    class="easyui-linkbutton easyui-tooltip c1" 
    onclick="javascript:mySave('#datagrid_roles','#dialog_roles','#form_roles','users/roles/read')">
    <i class="icon-save icon-medium"></i> SIMPAN</a>
<a href="javascript:void(0)" 
    title="Tutup Form." 
    class="easyui-linkbutton easyui-tooltip c2" 
    onclick="javascript:myClose('#dialog_roles')">
    <i class=" icon-remove"></i> TUTUP</a>
</div>