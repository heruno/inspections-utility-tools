<!--
/**
 * Description of auser
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com> 
 * Copyright 2014    
 */
-->
<table id="datagrid_auser" title="<i class=icon-save icon-large></i> Daftar User" class="easyui-datagrid" style="height:auto; width: 100%;"
       data-options="
                singleSelect : true,
                fit:true,
                url: 'users/users/read',
				method: 'post',
                toolbar:'#tool_button_auser',
                rownumbers:true,
                pagination:true,
                fitColumns:true,
				multiSort:true,
                striped:true">
    <thead>
    <tr>
	    <th data-options="field:'nip',width:150,align:'center',sortable:true">NIP</th>
        <th data-options="field:'nama',width:200,align:'left',sortable:true">Nama</th>
	    <th data-options="field:'users_id',width:150,align:'center',sortable:true">Id user</th>
        <th data-options="field:'username',width:100,align:'left',sortable:true">Username</th>
        <th data-options="field:'password',width:100,align:'left',sortable:true">Password</th>
        <th data-options="field:'active',width:50,align:'center',sortable:true">Status</th>
    </tr>
    </thead>
</table>
<div id="tool_button_auser" style="padding:10px;" class="c7">
    <a href="javascript:void(0)" onclick="javascript:myCreate('#dialog_auser','#form_auser','users/users/create','Tambah Data User')"
       title="Tambah Data." class="easyui-linkbutton easyui-tooltip c1"><i class="icon-file"></i> Tambah</a>
    <a href="javascript:void(0)" onclick="javascript:myUpdate('#datagrid_auser','#dialog_auser','#form_auser','users/users/update','Update Data User')"
       title="Ubah Data." class="easyui-linkbutton easyui-tooltip c8"><i class="icon-edit"></i> Ubah</a>
    <a href="javascript:void(0)" onclick="javascript:myDelete('#datagrid_auser','users/users/delete', 'users/users/read', 'Hapus Data User')"
       title="Hapus Data." class="easyui-linkbutton easyui-tooltip c5"><i class="icon-remove"></i> Hapus</a>
     
     <!--a href="javascript:void(0)" onclick="javascript:openMapGroup()"
       title="Tambah Data Group." class="easyui-linkbutton easyui-tooltip c1"><i class="icon-file"></i> Group User</a-->
    
    <a href="javascript:void(0)" title="Filter Data." class="easyui-splitbutton easyui-tooltip pull-right c2" data-options="menu:'#menu_filter_auser',plain:false">
    <i class="icon-filter"></i> Filter</a>
    <div id="menu_filter_auser" style="width:100px;">
        <div data-options="iconCls:'icon-filter'" onclick="javascript:filter('#datagrid_auser','users/users/read')">
        <span>Filter</span>
        </div>
        <div data-options="iconCls:'icon-remove'" onclick="javascript:cancelFilter('#datagrid_auser','users/users/read')">
        <span>Cancel</span>
        </div>
    </div>
</div>
<script type="text/javascript">
function openMapGroup()
{
    var row = $('#datagrid_auser').datagrid('getSelected');
	if (row) {
		$('#dialog_auser_group').dialog('open').dialog('setTitle', 'Mapping Group User');
		$('#form_auser_group').form('load',{
		  id_user:row.id_user  ,
          nama:row.nama
		});
        $('#datagrid_map_agroup').datagrid({url:'attribute/auser_grp/read_all/'+row.id_user},'reload');
        $('#datagrid_agroup').datagrid({url:'attribute/agroup/read_all/'+row.id_user},'reload');
	} else {
		$.messager.alert('Warning', 'Silahkan pilih Data ', 'warning');
	}
}
function addMapGroup()
{
    var data = [];
    var v_id  = $('#id_userx').textbox('getValue');
    var rows2 = $('#datagrid_agroup').datagrid('getSelections');
      var x;
 	  for (x = 0; x < rows2.length; x++) {
 	       var field = {};
           field['id_user']  = v_id;
           field['id_group'] = rows2[x].id_group;
           data.push(field);
      }
    if (rows2.length == data.length) {
        $.ajax({
                url: 'attribute/auser_grp/create_batch',
				type: "POST",
				data: JSON.stringify(data),
                processData: false,
                contentType: "application/json; charset=UTF-8",
				success: function(data) {
				var result = JSON.parse(data);
                if(result.msg){
                $('#datagrid_map_agroup').datagrid({url:'attribute/auser_grp/read_all/'+v_id},'reload');
                $('#datagrid_agroup').datagrid({url:'attribute/agroup/read_all/'+v_id},'reload');
				}
                }
			});
	} 
}
function delMapGroup()
{
    var datax = [];
     var v_id = $('#id_userx').textbox('getValue');
     var rows1 = $('#datagrid_map_agroup').datagrid('getSelections');
    var x;
 	  for (x = 0; x < rows1.length; x++) {
 	       var fieldx = {};
           fieldx['id_user']     = v_id;
           fieldx['id_group']    = rows1[x].id_group;
           datax.push(fieldx);
      }
	if (rows1.length == datax.length) {
	   $.ajax({
                url: 'attribute/auser_grp/delete_batch',
				type: "POST",
				data: JSON.stringify(datax),
                processData: false,
                contentType: "application/json; charset=UTF-8",
				success: function(data) {
	            var result = JSON.parse(data);
                if(result.msg){
				$('#datagrid_map_agroup').datagrid({url:'attribute/auser_grp/read_all/'+v_id},'reload');
                $('#datagrid_agroup').datagrid({url:'attribute/agroup/read_all/'+v_id},'reload');
                }
				}
			});
	}
}
</script>