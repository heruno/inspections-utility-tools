<!--
/**
 * Description of auser
 * @created on : 2014-09-26 05:01:55
 * @author Heruno Utomo <wolu.88@gmail.com> 
 * Copyright 2014    
 */
-->
<div id="dialog_auser" class="easyui-dialog"
     modal="true"
     iconCls="icon-save"
     closed="true"
     buttons="#dialog_button_auser"
     resizable="true"
     style="width:600px;">
    <div style="padding:10px">
        <form id="form_auser" class="easyui-form" method="post">
            <fieldset>
                <legend class="c1">Form User</legend>
                <input type="hidden" name="users_id" id="users_id"/>
                <p>
                    <label for="nip">NIP :</label>
                    <input class="easyui-textbox" type="text" name="nip" id="nip" required="true" size="50"/>
                </p>
                <p>
                    <label for="username">Username :</label>
                    <input class="easyui-textbox" type="text" name="username" id="username" required="true" size="50"/>
                </p>
                <p>
                    <label for="password">Password :</label>
                    <input class="easyui-textbox easyui-validatebox" type="password" name="password" id="password" required="true" size="50"/>
                </p>
                <p>
                    <label for="repassword">Re-Password :</label>
                    <input class="easyui-textbox easyui-validatebox" type="password" name="repassword" id="repassword" required="true" validType="equals['#password']" size="50"/>
                </p>
                <!--p>
                    <label for="group">Group User :</label>
                    <input class="easyui-combobox" size="50" required="true" name="id_group"
                    data-options="
                        prompt:'Group',
                        url:'attribute/agroup/read_all',
                        method:'get',
                        valueField:'id_group',
                        textField:'nama',
                        panelHeight:'auto'"/>
                </p-->
            </fieldset>
        </form>
    </div>
</div>
<div id="dialog_button_auser">
<span style="color: red;" id="msg_pass"></span>
<a href="javascript:void(0)" title="Save Data." class="easyui-linkbutton easyui-tooltip" onclick="javascript:mySave('#datagrid_auser','#dialog_auser','#form_auser','users/users/read')"><i class="icon-save icon-medium"></i> Simpan</a>
<a href="javascript:void(0)" title="Tutup Form." class="easyui-linkbutton easyui-tooltip" onclick="javascript:myClose('#dialog_auser')"><i class=" icon-remove"></i> Tutup</a>
</div>
<script type="text/javascript">
$('#nip').combogrid({
    panelWidth:500,
    mode:'local',
    fitColumns:true,
    url: 'master/mkaryawan/read_all',
    idField: 'nip',
    textField: 'nama',
    columns: [[
        {field:'nip',title:'NIP',width:100,sortable:true},
        {field:'nama',title:'Nama',width:200,sortable:true},
    ]],
    onSelect:function(id, row){
    $('#users_id').val(row.nip);
    }
});
</script>