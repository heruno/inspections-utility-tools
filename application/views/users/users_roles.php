<div style="padding:10px">
    <fieldset class="c7">
        ROLES :<br/>
        <input class="easyui-combobox pull-left" style="width: 30%"
               id="roles_id"
               name="roles_id"
               data-options="
                    url:'<?= base_url() ?>users/roles/read_all',
                    method:'get',
                    valueField:'roles_id',
                    textField:'name',
                    panelHeight:'auto',
                    onSelect:openMapUsers"/>
    </fieldset>

    <table style="width: 100%;">
        <tr>

            <td style="width: 47%;">
                <?php include('users_roles_datagrid.php'); ?>
            </td>
            <td style="width:6%;">
                <a href="javascript:void(0)" onclick="javascript:addMapUsers()" class="easyui-linkbutton c1"
                   style="width: 45px;"><i class="icon-circle-arrow-right large"></i></a>
                <br/>&nbsp;<br/>
                <a href="javascript:void(0)" onclick="javascript:delMapUsers()" class="easyui-linkbutton c5"
                   style="width: 45px;"><i class="icon-circle-arrow-left large"></i></a>
            </td>
            <td style="width: 47%;">
                <?php include('users_roles_map_datagrid.php'); ?>
            </td>

        </tr>
    </table>
</div>
<script type="text/javascript">
    var ur_datagrid_users = $('#datagrid_users');
    var ur_datagrid_map_users = $('#datagrid_map_users');

    function reload_map_roles_users(val) {
        ur_datagrid_users.datagrid({
            remoteFilter: true, url: '<?=base_url()?>users/users/read_all/' + val
        }, 'reload');
        ur_datagrid_map_users.datagrid({
            remoteFilter: true,
            url: '<?=base_url()?>users/users_roles/read_all/' + val
        }, 'reload');
        ur_datagrid_users.datagrid('enableFilter');
        ur_datagrid_map_users.datagrid('enableFilter');
    }

    function openMapUsers() {
        var val = $('#roles_id').combobox('getValue');
        if (val) {
            reload_map_roles_users(val);
        } else {
            $.messager.alert('Warning', 'Silahkan pilih Data ', 'warning');
            $('#roles_id').combobox('clear');
        }
    }

    function addMapUsers() {
        var data = [];
        var v_id = $('#roles_id').combobox('getValue');
        var rows2 = ur_datagrid_users.datagrid('getSelections');

        var x;
        for (x = 0; x < rows2.length; x++) {
            var field = {};
            field['roles_id'] = v_id;
            field['users_id'] = rows2[x].users_id;
            data.push(field);
        }
        if (rows2.length == data.length) {
            $.ajax({
                url: '<?=base_url()?>users/users_roles/create_batch',
                type: "POST",
                data: JSON.stringify(data),
                processData: false,
                contentType: "application/json; charset=UTF-8",
                success: function (data) {
                    var result = JSON.parse(data);
                    if (result.msg) {
                        reload_map_roles_users(v_id);
                    }
                }
            });
        }
    }

    function delMapUsers() {
        var datax = [];
        var v_id = $('#roles_id').combobox('getValue');
        var rows1 = ur_datagrid_map_users.datagrid('getSelections');
        var x;

        for (x = 0; x < rows1.length; x++) {
            var fieldx = {};
            fieldx['roles_id'] = v_id;
            fieldx['users_id'] = rows1[x].users_id;
            datax.push(fieldx);
        }
        if (rows1.length == datax.length) {
            $.ajax({
                url: '<?=base_url('users/users_roles/delete_batch')?>',
                type: "POST",
                data: JSON.stringify(datax),
                processData: false,
                contentType: "application/json; charset=UTF-8",
                success: function (data) {
                    var result = JSON.parse(data);
                    if (result.msg) {
                        reload_map_roles_users(v_id);
                    }
                }
            });
        }
    }
</script>