<table id="datagrid_users" title="<i class=icon-save icon-large></i> List Users" class="easyui-datagrid"
       style="height:400px; width: 99%;"
       data-options="
                singleSelect : false,
				method: 'post',
                fitColumns:true,
				multiSort:true,
                striped:true">
    <thead>
    <tr>
        <th data-options="field:'ck',checkbox:true"></th>
        <th data-options="field:'users_id',width:50,align:'center',sortable:true">ID</th>
        <th data-options="field:'nama',width:100,align:'left',sortable:true">Nama</th>
    </tr>
    </thead>
</table>