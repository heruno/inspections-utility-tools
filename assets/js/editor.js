Survey.defaultBootstrapCss.navigationButton = "btn btn-primary";

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    url = url.toLowerCase(); // This is just to avoid case sensitiveness
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
/*questionTypes
1. checkbox: "Checkbox",
2. comment: "Comment",
3. dropdown: "Dropdown",
4. file: "File",
5. html: "Html",
6. matrix: "Matrix (single choice)",
7. matrixdropdown: "Matrix (multiple choice)",
8. matrixdynamic: "Matrix (dynamic rows)",
9. multipletext: "Multiple Text",
10. panel: "Panel",
11. radiogroup: "Radiogroup",
12. rating: "Rating",
13. text: "Single Input"*/
//questionTypes: ["checkbox", "radiogroup", "dropdown","rating"],
var editorOptions = {
    showEmbededSurveyTab: true,
    showOptions: true,
    showState: true,
    questionTypes: ["checkbox", "radiogroup", "dropdown","rating"],
    generateValidJSON: false
};
var editor = new SurveyEditor.SurveyEditor("editor", editorOptions);
editor.saveSurveyFunc = function () {
    var nsurvei = $('#nama-survei').val();
    if (nsurvei == '' || nsurvei == undefined) {
        alert('masukan judul survei');
    } else {
        sessionStorage.setItem('judul-survei', nsurvei);
        sessionStorage.setItem('template-survei', editor.text);
    }
};