/**
@puskesmas
*/
$('.id_puskesmas').combogrid({
    panelWidth:500,
    mode:'local',
    fitColumns:true,
    url: 'master/mpuskesmas/read_all',
    idField: 'id_puskesmas',
    textField: 'nama',
    columns: [[
        {field:'id_puskesmas',title:'ID',width:100,sortable:true},
        {field:'nama',title:'Nama',width:400,sortable:true},
        {field:'alamat',title:'Alamat',width:400,sortable:true},
        {field:'pimpinan',title:'Pimpinan',width:300,sortable:true},
        {field:'id_kecamatan',title:'ID Kecamatan',width:100,sortable:true}
    ]]
});
/**
@gender
*/
$('.id_gender').combogrid({
    panelWidth:300,
    promt:'Gender',
    mode:'local',
    fitColumns:true,
    url: 'attribute/agender/read_all',
    idField: 'id_gender',
    textField: 'nama',
    columns: [[
        {field:'id_gender',title:'ID',width:200,sortable:true,hidden:true},
        {field:'nama',title:'Gender',width:300,sortable:true}
    ]]
});
$('.id_status_kwn').combogrid({
    panelWidth:300,
    mode:'local',
    fitColumns:true,
    url: 'attribute/astatus_kwn/read_all',
    idField: 'id_status_kwn',
    textField: 'nama',
    columns: [[
        {field:'id_status_kwn',title:'ID',width:200,sortable:true,hidden:true},
        {field:'nama',title:'Status',width:300,sortable:true}
    ]]
});
$('.id_goldarah').combogrid({
    panelWidth:300,
    mode:'local',
    fitColumns:true,
    url: 'attribute/agoldarah/read_all',
    idField: 'id_goldarah',
    textField: 'nama',
    columns: [[
        {field:'id_goldarah',title:'ID',width:200,sortable:true,hidden:true},
        {field:'nama',title:'GolDarah',width:300,sortable:true}
    ]]
});
$('.id_agama').combogrid({
    panelWidth:300,
    mode:'local',
    fitColumns:true,
    url: 'attribute/aagama/read_all',
    idField: 'id_agama',
    textField: 'nama',
    columns: [[
        {field:'id_agama',title:'ID',width:200,sortable:true,hidden:true},
        {field:'nama',title:'Agama',width:300,sortable:true}
    ]]
});
$('.id_kelurahan').combogrid({
    panelWidth:500,
    mode:'local',
    fitColumns:true,
    url: 'attribute/akelurahan/read_all',
    idField: 'id_kelurahan',
    textField: 'nama',
    columns: [[
        {field:'id_kelurahan',title:'ID',width:200,sortable:true,hidden:true},
        {field:'nama',title:'Kelurahan',width:300,sortable:true},
        {field:'id_kec',title:'ID Kecamatan',width:300,sortable:true,hidden:true}
    ]]
});

$('.id_jamkes').combogrid({
    panelWidth:300,
    mode:'local',
    fitColumns:true,
    url: 'master/mjamkes/read_all',
    idField: 'id_jamkes',
    textField: 'nama',
    columns: [[
        {field:'id_jamkes',title:'ID',width:200,sortable:true,hidden:true},
        {field:'nama',title:'Jamkes',width:300,sortable:true}
    ]]
});