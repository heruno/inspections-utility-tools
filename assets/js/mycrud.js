var url;
$.extend($.fn.validatebox.defaults.rules, {
	equals: {
		validator: function(value, param) {
			return value == $(param[0]).val();
		},
		message: 'Field do not match.'
	}
});
function myCreate(mydialog, myform, myurl, mytitle) {
	$(mydialog).dialog('open').dialog('setTitle', mytitle);
	$(myform).form('clear');
	url = myurl;
}
function myUpdate(mydatagrid, mydialog, myform, myurl, mytitle) {
	var row = $(mydatagrid).datagrid('getSelected');
	if (row) {
		$(mydialog).dialog('open').dialog('setTitle', mytitle);
		$(myform).form('load', row);
		url = myurl;
	} else {
		$.messager.alert('Warning', 'Silahkan pilih Item yang akan diperbaharui', 'warning');
	}
}
function myDelete(mydatagrid, myurl_del, myurl_read, mytitle) {
	var row = $(mydatagrid).datagrid('getSelected');
	if (row) {
		$.messager.confirm('Confirm', 'Apakah anda akan  ' + mytitle, function(r) {
			if (r) {
				$.post(myurl_del, row, function(result) {
					if (result.successful) {
						$.messager.show({
							title: 'Berhasil',
							msg: result.successful,
							timeout: 5000,
							showType: 'show'
						});
						$(mydatagrid).datagrid({
							url: myurl_read
						}, 'load');
						$.messager.progress('close');
					} else {
						$.messager.alert('Warning', result.unsuccessful, 'warning');
					}
				});
			}
		});
	} else {
		$.messager.alert('Warning', 'Silahkan pilih Item yang akan dihapus', 'warning');
	}
}
function mySave(mydatagrid, mydialog, myform, myurl_read) {
	$(myform).form('submit', {
		url: url,
		onSubmit: function() {
			return $(this).form('validate');
		},
		success: function(result) {
			var result = JSON.parse(result);
			if (result.successful) {
				$(mydialog).dialog('close');
				$.messager.show({
					title: 'Berhasil',
					msg: result.successful,
					timeout: 5000,
					showType: 'show'
				});
				$(mydatagrid).datagrid({
					url: myurl_read
				}, 'reload');
			} else {
				$.messager.alert('Warning', result.unsuccessful, 'warning');
			}
		}
	});
}
function filter(grid, myurl) {
	var dg = jQuery(grid);
	dg.datagrid({
		url: myurl,
        remoteFilter: true,
		pagination: true
	});
    dg.datagrid('enableFilter');
    dg.datagrid('doFilter');
}
function cancelFilter(grid, myurl) {
	var dg = jQuery(grid);
	dg.datagrid({
		url: myurl,
        remoteFilter: false,
		pagination: true
	});
    dg.datagrid('removeFilterRule');
    dg.datagrid('doFilter');
    dg.datagrid('disableFilter');
}
function myformatter(date) {
	var y = date.getFullYear();
	var m = date.getMonth() + 1;
	var d = date.getDate();
	return y + '-' + (m < 10 ? ('0' + m) : m) + '-' + (d < 10 ? ('0' + d) : d);
}
function myformatterdtb(date) {
	var y = date.getFullYear();
	var m = date.getMonth() + 1;
	var d = date.getDate();
    var h = date.getHours();
    var i = date.getMinutes();
    var s = date.getSeconds();
	return y + '-' + (m < 10 ? ('0' + m) : m) + '-' + (d < 10 ? ('0' + d) : d) + (h < 10 ? (' 0' + h):h)+':'+(i < 10 ? ('0' + i):i)+':'+(s < 10 ? ('0' + s):s);
}
function myformatter2(date) {
	var y = date.getFullYear();
	var m = date.getMonth() + 1;
	var d = date.getDate();
	return (d < 10 ? ('0' + d) : d)+ '-' + (m < 10 ? ('0' + m) : m) + '-' + y;
}
function myparserdtb(s) {
	if (!s) return new Date();
	var ss = (s.split('-'));
	var y = parseInt(ss[0], 10);
	var m = parseInt(ss[1], 10);
	var d = parseInt(ss[2], 10);
    var h = parseInt(ss[3], 10);
    var i = parseInt(ss[4], 10);
    var s = parseInt(ss[5], 10);
	if (!isNaN(y) && !isNaN(m) && !isNaN(d)&& !isNaN(h)&& !isNaN(i)&& !isNaN(s)) {
		return new Date(y, m - 1, d,h,i,s);
	} else {
		return new Date();
	}
}
function myparser(s) {
	if (!s) return new Date();
	var ss = (s.split('-'));
	var y = parseInt(ss[0], 10);
	var m = parseInt(ss[1], 10);
	var d = parseInt(ss[2], 10);
	if (!isNaN(y) && !isNaN(m) && !isNaN(d)) {
		return new Date(y, m - 1, d);
	} else {
		return new Date();
	}
}
function myparser2(s) {
	if (!s) return new Date();
	var ss = (s.split('-'));
	var y = parseInt(ss[0], 10);
	var m = parseInt(ss[1], 10);
	var d = parseInt(ss[2], 10);
	if (!isNaN(y) && !isNaN(m) && !isNaN(d)) {
		return new Date(d, m - 1, y);
	} else {
		return new Date();
	}
}
function myClose(mydialog) {
	$(mydialog).dialog('close');
}
function myClear(myform) {
	$(myform).form('clear');
}
function doSearch(value, name) {
	var d = name.split(',');
	$(d[1]).datagrid('load', {
		like: d[0],
		cari: value
	});
}
function tmenu(param) {
	window.location.href = param;
}
function myGenerate(myurl) {
	$.get(myurl, function(result) {
		var result = eval('(' + result + ')');
		if (result.successful) {
			$.messager.alert('Info', result.successful, 'info');
		} else {
			$.messager.alert('Error', result.successful, 'error');
		}
	});
}
function hayuSearch() {
	$('#tt').datagrid('reload');
}