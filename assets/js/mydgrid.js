var cmenu;
var mydgrid = {
	onCmenu: function(mygrid) {
		$(mygrid).datagrid({
			onHeaderContextMenu: function(e, field) {
				e.preventDefault();

					createColumnMenu(mygrid);
				cmenu.menu('show', {
					left: e.pageX,
					top: e.pageY
				});
			}
		});
	},
    onChide: function(mygrid, myfield={}){
        $(mygrid).datagrid('hideColumn', item.name);
    }

};
function createColumnMenu(omygrid)
{
	cmenu = $('<div/>').appendTo('body');
	cmenu.menu({
		onClick: function(item){
			if (item.iconCls == 'icon-ok') {
				$(omygrid).datagrid('hideColumn', item.name);
				cmenu.menu('setIcon', {
					target: item.target,
					iconCls: 'icon-empty'
				});
			} else {
				$(omygrid).datagrid('showColumn', item.name);
				 cmenu.menu('setIcon', {
					target: item.target,
					iconCls: 'icon-ok'
				});
			}
		}
	});
	var fields = $(omygrid).datagrid('getColumnFields');
	for (var i = 0; i < fields.length; i++) {
		var field = fields[i];
		var col = $(omygrid).datagrid('getColumnOption', field);
        if(col.hidden == false || col.hidden == undefined){
          cmenu.menu('appendItem', {
			text: col.title,
			name: field,
			iconCls: 'icon-ok'
		});  
        }else{
            cmenu.menu('appendItem', {
			text: col.title,
			name: field,
			iconCls: 'icon-empty'
		});
        }
		
	}
}