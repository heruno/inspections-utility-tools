$("#tree").tree({
	url: 'partial/partial/menu',
	animate: true,
	lines: true,
	dnd: true,
	onClick: function(b) { /** console.log(b);*/
		var icon = b.iconCls;
		if (icon == "") {
			icon = "icon-file";
		}
		if (b.url != null) {
			if ($("#tt").tabs("exists", b.text)) {
				$("#tt").tabs("select", b.text)
			} else {
				try {
					$("#tt").tabs("add", {
						type: "post",
						title: b.text,
						href: b.url,
						iconCls: icon + " icon-large",
						closable: true
					})
				} catch (e) {
					console.log('asem:' + e);
					$.messager.show({
						title: 'Error',
						msg: e,
						timeout: 5000,
						showType: 'slide'
					});
				}
			}
		}
	},
	formatter: function(c) {
		var b = c.text;
		if (c.children != "") {
			b += "&nbsp;<span style='color:green;font-weight:bold'>(" + c.children.length + ")</span>"
		}
		return b
	},
	onDrop: function(e, d, b) {
		var c = $("#tree").tree("getNode", e).id;
		$.ajax({
			url: "partial/partial/update",
			type: "post",
			dataType: "json",
			data: {
				id: d.id,
				targetId: c,
				point: b
			}
		})
	},
	onContextMenu: function(c, b) {
		c.preventDefault();
		$(this).tree("select", b.target);
		$("#mm").menu("show", {
			left: c.pageX,
			top: c.pageY
		})
	}
});

function treeSearch(value) {
	$('#tree').tree({
		url: 'partial/partial/menu/' + value
	});
}

function reload() {
	$('#tree').tree('reload');
}

function collapseAll() {
	var node = $('#tree').tree('getSelected');
	if (node) {
		$('#tree').tree('collapseAll', node.target);
	} else {
		$('#tree').tree('collapseAll');
	}
}

function expandAll() {
	var node = $('#tree').tree('getSelected');
	if (node) {
		$('#tree').tree('expandAll', node.target);
	} else {
		$('#tree').tree('expandAll');
	}
}